package com.getinsured.hix.tkm;

import org.apache.log4j.xml.DOMConfigurator;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * 
 * @author Sharma_k
 *	BaseTest added for Ticket Management module
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public abstract class BaseTest 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);
	private static ApplicationContext context = null;
	@BeforeClass
	public static void setUp()
	{
		try
		{
			setContext(new ClassPathXmlApplicationContext("/applicationContext.xml"));
			String rootPath = System.getenv("GHIX_HOME");
			DOMConfigurator.configure(rootPath + "/ghix-setup/conf/ghix-log4j.xml");
		}
		catch(Exception ex)
		{	
			LOGGER.error("",ex);
		}
	}
	/**
	 * @return the context
	 */
	public static ApplicationContext getContext() {
		return context;
	}
	/**
	 * @param context the context to set
	 */
	public static void setContext(ApplicationContext context) {
		BaseTest.context = context;
	}
}