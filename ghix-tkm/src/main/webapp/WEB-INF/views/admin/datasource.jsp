<%@ page import = "java.util.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.springframework.context.ApplicationContext"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>
<sql:setDataSource var="jspDataSource" driver='<%=prop.getProperty("driver") %>' url='<%=prop.getProperty("url") %>' user='<%=prop.getProperty("username") %>' password='<%=prop.getProperty("password") %>' scope="session"/>
