package com.getinsured.hix.tkm.service.jpa;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.dto.tkm.TicketTaskDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmQuickActionDto;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmUserDTO;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.tkm.repository.ITicketTaskRepository;
import com.getinsured.hix.tkm.service.TkmTicketTaskService;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.getinsured.tkm.enums.TicketStatus;
import java.util.Iterator;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import com.getinsured.hix.tkm.util.TkmConstants;
import com.getinsured.hix.tkm.util.TkmUtil;


@Service("TkmTicketTaskService")
public class TkmTicketTaskServiceImpl implements TkmTicketTaskService, ApplicationContextAware {
	@Autowired
	private ITicketTaskRepository iTicketTaskRepository;
	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;
	@Autowired
	private UserService userService;
	public static final String TASK_MODEL_NAME = "com.getinsured.hix.model.TkmTicketTasks";
	public static final String TASK_REPOSITORY_NAME = "ITicketTaskRepository";
	private static final String TASK_COMMENT_COL = "comments";
	private static final String TASK_NAME_COL = "taskName";
	private static final String TASK_ASSIGNEE_COL = "assignee";
	public static final String TICKET_MODEL_NAME = "com.getinsured.hix.model.TkmTickets";
	public static final String TICKET_REPOSITORY_NAME = "ITicketRepository";
	private static final String TICKET_COMMENT_COL = "closingComment";
	private static final String TICKET_SUBJECT_COL = "subject";
	private static final String TICKET_ASSIGNEE_COL = "tkmQueues";
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String UPDATOR_COL = "updator";
	private static final String STATUS_COL = "status";
	private static final String UPDATED_COL = "updated";
	private static final String NO_COMMENTS = "No Comments";
	private static final String COMMENT_ADDED = "Comment Added";
	private static final String ASSIGNED_TO = "AssignTo";
	private static final String DIS_DATE = "Date";
	private static final String DIS_DESCRIPTION = "TicketOrTask";
	private static final String DIS_STATUS = "Status";
	private static final String DIS_COMMENTS = "Comments";
	private static final String DIS_UPDATOR = "UpdatorName";
	
    @PersistenceUnit private EntityManagerFactory  entityManagerFactory;
	private ApplicationContext appContext;
    private static final String DISPLAY_DATE_FORMAT = "MMM d, yyyy hh:mm a";
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TkmTicketTaskServiceImpl.class);
	
    public List<HistoryDto> loadTicketAndTaskHistory(final Integer ticketId, final boolean isDetailedView)throws TKMException{
        List<HistoryDto> historyDtoList = null;
        historyDtoList = getTicketHistoryAud(ticketId);
        if(isDetailedView){
            // to get the list of tasks history related to a ticketId
            getTaskHistoryAud(ticketId, historyDtoList);
        }
        return historyDtoList;
    }

	/**
	 * implementation of interface signature method.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<HistoryDto> loadTicketStatusHistory(Integer ticketId, boolean isDetailedView) throws TKMException {

		List<HistoryDto> dtoList = null;
		List<Map<String, Object>> ticketHistoryList = null;
		List<Map<String, String>> filteredTicketHistoryList = null;
		try {
			ticketHistoryList = getTicketHistory(ticketId);
			if (ticketHistoryList != null && !ticketHistoryList.isEmpty()) {
				dtoList = new ArrayList<HistoryDto>();
				filteredTicketHistoryList = filterHistory(ticketHistoryList, TICKET_COMMENT_COL, TICKET_SUBJECT_COL, TICKET_ASSIGNEE_COL);
				dtoList.addAll(convertMapToList(filteredTicketHistoryList));
				
				if(isDetailedView) {
					// to get the list of tasks history related to a ticketId
					fetchTasksHistory(ticketId, dtoList);
				}
				
				// sort the list of history objects based on updated date
				Collections.sort(dtoList, new Comparator<HistoryDto>() {
					@Override
					public int compare(HistoryDto firstObj, HistoryDto secondObj) {
						return firstObj.getSortDate().compareTo(
								secondObj.getSortDate());
					}
				});
				Collections.reverse(dtoList);
			}

		} catch (Exception e) {
			LOGGER.error("exception occurred while loading ticket/task history"
					+ e.getMessage());
			throw (TKMException)e;
		}
		return dtoList;
	}

	/**
	 * 
	 * @param ticketId
	 * @param dtoList
	 * Fetches all task history and append it to ticket history
	 */
	private void fetchTasksHistory(Integer ticketId, List<HistoryDto> dtoList) throws TKMException {
		List<TkmTicketTasks> tkmTicketTasksList = getTkmTicketTasksList(ticketId);
		if (tkmTicketTasksList != null && !tkmTicketTasksList.isEmpty()) {
			for (TkmTicketTasks tasks : tkmTicketTasksList) {

				List<Map<String, Object>> taskHistoryList = null;
				List<Map<String, String>> filteredTaskHistoryList = null;
				Integer taskId = tasks.getTaskId();
				taskHistoryList = getTaskHistory(taskId);
				filteredTaskHistoryList = filterHistory(taskHistoryList, TASK_COMMENT_COL, TASK_NAME_COL, TASK_ASSIGNEE_COL);
				dtoList.addAll(convertMapToList(filteredTaskHistoryList));
			}
		}
	}

	/**
	 * convert filtered map data of a ticket/task to List of DTO objects
	 */
	private List<HistoryDto> convertMapToList(List<Map<String, String>> filteredData) {

		List<HistoryDto> localList = new ArrayList<HistoryDto>();
		SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_PATTERN);
		DateFormatSymbols dateSymbols=new DateFormatSymbols();
		dateSymbols.setAmPmStrings(new String[]{"am", "pm"});
		SimpleDateFormat sdfFormat=new SimpleDateFormat("MMM d, yyyy hh:mm a");
		sdfFormat.setDateFormatSymbols(dateSymbols);
		for (Map<String, String> ticketMap : filteredData) {
			HistoryDto dto = new HistoryDto();
			String strDate = ticketMap.get(DIS_DATE);
			Date dbDate = null;
			if (!StringUtils.isEmpty(strDate)) {
				try {
					dbDate = sdf.parse(strDate);
					strDate = sdfFormat.format(dbDate);
				} catch (ParseException e) {
					LOGGER.error("Unable to parse Updated Date"+ strDate + " " + e);
				}
			}
			dto.setDisDate(strDate);
			dto.setSortDate(dbDate);
			dto.setStatus(ticketMap.get(DIS_STATUS));
			dto.setDescription(ticketMap.get(DIS_DESCRIPTION));
			dto.setComments(ticketMap.get(DIS_COMMENTS));
			dto.setName(ticketMap.get(DIS_UPDATOR));
			dto.setAssignee(ticketMap.get(ASSIGNED_TO));
			localList.add(dto);
		}
		return localList;
	}

	/**
	 * get the history of a ticket 'ticketId'
	 */
	private List<Map<String, Object>> getTicketHistory(Integer ticketId){
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();

		requiredFieldsMap.put(UPDATED_COL, DIS_DATE);
		requiredFieldsMap.put(STATUS_COL, DIS_STATUS);
		requiredFieldsMap.put(TICKET_COMMENT_COL, DIS_COMMENTS);
		requiredFieldsMap.put(TICKET_SUBJECT_COL, DIS_DESCRIPTION);
		requiredFieldsMap.put(UPDATOR_COL, DIS_UPDATOR);
		requiredFieldsMap.put(TICKET_ASSIGNEE_COL, ASSIGNED_TO);
		List<Map<String, Object>> ticketHistoryData = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		ticketHistoryData = service.findAudRevisions(TICKET_REPOSITORY_NAME, TICKET_MODEL_NAME, requiredFieldsMap, ticketId);
		return ticketHistoryData;
	}

	/**
	 * get the history of a task 'taskId'
	 */
	private List<Map<String, Object>> getTaskHistory(Integer taskId) {

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();

		requiredFieldsMap.put(UPDATED_COL, DIS_DATE);
		requiredFieldsMap.put(STATUS_COL, DIS_STATUS);
		requiredFieldsMap.put(TASK_COMMENT_COL, DIS_COMMENTS);
		requiredFieldsMap.put(TASK_NAME_COL, DIS_DESCRIPTION);
		requiredFieldsMap.put(TASK_ASSIGNEE_COL, ASSIGNED_TO);
		requiredFieldsMap.put(UPDATOR_COL, DIS_UPDATOR);
		List<Map<String, Object>> taskHistoryData = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		taskHistoryData = service.findAudRevisions(TASK_REPOSITORY_NAME, TASK_MODEL_NAME, requiredFieldsMap, taskId);
		return taskHistoryData;
	}

	/**
	 * filtering the history data(for ticket and tasks) based on status,
	 * comments, updator and assignee
	 */

	private List<Map<String, String>> filterHistory(
			List<Map<String, Object>> history, String commentsCol,
			String subjectOrNameCol, String assigneeCol) {
		List<Map<String, String>> filteredData = new ArrayList<Map<String, String>>();
		int size = history.size();
		Map<String, Object> firstElement = history.get(0);
		filteredData.add(convertAndAdd(firstElement, commentsCol, subjectOrNameCol, assigneeCol));
		Map<String, Object> secondElement = null;

		for (int i = 0; i < size - 1; i++) {
			firstElement = history.get(i);
			secondElement = history.get(i + 1);

			if(isEqual(firstElement, secondElement, STATUS_COL) && isEqual(firstElement, secondElement, commentsCol) && (getId(firstElement, assigneeCol) == getId(secondElement, assigneeCol)) && (getId(firstElement, UPDATOR_COL)==getId(secondElement, UPDATOR_COL))){
				continue;
			}
			else{
				filteredData.add(convertAndAdd(secondElement, commentsCol, subjectOrNameCol, assigneeCol));
			}
		}
		return filteredData;
	}
	
	/**
	 * this method converts Object value to string value
	 */
	private Map<String, String> convertAndAdd(Map<String, Object> inMap,
			String commentsCol, String subjectOrNameCol, String assigneeCol) {
		AccountUser user = null;
		TkmQueues tkmQueues = null;
		String assignee = "";
		Date updateDate = null;
		Map<String, String> outMap = new HashMap<String, String>();
		String comments = (String) inMap.get(commentsCol);
		String subject = (String) inMap.get(subjectOrNameCol);
		String status = (String) inMap.get(STATUS_COL);
		Object dateObj = inMap.get(UPDATED_COL);
		if (dateObj instanceof Date) {
			updateDate = (Date) dateObj;
		}
		Object updatorObj = inMap.get(UPDATOR_COL);
		if (updatorObj instanceof Integer) {
			int userId = (Integer) updatorObj;
			user = userService.findById(userId);
		} else if (updatorObj instanceof AccountUser) {
			user = (AccountUser) updatorObj;
		}
		Object assigneeObj = inMap.get(assigneeCol);
		if (assigneeObj instanceof TkmQueues) {
			tkmQueues = (TkmQueues) assigneeObj;
			assignee = getValue(tkmQueues);
		} else if (assigneeObj instanceof AccountUser) {
			AccountUser assignUser = (AccountUser) assigneeObj;
			assignee = getValue(assignUser);
		}
		comments = (StringUtils.isEmpty(comments)) ? NO_COMMENTS
				: COMMENT_ADDED;
		subject = (!(StringUtils.isEmpty(comments))) ? subject : "";
		status = (!(StringUtils.isEmpty(status))) ? status : "";
		String lastUpdatedDate = (updateDate != null) ? updateDate.toString() : "";
		String updator = getValue(user);
		outMap.put(DIS_COMMENTS, comments);
		outMap.put(DIS_DESCRIPTION, subject);
		outMap.put(DIS_STATUS, status);
		TicketStatus[] ticketStatus = TicketStatus.values();
		for (TicketStatus ticketTaskStatus : ticketStatus) {
			if(outMap.get(DIS_STATUS).equals(ticketTaskStatus.getTicketStatusCode())){
				outMap.put(DIS_STATUS, TicketStatus.valueOf(status).getDescription());
				break;
			}
		}
		
		
		outMap.put(DIS_UPDATOR, updator);
		outMap.put(DIS_DATE, lastUpdatedDate);
		outMap.put(ASSIGNED_TO, assignee);
		return outMap;
		}

	/**
	 * this method returns id(database id value) of the object
	 */
	private int getId(Map<String, Object> mapObj, String key) {
		Object obj = mapObj.get(key);
		if (obj instanceof TkmQueues) {
			TkmQueues tkmQueues = (TkmQueues) obj;
			return tkmQueues.getId();
		} else if (obj instanceof AccountUser) {
			AccountUser user = (AccountUser) obj;
			return user.getId();
		} else if (obj instanceof Integer) {
			return (Integer)obj;
		} else {
			return 0;
		}
	}

	/**
	 * this method returns string value (name)
	 */
	private String getValue(Object obj){
		if (obj instanceof TkmQueues) {
			TkmQueues tkmQueues = (TkmQueues) obj;
			return (tkmQueues!=null) ? (!StringUtils.isEmpty(tkmQueues.getName()) ? tkmQueues.getName() : "" ) : "";
		} else if (obj instanceof AccountUser) {
			AccountUser user = (AccountUser) obj;
			return (user != null) ? ((!StringUtils.isEmpty(user.getFirstName()) ? user.getFirstName() : "") + " " + (!StringUtils.isEmpty(user.getLastName()) ? user.getLastName() : "")) : "";
		}else{
			return "";
		}
	}
    	
	/**
	 * Compares values of maps firstElement and secondElement
	 */
	private boolean isEqual(Map<String, Object> firstElement,
			Map<String, Object> secondElement, String keyColumn) {
		String val1 = (String) firstElement.get(keyColumn);
		String val2 = (String) secondElement.get(keyColumn);
		return StringUtils.equals(val1, val2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TkmTicketTasks> getTkmTicketTasksList(int ticketId) throws TKMException{
	 	
		return iTicketTaskRepository.getTkmTicketTaskList(ticketId);
	}

	@Override
	@Transactional
	public TkmTicketTasks addNewTicketTask(TkmTicketTasks tkmTicketTasks) {
		return iTicketTaskRepository.save(tkmTicketTasks);
	}

	@Override
	@Transactional
	public boolean claimTicket(Integer taskId, Integer userId) {
		boolean bStatus = false;

		try {
			TkmTicketTasks tkmTicketTasks = iTicketTaskRepository
					.findOne(taskId);
			if (tkmTicketTasks == null) {
				return bStatus;
			}

			AccountUser assignee = new AccountUser();
			assignee.setId(userId);

			tkmTicketTasks.setAssignee(assignee);
			tkmTicketTasks
					.setStatus(TkmTicketTasks.task_status.Claimed.toString());

			iTicketTaskRepository.save(tkmTicketTasks);

			bStatus = true;
		} catch (Exception e) {
			LOGGER.error("Claim failed: " + e);
		}

		return bStatus;
	}

	@Override
	public boolean completeTask(Integer taskId, String comments) {
		boolean bStatus = false;
		if (taskId == null || taskId <= 0) {
			return bStatus;
		}

		
			TkmTicketTasks tkmTicketTasks = iTicketTaskRepository
					.findOne(taskId);

			if (tkmTicketTasks == null) {
				return bStatus;
			}

			tkmTicketTasks.setEndDate(new TSDate());
			tkmTicketTasks.setStatus(TkmTicketTasks.task_status.Closed
					.toString());
			if (comments != null) {
				tkmTicketTasks.setComments(comments);
			}

			iTicketTaskRepository.save(tkmTicketTasks);
			bStatus = true;
		
		return bStatus;
	}

	@Override
	@Transactional
	public TkmTicketTasks findByTaskId(Integer taskId) {
		TkmTicketTasks tkmTicketTasks = iTicketTaskRepository.findOne(taskId);
		
		return tkmTicketTasks;
	}

	@Override
	@Transactional
	public boolean updateTasks(List<TkmTicketTasks> tasks) {

		boolean bStatus = false;
		List<TkmTicketTasks> savedTasks = iTicketTaskRepository.save(tasks);

		bStatus = (savedTasks.size() == tasks.size()) ? true : bStatus;

		return bStatus;
	
	}
	

	@Override
	public List<TkmTicketTasks> getProcessInstanceTaskList(String processInstanceId) {
		
		return iTicketTaskRepository.findProcessInstanceTaskList(processInstanceId);
	}
	
	@Override
	public Integer getTaskTicketId(Integer taskId) {
		
		return iTicketTaskRepository.getTicketIdForTask(taskId);
	}

	@Override
	public TkmTicketTasks findTkmTasksByActivitiTaskId(
			String activitiTaskId) {
		 return iTicketTaskRepository.getTaskForActivitiTaskTaskId(activitiTaskId);
	}

	@Override
	public long getTicketTaskCount() 
	{
		return iTicketTaskRepository.getTicketTaskCount();
	}

	@Override
	public List<TkmTicketTasks> getTicketTaskByAssignee(String assigneeName)  
	{   List<TkmTicketTasks> ticketTaskByAssignee = iTicketTaskRepository.getTkmticketByAssignee(assigneeName);
	
	return ticketTaskByAssignee;
	}
	
	@Override
	public TkmTicketTasks getTicketByTaskIdAndActivitiId(String processInstanceId,
			String activitiTaskId) {
		TkmTicketTasks task = iTicketTaskRepository.findByProcessInstanceIdAndActivitiTaskId(processInstanceId, activitiTaskId);
		
		return task;
	}
	
	@Override
	public List<TkmUserDTO> geTaskUserInfoByAssignee(String assigneeName)  
 {
		List<TkmUserDTO> tkmUserDTOs = new ArrayList<TkmUserDTO>();

			List<Object[]> qryResult = iTicketTaskRepository
					.getTkmUserInfoByAssignee(assigneeName);
			tkmUserDTOs = mapQueryResultToDTO(qryResult);
		
		return tkmUserDTOs;
	}
	
	
	@Override
	public TicketTaskDto getNewTicketTaskToClaim(Integer userID,String workGroupId)  throws TKMException
 {
		LOGGER.debug(
				"-------------------------------------\n inside getNewTicketTaskToClaim --------------------------------------\n");
		TicketTaskDto taskDto = new TicketTaskDto();
		EntityManager entityManager = null;
		try {
			entityManager = entityManagerFactory.createEntityManager();
			StringBuilder stringbuilder = new StringBuilder();

			stringbuilder.append(
					"Select ttt.tkm_ticket_task_id,ttt.tkm_ticket_id, ttt.tkm_queue_id,ttt.activiti_task_id,ttt.name,tkt.priority_order as priorityOrder,");
			stringbuilder.append(
					" tkt.due_date  AS tktDueDt,tkt.creation_timestamp AS tktcreation from tkm_tickets tkt,tkm_ticket_tasks ttt where tkt.tkm_ticket_id=ttt.tkm_ticket_id and ttt.status='UnClaimed' ");
			
			
			if(workGroupId==null )
			{
				stringbuilder.append (" and tkm_queue_id in (  select group_id from queue_user where user_id= ");
				stringbuilder.append (userID);
			}else
			{	stringbuilder.append(" and  tkm_queue_id in (");
				stringbuilder.append("select tkm_queue_id from tkm_queues where queue_name= '" + workGroupId +"'");
			}
			
			stringbuilder.append (")");
			stringbuilder.append(" order by  priorityOrder desc,tktDueDt,tktcreation asc limit 1");

			Query query = entityManager.createNativeQuery(stringbuilder.toString());
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();

			while (rsIterator.hasNext()) {

				Object[] objArray = (Object[]) rsIterator.next();
				BigDecimal taskId=(BigDecimal) objArray[0];
				taskDto.setTaskId(taskId.intValue());
				BigDecimal ticketId=(BigDecimal) objArray[1];
				taskDto.setTicketId(ticketId.intValue());
				BigDecimal tkmQueueId=(BigDecimal) objArray[2];
				taskDto.setTkmQueueId(tkmQueueId.intValue());
				taskDto.setActivitiTaskId((String) objArray[3]);
				taskDto.setTaskName((String) objArray[4]);
				break;
			}
		} catch (Exception ex) {
			// LOGGER.error("Exception occurred while fetching data ",ex);
			throw new TKMException(ex);
		}

		finally {
			if (entityManager != null && entityManager.isOpen()) {
				try {
					entityManager.close();
				} catch (IllegalStateException illegal) {
					// Suppressing EntityManager close exception
					LOGGER.error("Exception occurred while closing entityManager ", illegal);
				}
			}
		}
		return taskDto;
	}
	
	
	
	private List<TkmUserDTO> mapQueryResultToDTO(List<Object[]> qryResults) {
		List<TkmUserDTO> tkmUserDTOs = new ArrayList<TkmUserDTO>();
		if(qryResults!=null){
			for (Object[] qryResult : qryResults) {
				TkmUserDTO tkmUserDTO = new TkmUserDTO();
				tkmUserDTO.setUserId((Integer)qryResult[0]);
				tkmUserDTO.setFullName((String) qryResult[1]);
				tkmUserDTOs.add(tkmUserDTO);
			}
		}
		return tkmUserDTOs;
	}

	@Override
	public TkmQuickActionDto getDetailsForQuickActionBar(Integer ticketId) {
		TkmQuickActionDto actionBarDetails = null;
		
		try {
			List<Object> resultSet = iTicketTaskRepository.getDetailsForQuickActionBar(ticketId);		
			if(resultSet != null ) {
				Object[] result = (Object[]) resultSet.get(0);
				
				String userName = StringUtils.EMPTY;
				if(result[0] != null) {
					Integer userId  = Integer.valueOf((int)result[0]);
					userName = userService.getUserName(userId);
				}
				
				String status = (String)result[1];
				if(status!=null){
					status = TicketStatus.valueOf(status).getDescription() ;
				}
				actionBarDetails = new TkmQuickActionDto(userName, status, (String)result[2], (String)result[3]);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("",e);
		}
		
		return actionBarDetails;
	}
	
	/**
	 * 
	 * @param ticketId
	 * @return
	 */
	private List<HistoryDto> getTicketHistoryAud(final Integer ticketId)throws TKMException{
		LOGGER.debug("-------------------------------------\n inside getTicketHistoryAud --------------------------------------\n");
		List<HistoryDto> historyDtoList = null;
		EntityManager entityManager = null;
		try{
			entityManager = entityManagerFactory.createEntityManager();
			StringBuilder stringbuilder = new StringBuilder();
		
			stringbuilder.append("SELECT tkm.CLOSING_COMMENT, que.QUEUE_NAME, userObj.FIRST_NAME, userObj.LAST_NAME, tkm.LAST_UPDATE_TIMESTAMP, tkm.SUBJECT, tkm.STATUS");
			stringbuilder.append(" FROM TKM_TICKETS_AUD tkm");
			stringbuilder.append(" LEFT JOIN USERS userObj ON tkm.LAST_UPDATED_BY = userObj.id, TKM_QUEUES que ");
			stringbuilder.append(" WHERE tkm.ASSIGNED_QUEUE = que.TKM_QUEUE_ID");
			stringbuilder.append(" AND tkm.TKM_TICKET_ID=");
			stringbuilder.append(ticketId);
			stringbuilder.append(" ORDER BY tkm.LAST_UPDATE_TIMESTAMP ASC");


			Query query = entityManager.createNativeQuery(stringbuilder.toString());
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			historyDtoList = new ArrayList<HistoryDto>();
			
			HashSet<String> ticketKeys = new HashSet<String>();
			while (rsIterator.hasNext()) {
				StringBuilder ticketKey = new StringBuilder();
				Object[] objArray = (Object[]) rsIterator.next();
				String updatorName = TkmUtil.getFullName((String)objArray[TkmConstants.TWO], null, (String)objArray[TkmConstants.THREE]);
				Date lastUpdatedDate = (Date)objArray[TkmConstants.FOUR];
				
				// form ticket key - assignee + updator name + display date + status
				ticketKey.append((String)objArray[TkmConstants.ONE]);
				ticketKey.append(updatorName);
				if (lastUpdatedDate != null) {
					ticketKey.append(formatDisplayDate(lastUpdatedDate));
				}
				ticketKey.append((String)objArray[TkmConstants.SIX]);
				
				if(ticketKeys.contains(ticketKey.toString())){
					historyDtoList.remove(historyDtoList.size()-1); // same key is present so remove prev record and add new one
																	// records are arranged by timestamp asc.
				}
				
				LOGGER.debug("Ticket Key="+ ticketKey.toString());
				HistoryDto historyDto = new HistoryDto();
				historyDto.setComments((String)objArray[TkmConstants.ZERO]);
				historyDto.setAssignee((String)objArray[TkmConstants.ONE]);
				historyDto.setName(updatorName);
				if (lastUpdatedDate != null) {
					historyDto.setSortDate(lastUpdatedDate);
					historyDto.setDisDate(formatDisplayDate(lastUpdatedDate));
				}
				historyDto.setDescription((String)objArray[TkmConstants.FIVE]);
				historyDto.setStatus((String)objArray[TkmConstants.SIX]);
				historyDtoList.add(historyDto);					
				ticketKeys.add(ticketKey.toString());

			}
		}
		catch(Exception ex){
			//LOGGER.error("Exception occurred while fetching data ",ex);
			throw new TKMException(ex);
		}

		finally{
			if(entityManager!= null && entityManager.isOpen()){
				try{
				entityManager.close();
				}
				catch(IllegalStateException illegal){
					//Suppressing EntityManager close exception
					LOGGER.error("Exception occurred while closing entityManager ",illegal);
				}
			}
		}
		return historyDtoList;
	}

	/**
	 * 
	 * @param ticketId
	 * @param historyDtoList
	 * @throws TKMException
	 */
	private void getTaskHistoryAud(final Integer ticketId, List<HistoryDto> historyDtoList)throws TKMException{
		EntityManager entityManager = null;
		try{
			List<TkmTicketTasks> tkmTicketTasksList = getTkmTicketTasksList(ticketId);
			

			if (tkmTicketTasksList != null && !tkmTicketTasksList.isEmpty()) {
				entityManager = entityManagerFactory.createEntityManager();
				StringBuilder stringbuilder = new StringBuilder();
				stringbuilder.append("SELECT taskAud.LAST_UPDATE_TIMESTAMP, taskAud.STATUS, taskAud.COMMENTS, taskAud.NAME, assign.FIRST_NAME, assign.LAST_NAME, updatedBy.FIRST_NAME as UPDATEDBY_FIRST_NAME, updatedBy.LAST_NAME as UPDATEDBY_LAST_NAME");
				stringbuilder.append(" FROM TKM_TICKET_TASKS_AUD taskAud "
						+ " LEFT JOIN USERS assign ON taskAud.ASSIGNEE = assign.id"
						+ " LEFT JOIN USERS updatedBy ON taskAud.LAST_UPDATED_BY = updatedBy.id");
				stringbuilder.append(" WHERE TKM_TICKET_TASK_ID =");
				String sortBy = " ORDER BY taskAud.LAST_UPDATE_TIMESTAMP ";

				String sqlQuery = stringbuilder.toString();

				if(historyDtoList == null){
					historyDtoList = new ArrayList<HistoryDto>();
				}

				for (TkmTicketTasks tasks : tkmTicketTasksList) {
					HashSet<String> taskKeys = new HashSet<String>();
					Query query = entityManager.createNativeQuery(sqlQuery +tasks.getTaskId()+ sortBy);
					List<?> rsList = query.getResultList();
					Iterator<?> rsIterator = rsList.iterator();

					while (rsIterator.hasNext()) {
						// Form task key - assignee + updator name + display date + status
						StringBuilder taskKey = new StringBuilder();
						Object[] objArray = (Object[]) rsIterator.next();
						Date lastUpdatedDate = (Date)objArray[TkmConstants.ZERO];
						String assigneeName = TkmUtil.getFullName((String)objArray[TkmConstants.FOUR], null, (String)objArray[TkmConstants.FIVE]);
						String updatorName = TkmUtil.getFullName((String)objArray[TkmConstants.SIX], null, (String)objArray[TkmConstants.SEVEN]);
						
						taskKey.append(assigneeName);
						taskKey.append(updatorName);
						if (lastUpdatedDate != null) {
							taskKey.append(formatDisplayDate(lastUpdatedDate));
						}
						taskKey.append((String)objArray[TkmConstants.ONE]);
						
						if(taskKeys.contains(taskKey.toString())){
							historyDtoList.remove(historyDtoList.size()-1);
						}
						
						
						HistoryDto historyDto = new HistoryDto();
						

						if (lastUpdatedDate != null) {
							historyDto.setSortDate(lastUpdatedDate);
							historyDto.setDisDate(formatDisplayDate(lastUpdatedDate));
						}
						historyDto.setStatus((String)objArray[TkmConstants.ONE]);
						historyDto.setComments((String)objArray[TkmConstants.TWO]);
						historyDto.setDescription((String)objArray[TkmConstants.THREE]);
						
						historyDto.setAssignee(assigneeName);
						
						historyDto.setName(updatorName);
						historyDtoList.add(historyDto);
						
						taskKeys.add(taskKey.toString());
					}
				}
			}
		}
		catch(Exception ex){
			throw new TKMException(ex);
		}
		finally{
			if(entityManager!= null && entityManager.isOpen()){
				try{
				entityManager.close();
				}
				catch(IllegalStateException illegal){
					//Suppressing EntityManager close exception
					LOGGER.error("Exception occurred while closing entityManager ",illegal);
				}
			}
		}
	}

	private String formatDisplayDate(final Date date){
		DateFormatSymbols dateSymbols=new DateFormatSymbols();
		dateSymbols.setAmPmStrings(new String[]{"am", "pm"});
		SimpleDateFormat sdfFormat=new SimpleDateFormat(DISPLAY_DATE_FORMAT);
		sdfFormat.setDateFormatSymbols(dateSymbols);

		return sdfFormat.format(date);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}
	
}
