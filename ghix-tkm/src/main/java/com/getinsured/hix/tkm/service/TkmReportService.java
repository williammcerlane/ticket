package com.getinsured.hix.tkm.service;

import java.util.Map;

public interface TkmReportService {
	Map<String, Object> getPendingTicketDetails(Map<String, Object> searchCriteria);
}
