package com.getinsured.hix.tkm.activiti;

import java.util.logging.Logger;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmNotificationService;
 
 
public class OverdueEmailNotificationListener implements JavaDelegate {
 
	/**
	 * 
	 */
	private static Logger log = Logger.getLogger(OverdueEmailNotificationListener.class.getName());
 
	@SuppressWarnings("unused")
	private TkmNotificationService tkmNotificationService;
	
	@SuppressWarnings("unused")
	private TicketListService ticketListService;
	
	public void setTicketListService(TicketListService ticketListService) {
		this.ticketListService = ticketListService;
	}
	
	public void setTkmNotificationService(TkmNotificationService tkmNotificationService) {
		this.tkmNotificationService = tkmNotificationService;
	}
	
	@Override
	public void execute(DelegateExecution execution) {
	/*	TaskService taskService;*/
	/*	try{*/
		log.info("Inside OverdueEmailNotificationListener commented as per HIX-63524");
		/*	taskService=execution.getEngineServices().getTaskService();
		List<Task> activeTaskList  = taskService.createTaskQuery().processInstanceId(execution.getProcessInstanceId())
				.list();
		TkmTickets tkmTickets = ticketListService.getTkmTicketsByActivitiTaskId(activeTaskList.get(0).getId()).get(0);
		
		String subject = "";
		//both user name and group name are null
		for (Task activeTasks : activeTaskList) {
			StringBuilder message = new StringBuilder("");
				if (tkmTickets.getStatus().equals("Open")) {

					subject = "OVERDUE – Your"
							+ tkmTickets.getTkmWorkflows().getType();
					message.append("<br> Ticket is overdue. A ticket assigned to you is now overdue. Please complete and close the ticket ASAP!.<br/><br/>");
				} else if (tkmTickets.getStatus().equals("UnClaimed")) {
					subject = "OVERDUE – Unclaimed "
							+ tkmTickets.getTkmWorkflows().getType();
					message.append("<br> Ticket is overdue. A ticket assigned to your workgroup is unclaimed and now overdue. Please claim and complete work on the ticket ASAP!.<br/><br/>");
				}
			message.append("Ticket Type: "+tkmTickets.getTkmWorkflows().getType()+"<br>");
			message.append("Due Date: "+tkmTickets.getDueDate()+"<br>");
			message.append("Priority: "+activeTasks.getPriority()+"<br>");
			message.append("</a>");
			String assignee = activeTasks.getAssignee();
			String owner = null;
			
			if(assignee == null) {
				//ASSUMPTION: Owner should always be a group (only one) in BPM file 
				List<IdentityLink> links = taskService.getIdentityLinksForTask(activeTasks.getId());
				for (IdentityLink identityLink : links) {
					List<Group> group = Context.getProcessEngineConfiguration().getIdentityService().createGroupQuery().groupName(identityLink.getGroupId()).list();
					owner = group.get(0).getName();
					
					if(owner != null) {
						break;
					}
				}
				if(owner!=null){
					List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().memberOfGroup(owner).list();
					for (User user : users) {
						Map<String,String> emailTokens = new HashMap<String, String>();
						emailTokens.put("recipient", user.getEmail());
						emailTokens.put("subject", subject);
						emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
						emailTokens.put("msgText",message.toString() );
						
						tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
					}
					
					break;
				}
			}	
			else{
				List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().userId(assignee).list();
				User user =users.get(0);
				Map<String,String> emailTokens = new HashMap<String, String>();
				emailTokens.put("recipient", user.getEmail());
				emailTokens.put("subject", subject);
				emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
				emailTokens.put("msgText",message.toString() );
				
				tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
			}
		}*/
	/*	}
		catch(Exception e){
			log.info("Exception in notification listener: "+ e);
		}*/
 
	}
}
