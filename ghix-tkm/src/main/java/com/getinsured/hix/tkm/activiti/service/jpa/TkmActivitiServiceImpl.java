package com.getinsured.hix.tkm.activiti.service.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.service.TkmMgmtService;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.tkm.activiti.service.TkmActivitiService;
import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmQueuesService;
import com.getinsured.hix.tkm.service.TkmTicketTaskService;
import com.getinsured.hix.tkm.tkmException.TKMException;

@Service("TkmActivitiService")
public class TkmActivitiServiceImpl implements TkmActivitiService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TkmActivitiServiceImpl.class);
	
	@Autowired private RuntimeService runtimeService;
	@Autowired private TaskService taskService;
	@Autowired private UserService userService;
	@Autowired private TkmQueuesService tkmQueuesService;
	@Autowired private TkmTicketTaskService tkmTicketTaskService;
	@Autowired private TicketListService ticketListService;
	@Autowired private HistoryService historyService;
	@Autowired private FormService formService;
	@Autowired private  RepositoryService repositoryService ;
	@Autowired private TkmMgmtService tkmMgmtService;
	@Autowired private ITicketRepository ticketRepository;
	
	@Override
	public boolean initiateProcess(TkmTickets tickets, String processId, String groupName, Map<String, String> ticketFormPropertiesMap) {
		LOGGER.info("initiateProcess: initiating Activiti Process: " + processId + "( " + groupName + ")");
		
		boolean bStatus = false;
		if(StringUtils.isEmpty(processId) || StringUtils.isEmpty(groupName)) {
			LOGGER.error("Error initiating process parameters invalid");
			return bStatus;
		}
		
		Map<String, Object> variables = null;
		ProcessInstance processInstance = null;
		
		try {
			variables = getProcessVariableMap(tickets, groupName, ticketFormPropertiesMap);
			processInstance = runtimeService.startProcessInstanceByKey(processId, variables);		
			
		} catch (ActivitiObjectNotFoundException ex) {
			LOGGER.info("Unable to execute the workflow, initiating the default workflow",ex);
			processInstance = getDefaultProcessInstance("defaultProcess", variables);
		}
		
		if(processInstance == null) {
			LOGGER.error("Unable to process the workflow: " + processId);
			return bStatus;
		}
		
		String processInstanceId = processInstance.getProcessInstanceId();
		LOGGER.info("initiateProcess: process initiated");
		
		bStatus = insertTask(tickets, processInstanceId, null, 0);	
		return bStatus;
	}
	
	private ProcessInstance getDefaultProcessInstance(String processId, Map<String, Object> variables) {
		return runtimeService.startProcessInstanceByKey(processId, variables);
	}
	
	private boolean insertTask(TkmTickets tickets, String processInstanceId, String groupName, Integer userId) {
		boolean bStatus = false;
		//Check for the valid parameters
		if(StringUtils.isEmpty(processInstanceId) || tickets == null) {
			LOGGER.error("insertTask: Invalid parameters");
			return bStatus;
		}
		
		AccountUser user = null;
		
		//If user is present check tasks for user
		if((userId != null) && (userId > 0)) {
			user = userService.findById(userId);
		}
		List<Task> tasks = getTaskList(user, groupName, processInstanceId);
		
		if(tasks != null) {
			
			//If task list size is 0, then check if process has been completed.
			if(tasks.isEmpty()) {
				bStatus = isProcessComplete(processInstanceId);
				return bStatus;
			}
			
			TkmTickets ticket = null;
			try {
				ticket = ticketListService.findTkmTicketsById(tickets.getId());
			} catch (Exception e) {
				LOGGER.error("Ticket id is invalid",e);
				return bStatus;
			}
			/*if(ticket == null) {
				LOGGER.error("Ticket id is invalid");*/
				
			
			
			//if tasks is not null the do the actual insert in the TKM_TICKET_TASKS table
			for (Task task : tasks) {
				bStatus = addTicketTask(task, ticket, user, groupName);
				
				if(!bStatus) {
					break;
				} 
			}
		} else {
			LOGGER.info("insertTask + No task inserted");
			bStatus = false;
		}
		
		return bStatus;
	}
	
	private TkmTicketTasks populateTicketTaskObject(TkmTickets tickets, Task task){
		TkmTicketTasks ticketTask = new TkmTicketTasks();
		ticketTask.setCreator(tickets.getCreator().getId());
		ticketTask.setUpdator(tickets.getUpdator().getId());
		ticketTask.setTicket(tickets);
		ticketTask.setTaskName(task.getName());
		ticketTask.setCreated(task.getCreateTime());
		ticketTask.setStatus(TkmTicketTasks.task_status.UnClaimed.toString());
		ticketTask.setProcessInstanceId(task.getProcessInstanceId());
		ticketTask.setActivitiTaskId(task.getId());
		ticketTask.setStartDate(task.getCreateTime());
		ticketTask.setDueDate(tickets.getDueDate());
		return ticketTask;
	}
	
	private boolean addTicketTask(Task task, TkmTickets tickets, AccountUser user, String groupName) {
		boolean bStatus = false;
		
		TkmTicketTasks ticketTask = populateTicketTaskObject(tickets, task);
		
// COMMENTED : user id would be the assignee of the previous task.
// As per HIX-44541 : Only first task should be claimed and other task should be auto-assigned		
//		//if user is passed then assign a task to the user specified
//		// in this case the group should be null
//		if(user != null) {
//			ticketTask.setAssignee(user);
//			ticketTask.setQueue(null);					
//		} else 
		if(!StringUtils.isEmpty(groupName)) {
			//if group name is provided and user name is not available 
			//then assign the task to the group and user should be null
			ticketTask.setAssignee(null);
			TkmQueues queue = tkmQueuesService.findByName(groupName);					
			ticketTask.setQueue(queue);
		} else {
			//both user name and group name are null
			String assignee = task.getAssignee();
			String owner = null;
			if(assignee == null) {
				//ASSUMPTION: Owner should always be a group (only one) in BPM file 
				for (IdentityLink identityLink : taskService.getIdentityLinksForTask(task.getId())) {
					owner = identityLink.getGroupId();
					
					if(owner != null) {
						break;
					}
				}
			}
			
			if(!StringUtils.isEmpty(assignee)) {
				AccountUser taskUser = userService.findByUserName(assignee);                                                      
				ticketTask.setAssignee(taskUser);
				ticketTask.setStatus(TkmTicketTasks.task_status.Claimed.toString());
				ticketTask.setQueue(null);
				
				openUnclaimedTicket(task, tickets);
			} else if(!StringUtils.isEmpty(owner)) {
				TkmQueues taskOwner = tkmQueuesService.findByName(owner);
				ticketTask.setAssignee(null);
				ticketTask.setQueue(taskOwner);
			} else {
				LOGGER.error("Task does not have any group or user in BPM file.");
				return bStatus;
			}
		}	

		// Check for the auto-assignment for the task, if task humanPerformer is not available
		autoAssignTask(ticketTask, task, user);
		
		// Add new task to the table
		try {
			tkmTicketTaskService.addNewTicketTask(ticketTask);
			bStatus = true;
			
			LOGGER.info("insertTask: Task inserted for processId: " + task.getProcessInstanceId());
		} catch (Exception e) {
			LOGGER.error("Unable to add the task for the ticket", e);
		}
		return bStatus;
	}
	
	/**
	 * This method will assign the task to the user if activiti task does not have any human performer and is assigned to group
	 * @param ticketTask
	 * @param task
	 * @param user
	 */
	private void autoAssignTask(TkmTicketTasks ticketTask, Task task, AccountUser user) {
		if(ticketTask ==null || task == null || user == null ) {
			return;
		}
		
		// Check if the task has humanPerformer
		String taskAssignee = task.getAssignee();
		if(StringUtils.isEmpty(taskAssignee)) {
			taskService.claim(task.getId(), String.valueOf(user.getId()));
			ticketTask.setStatus(TkmTicketTasks.task_status.Claimed.toString());
			ticketTask.setAssignee(user); 	// Add the assignee
			ticketTask.setQueue(null);		// Remove the group
		}
		
	}

	private Map<String, Object> getProcessVariableMap(TkmTickets tickets, String groupName, Map<String, String> ticketFormPropertiesMap) {
		Map<String, Object> map = new HashMap<String, Object>();

		AccountUser user = userService.findById(tickets.getRole().getId());
		if (user == null) {
			LOGGER.error("initiateProcess: user id is null");
			return map;
		}

		Integer moduleId  = tickets.getModuleId();
		map.put("groupName", groupName);
		map.put("requesterName", user.getUserName());
		map.put("moduleId", (moduleId == null) ? "" : moduleId.toString());
		
		Role role  = tickets.getUserRole();
		map.put("roleId", (role == null) ? "" : String.valueOf(role.getId()));
		map.put("ticketStatus", "New");
		
		if(null!=ticketFormPropertiesMap && !ticketFormPropertiesMap.isEmpty()){
			for (Entry<String, String> entry : ticketFormPropertiesMap.entrySet()) {
				map.put(entry.getKey(), entry.getValue());
			}	
		}
		
		return map;
	}
	
	@Override
	public List<Task> getTaskList(AccountUser user, String groupName, String processInstanceId) {
		List<Task> tasks = null;
		
		//If user is present check tasks for user
//		if(user != null) {
//			tasks = taskService.createTaskQuery().processInstanceId(processInstanceId)
//					.taskCandidateUser(user.getFullName())
//					.list();			
//		} else 
		if(!StringUtils.isEmpty(groupName)) {
			tasks = taskService.createTaskQuery().processInstanceId(processInstanceId)
					.taskCandidateGroup(groupName)
					.list();
		} else {
			//If group and user both are not available
			tasks = taskService.createTaskQuery().processInstanceId(processInstanceId)
					.list();
			if(tasks == null || tasks.isEmpty()) {
				//Check the tasks from sub-processes if any
				List<ProcessInstance> subProcList = runtimeService.createProcessInstanceQuery().superProcessInstanceId(processInstanceId).list();
				tasks = new ArrayList<>();
				for (ProcessInstance processInstance : subProcList) {
					List<Task> subTasks = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
					tasks.addAll(subTasks);
				}
			}
		}
		
		return tasks;
	}
	
	private boolean insertNextTask(Integer taskId) throws TKMException {
		boolean bStatus = false;
		LOGGER.debug("TkmActivitiServiceImpl.insertNextTask add new task for taskId: "+taskId);
		TkmTicketTasks task = tkmTicketTaskService.findByTaskId(taskId);
		if(task == null) {
			LOGGER.error("insertNextTask : Unable to insert new task for the task id: " + taskId);
			return bStatus;
		}
		
		bStatus = insertTask(task.getTicket(), task.getProcessInstanceId(), null, task.getAssignee().getId());
		return bStatus;
	}
	
	@Override
	public boolean claimTask(Integer taskId, String activitiTaskId, Integer userId) {
		boolean bStatus = false;
		
		//Check for the valid parameters
		if(StringUtils.isEmpty(activitiTaskId) || (userId==null)) {
			LOGGER.error("claimTask : Invalid parameters");
			return bStatus;
		}
		
			taskService.claim(activitiTaskId, userId.toString());		
			bStatus = true;
			LOGGER.info("claimTask: Task claimed for taskId: " + taskId + "with userId: " + userId);
		
		return bStatus;
	}
	
	@Override
	@Transactional
	public boolean completeTask(Integer taskId, String activitiTaskId, String comments, Map<String, Object> taskVariables, String processInstanceId) throws TKMException {
		boolean bStatus = false;
		
		//Check for the valid parameters
		if((taskId == null) || (taskId <= 0) || (taskVariables == null)) {
			LOGGER.error("completeTask : Invalid parameters");
			return bStatus;
		}
		
			LOGGER.debug("TkmActivitiServiceImpl.completeTask is called with taskId: " + taskId +" and activitiTaskId: " + activitiTaskId );
			taskVariables.put("comments", comments);

			TkmTicketTasks task = tkmTicketTaskService.findByTaskId(taskId);
			TkmTickets ticket = task.getTicket();
			if(task.getTaskName().equals("Verify Document")) {
				String documentId = getDocIdFromVerifyDocumentBPMN(ticket.getTkmWorkflows().getFilename(), task);
				if(!taskVariables.containsKey("docId")) {
					taskVariables.put("docId", documentId);
				}
				LOGGER.debug("completeTask::documentId = {}", documentId);
				List<TkmDocuments> tkmDocuments = ticketRepository.findDocumentsFromTicketID(ticket);
				List<String> consumerDocumentIds = new ArrayList<>();
				for(TkmDocuments document : tkmDocuments) {
					ConsumerDocument consumerDocument = ticketRepository.findConsumerDocumentsByECMID(document.getDocumentPath());
					consumerDocumentIds.add(String.valueOf(consumerDocument.getId()));
				}

				tkmMgmtService.verifyDocumentStatusChange((String) taskVariables.get("ticketStatus"), consumerDocumentIds, processInstanceId, activitiTaskId, comments, (String)taskVariables.get("taskRejectionReason"));
			}
			
			try {
				taskService.complete(activitiTaskId, taskVariables);	
			} catch (ActivitiObjectNotFoundException e) {
				LOGGER.error("Unable to complete the process with activitiTaskId: " + activitiTaskId, e);
			}
			

			LOGGER.info("completeTask: Task completed for tkm_task_id" + taskId);
			
			//insert Next task
			LOGGER.debug("TkmActivitiServiceImpl.insertNextTask add new task begins");
			bStatus = insertNextTask(taskId);
			
		
		return bStatus;
	}
	
	@Override
	public boolean isProcessComplete(String processInstanceId) {
		boolean bStatus = false;
		if(StringUtils.isEmpty(processInstanceId)) {
			LOGGER.error("isTaskProcessComplete: Invalid parameters");
			return bStatus;
		}
		
		Date date;
		try {
			HistoricProcessInstance historicProcessInstance = historyService
					.createHistoricProcessInstanceQuery()
					.processInstanceId(processInstanceId).singleResult();
			
			date = historicProcessInstance.getEndTime();
			bStatus = (date != null) ? true : bStatus;
			
			LOGGER.info("ProcessEnded on date: " + date);
		} catch (Exception e) {
			LOGGER.error("Unable to get the end time for process instance id: " + processInstanceId, e);
		}
		
		return bStatus;
	}
	
	@Override
	public boolean cancelProcess(String processInstanceId) {
		boolean bStatus = false;
		if(StringUtils.isEmpty(processInstanceId)) {
			LOGGER.error("cancelProcess: Invalid parameters");
			return bStatus;
		}
		
		try {
			runtimeService.deleteProcessInstance(processInstanceId, "Canceled");
			bStatus = isProcessComplete(processInstanceId);
		} catch (ActivitiObjectNotFoundException e) {
			LOGGER.error("Unable to cancel the process instance id: " + processInstanceId, e);
		}
		
		return bStatus;
	}
	
	@Override
	public String getTicketStatus(String processInstanceId) {
		String ticketStatus = null;
		String varName = "ticketStatus";
		
		boolean bStatus = isProcessComplete(processInstanceId);
		
		try {
			if(!bStatus) {
				//Process is not completed
				List<Execution> executions = runtimeService.createExecutionQuery().processInstanceId(processInstanceId).list();
				for (Execution exec : executions) {
					if(!exec.isEnded()) {
						ticketStatus = (String) runtimeService.getVariable(exec.getId(), varName);
						break;
					}
				}	
			} else {
				//Process is DONE, get value from history
				HistoricVariableInstance var = historyService.createHistoricVariableInstanceQuery()
						.processInstanceId(processInstanceId)
						.variableName(varName).singleResult();
				
				if(var != null) {
					ticketStatus = (String) var.getValue();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while getting the ticket status: ", e);
		}
		
		return ticketStatus;
	}
	
	/**
	 * 
	 * @param task
	 * @param ticket
	 * Updates the TkmTickets status of UnClaimed ticket to Open 
	 * When the first task assignee is read from BPM file
	 */
	private void openUnclaimedTicket(Task task, TkmTickets ticket) {
		if(!StringUtils.isEmpty(task.getAssignee()) 
				&& ticket.getStatus().equalsIgnoreCase(TkmTickets.ticket_status.UnClaimed.toString())) {
			ticket.setStatus(TkmTickets.ticket_status.Open.toString());
		}
	}
	
	@Override
	public boolean reassignActivitiTask(String activitiTaskId,String groupId, String assignee) {
		boolean bStatus= false; 
		Task task= taskService.createTaskQuery().taskId(activitiTaskId).singleResult();
		
		// Assign the ticket
		if (StringUtils.isEmpty(task.getAssignee())) {
			Integer userId = Integer.parseInt(assignee);
			bStatus = claimTask(null, activitiTaskId, userId);
			
		} else if(null != task) {
			// Reassign the ticket
			if (null != assignee) {
				task.setAssignee(assignee);
				task.setOwner(null);
			} else {
				task.setAssignee(null);
				task.setOwner(groupId);
			}
			taskService.saveTask(task);
			return true;
		}
				
		return bStatus;
	}
	
	/**
	 * Get the document Id for the verification workflow
	 * 
	 * @param workFlowName
	 * @param task
	 * @return
	 */
	@Override
	public String getDocIdFromVerifyDocumentBPMN(String workFlowName, TkmTicketTasks task) {
		String docId = null;
		
		if(task==null || StringUtils.isEmpty(workFlowName)) {
			LOGGER.error("Invalid parameters");
			return docId;
		}
		
			List<HistoricVariableInstance> historicVariableInstance = historyService
					.createHistoricVariableInstanceQuery()
					.processInstanceId(task.getProcessInstanceId()).list();
			for (HistoricVariableInstance formProperty : historicVariableInstance ) {
				if (formProperty.getValue() != null) {
					if (formProperty.getVariableName().equalsIgnoreCase("docId")) {
						docId = (String) formProperty.getValue();
						break;
					}

				}
			}
		
		return docId;
	}
	
	@Override
	public List<TaskFormProperties> getWorlFlowsMergedProperties(
			String workFlowName, Integer ticketId) throws TKMException {
			List<TkmTicketTasks> ticketTasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if(ticketTasks == null || ticketTasks.isEmpty()) {
			LOGGER.error("Unable to fetch the ticket tasks");
			return null;
		}
			
		TkmTicketTasks task = ticketTasks.get(ticketTasks.size() - 1);
		String processInstanceId = task.getProcessInstanceId();

		String processDefinitionId = null;

		ProcessDefinition processDefinition = repositoryService
				.createProcessDefinitionQuery().latestVersion()
				.processDefinitionKey(workFlowName).singleResult();

		/*
		 * if target workflow for reclassify does not exist it means the ticket
		 * will be reclassified to default workflow hence return empty list of
		 * properties.
		 */

		if (null != processDefinition) {
			processDefinitionId = processDefinition.getId();
		}

		else {

			return new ArrayList<TaskFormProperties>();
		}

		List<HistoricVariableInstance> historicVariableInstance = historyService
				.createHistoricVariableInstanceQuery()
				.processInstanceId(processInstanceId).list();

		List<FormProperty> formProperties = formService.getStartFormData(
				processDefinitionId).getFormProperties();

		return getMergedFormProperties(historicVariableInstance, formProperties);
	}
	
	/*
	 * the function returns the merged form properties for the existing work flow and the target reclassify work flow
	 */
	
	private List<TaskFormProperties> getMergedFormProperties(
			List<HistoricVariableInstance> historicVariableInstance,
			List<FormProperty> formProperties) {
		
		Map<String, TaskFormProperties> propertyNameValuesMap = new HashMap<String, TaskFormProperties>();//map to hold properties with name as key.
		
		Map<String, String> formPropertyIdName = new HashMap<String, String>();//map to hold all properties for new work flow. will be used to store only required properties
		
		List<String> propertyValues = new ArrayList<String>();
		
		for (FormProperty newFormProperty : formProperties) {

			String propertyName = newFormProperty.getName();
			String propertyId = newFormProperty.getId();
			String propertyType = null;
			if (newFormProperty.getType() instanceof EnumFormType) {
				propertyType = "enum";
				@SuppressWarnings(value = "unchecked")
				Map<String, String> propertiesMap = (Map<String, String>) newFormProperty
						.getType().getInformation("values");
				for (Entry<String, String> propertiesValue : propertiesMap
						.entrySet()) {
					propertyValues.add(propertiesValue.getValue());
				}
			} else {
				propertyType = "string";
				propertyValues = new ArrayList<String>();
			}

			TaskFormProperties taskFormProperties = new TaskFormProperties();
			taskFormProperties.setId(propertyId);
			taskFormProperties.setName(propertyName);
			taskFormProperties.setType(propertyType);
			taskFormProperties.setValues(propertyValues);

			formPropertyIdName.put(newFormProperty.getId(),
					newFormProperty.getName());

			propertyNameValuesMap.put(newFormProperty.getName(), taskFormProperties);
		}

		for (HistoricVariableInstance formProperty : historicVariableInstance ) {
			if (formProperty.getValue() != null) {
				String propertyName = null;
				String propertyId = null;
				
				//if new workflow has the same property then only the existing property is stored in the map
				if (formPropertyIdName.containsKey(formProperty
						.getVariableName())) {
					propertyName = formPropertyIdName.get(formProperty
							.getVariableName());
					propertyId = formProperty.getVariableName();
					String propertyType = formProperty.getVariableTypeName();
					propertyValues = new ArrayList<String>();
					propertyValues.add((String) formProperty.getValue());

					TaskFormProperties taskFormProperties = new TaskFormProperties();
					taskFormProperties.setId(propertyId);
					taskFormProperties.setName(propertyName);
					taskFormProperties.setType(propertyType);
					taskFormProperties.setValues(propertyValues);

					propertyNameValuesMap.put(propertyName, taskFormProperties);
				}

			}
		}

		return new ArrayList<TaskFormProperties>(propertyNameValuesMap.values());

	}

	@Override
	public Task getActivitiTaskById(String activitiTaskId) throws TKMException{
		Task task = taskService.createTaskQuery().taskId(activitiTaskId).singleResult();
		return task;
	}
		
}
