package com.getinsured.hix.tkm.service.jpa;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.tkm.activiti.service.TkmActivitiService;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmDocumentsRepository;
import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmCommentsService;
import com.getinsured.hix.tkm.service.TkmDocumentService;
import com.getinsured.hix.tkm.service.TkmMgmtService;
import com.getinsured.hix.tkm.service.TkmNotificationService;
import com.getinsured.hix.tkm.service.TkmQueueUsersService;
import com.getinsured.hix.tkm.service.TkmQueuesService;
import com.getinsured.hix.tkm.service.TkmTicketTaskService;
import com.getinsured.hix.tkm.service.TkmWorkflowsService;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.getinsured.hix.tkm.util.TkmRequestValidator;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

/**
 * @author hardas_d
 *
 */
@Service("tkmMgmtService")
public class TkmMgmtServiceImpl implements TkmMgmtService {

	@Autowired private TkmTicketTaskService tkmTicketTaskService;
	@Autowired private TkmActivitiService tkmActivitiService;
	@Autowired private TicketListService ticketListService;
	@Autowired private TkmWorkflowsService tkmWorkflowsService;
	@Autowired private UserService userService;
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private TkmCommentsService tkmCommentsService;
	@Autowired private TkmDocumentService tkmDocumentService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private  TkmQueuesService tkmQueuesService;
	@Autowired private TkmQueueUsersService tkmQueueUsersService;
	@Autowired private TkmNotificationService tkmNotificationService;
	@Autowired private HIXHTTPClient hIXHTTPClient;
	@Autowired private RoleService roleService;
	@Autowired private TkmRequestValidator tkmReqValidator;
	@Autowired private LookupService lookupService;
	@Autowired private AppEventService appEventService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private ITicketRepository ticketRepository;
	@Autowired private ITkmDocumentsRepository iTkmDocumentsRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmMgmtServiceImpl.class);
	private static final String ERR_MSG_NO_TICKET_TYPES_FOUND = "No Records for ticket type found";
	private static final String ERR_MSG_NO_QUEUES_FOUND = "No Records for queues found";
	private static final String ERR_MSG_ADDING_TICKET = "No ticket is added";
	private static final String ERR_MSG_PARAM_MISSING = "Required parameters are missing.";
	private static final String RECIPIENT ="recipient";
	private static final String SUBJECT ="subject";
	private static final String NAME ="name";
	private static final String TASKDETAIL ="taskDetail";
	private static final String DOCUMENT_TYPE = "documentType";
	private static final String ERR_MSG_UNABLE_GET_TICKET = "Unable to get the ticket id. ";
	private static final String DESC = "description";
	private static final String DOCUMENT_REJECT_SUBJECT = "Your document for RIDP manual verification was rejected ";
	private static final String DOCUMENT_APPROVED_SUBJECT = "Your RIDP Manual Verification was successfully completed ";
	private static final String DOCUMENT_CANCELED_COMMENT = "Verification was canceled.";
//	private static final String APPROVED = "Approved";
	private static final String ACCEPTED = "Accepted";
	private static final String TICKET_SUBJECT ="Ticket ";
	private static final String DEFAULT_WORKFLOW_NAME = "defaultProcess";
	
	private static final String APPEVENT_TICKET = "APPEVENT_TICKET";
	private static final String TICKET_NUMBER = "Ticket Number";
	private static final String TICKET_TYPE = "Ticket Type";
	private static final String TICKET_SUBTYPE = "Ticket Subtype";
	private static final String TICKET_WORKGROUP = "Ticket Workgroup";
	private static final String USER_ID ="userId";
	
	@Override
	@Transactional
	public boolean cancelTicketAndTasks(Integer ticketId, Integer userId, String comments,boolean reclassifyAction) throws TKMException
	{	LOGGER.info("======================== in cancelTicketAndTasks() =======================");
		boolean bStatus = false;
		
		List<TkmTicketTasks> tasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if(null != tasks) {
			if(tasks.isEmpty()) {
				LOGGER.error("Unable to get the task for the ticket");
				return bStatus;
			}
			String processInstanceId =null;
			for (TkmTicketTasks tkmTicketTasks : tasks) {
				if(!(tkmTicketTasks.getStatus().equalsIgnoreCase("Closed")||tkmTicketTasks.getStatus().equalsIgnoreCase("Canceled"))){
				processInstanceId	= tkmTicketTasks.getProcessInstanceId();
				break;
				}
			}
			
			// If the ticket is already cancelled and the status is made cancelled.
			if(!StringUtils.isEmpty(processInstanceId)) {
				processInstanceId = tasks.get(0).getProcessInstanceId();
			}
			
			if(!tkmActivitiService.isProcessComplete(processInstanceId)) {
				bStatus = tkmActivitiService.cancelProcess(processInstanceId);
			} else {
				LOGGER.info("The ticket is already in cancel mode");
				bStatus = true;
			}
			
			
			if(!bStatus) {
				LOGGER.error("Unable to cancel the ticket with id: " + ticketId);
				return bStatus;
			}
			
			//Change the status of the tasks if not already "Resolved"
			bStatus = changeTicketAndTaskStatus(tasks,ticketId,userId, comments,reclassifyAction);
			
			//String docId, String status, String comments	
			try {
				updateStatusforDocumentVerification(tasks.get(0), null, "REJECTED", DOCUMENT_CANCELED_COMMENT, "");
			} catch (NotificationTypeNotFound e) {
				LOGGER.error("Unable to update the document verification status");
			}
		}
		return bStatus;
	}
	
	@Override
	@Transactional
	public TkmTickets createTicketInDB(TkmTicketsRequest tickRequest) throws TKMException{
		LOGGER.info("======================== in createTicketInDB() =======================");
		TkmTickets addedTicket = null;
		
		//HIX-114372 : Adding 
		String category = tickRequest.getCategory();
		String type = tickRequest.getType();		

		TkmWorkflows tkmWorkflows = null;

		if(null != type && null != category) {
			tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria(type, category);
		} else {
			tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria(category);
		}

		if(tkmWorkflows == null) {
			LOGGER.error(ERR_MSG_NO_TICKET_TYPES_FOUND);
			return addedTicket;
		}
		
		TkmQueues group =tkmWorkflows.getTkmQueues();
		if(group == null) {
			LOGGER.error(ERR_MSG_NO_QUEUES_FOUND);
			return addedTicket;
		}
		
		TkmTickets ticket = fillTicketData(tickRequest, tkmWorkflows, group);		
		addedTicket = ticketListService.addNewTicket(ticket);
		if(addedTicket == null) {
			LOGGER.error(ERR_MSG_ADDING_TICKET);
			return addedTicket;
		}
		
		addedTicket = ticketListService.updateTicket("TIC-" + addedTicket.getId(), addedTicket.getId());
		if(addedTicket == null) {
			LOGGER.error(ERR_MSG_ADDING_TICKET);
			return addedTicket;
		}
		
		//Initiate the activiti framework to add tasks for the ticket
		Map<String, String> tickFormMap = getFormProperties(tickRequest, addedTicket);
		boolean bStatus = tkmActivitiService.initiateProcess(addedTicket, tkmWorkflows.getFilename(), group.getName(), tickFormMap);
		if(!bStatus) {
			LOGGER.error("Unable to add the task for the ticket created with id: " + addedTicket.getId());
		}
		return addedTicket;	
	}
	
	private Map<String, String> getFormProperties(TkmTicketsRequest ticketRequest, TkmTickets ticket) {
		Map<String, String> map = ticketRequest.getTicketFormPropertiesMap();
		 if(map == null) {
	        	map = new HashMap<>();
	        }
		if(("Complaint").equalsIgnoreCase(ticketRequest.getCategory())) {        
	       
	        
			map.put("complaintType", ticketRequest.getType());
			map.put("ticketNum", ticket.getNumber());
			map.put("denounceUserId", "1");
			
        } else if(ticket.getTkmWorkflows().getCategory().equalsIgnoreCase("Document Verification")&& !ticket.getTkmWorkflows().getType().equalsIgnoreCase("RIDP")){
        	map.put("caseNumber", ticketRequest.getCaseNumber());
        	map.put("redirectUrl", ticketRequest.getRedirectUrl());
        }else if(ticket.getTkmWorkflows().getCategory().equalsIgnoreCase("Document Verification")&& ticket.getTkmWorkflows().getType().equalsIgnoreCase("RIDP")){
        	map.put("docInfoUrl", ticketRequest.getRedirectUrl());
        } else if (("Individual Appeal").equalsIgnoreCase(ticketRequest.getType())) {
        	map.put("appealReason", ticketRequest.getIndAppealReason());
        }
        else if (("Qle Validation").equalsIgnoreCase(ticketRequest.getType())) {
        	map.put("ticketId", String.valueOf(ticket.getId()));
        }
		ticketRequest.setTicketFormPropertiesMap(map);
        
		
		return map;
	}
	
	private TkmTickets fillTicketData(TkmTicketsRequest ticketRequest, TkmWorkflows tkmWorkflows, TkmQueues group) {
		TkmTickets ticket = new TkmTickets();
		ticket.setTkmWorkflows(tkmWorkflows);
		ticket.setTkmQueues(group);
		
		String subject = ticketRequest.getSubject();
		ticket.setSubject(StringUtils.isEmpty(subject) ? ticketRequest.getType() : subject);
		
		String desc = ticketRequest.getDescription();
		ticket.setDescription(StringUtils.isEmpty(desc) ? ticketRequest.getComment() : desc);
		
		String priority = ticketRequest.getPriority();
		if(StringUtils.isEmpty(priority)) {
			priority = TkmTickets.TicketPriority.Medium.toString(); //default priority
		}
		ticket.setPriority(TkmTickets.TicketPriority.valueOf(priority).toString());
		
		setTicketUserData(ticketRequest, ticket);
		
		
		Role userRole = roleService.findById(ticketRequest.getUserRoleId());
		ticket.setUserRole(userRole);		
		ticket.setStatus(TkmTickets.ticket_status.UnClaimed.toString());
		ticket.setIsArchived("N");
		
		Calendar c = TSCalendar.getInstance();
		c.setTime(new TSDate()); // Now use today date.
		c.add(Calendar.DATE,tkmWorkflows.getExpDate()); // Adding 7 days
		Date dueDate =c.getTime();
        ticket.setDueDate(dueDate);
		
        if(ticketRequest.getModuleId() != null) { // Module id  
			ticket.setModuleId(ticketRequest.getModuleId());
		}
        
        if(!StringUtils.isEmpty(ticketRequest.getModuleName()) ) { // Module name
        	ticket.setModuleName(ticketRequest.getModuleName());
        }
        
		return ticket;
	}
	
	private void setTicketUserData(TkmTicketsRequest ticketRequest, TkmTickets ticket) {
		AccountUser user = new AccountUser();
		user.setId(ticketRequest.getCreatedBy()); // Creator
		ticket.setCreator(user);
		
		AccountUser updatedBy = new AccountUser();
		Integer updatorId = ticketRequest.getLastUpdatedBy();
		updatedBy.setId((updatorId != null) ? updatorId :  ticketRequest.getCreatedBy());
		ticket.setUpdator(updatedBy); // Updator
		
		AccountUser requestedBy = userService.findById(ticketRequest.getRequester());
		//requestedBy.setId(ticketRequest.getRequester()); // Role Id
		ticket.setRole(requestedBy);		
	}

	@Override
	@Transactional
	public boolean saveCommentsInDB(Integer ticketId, Integer creatorId, String strComment, boolean bUpdate) {
		LOGGER.info("======================== in saveCommentsInDB() =======================");
		boolean bStatus = false;
		if(StringUtils.isEmpty(strComment) || (ticketId <= 0) || (creatorId <= 0)) {
			LOGGER.error(ERR_MSG_PARAM_MISSING);
			return bStatus;
		}
		
		List<Comment> comments = null;
		AccountUser user = userService.findById(creatorId);
		if(user == null) {
			LOGGER.error("Unable to get the user");
			return bStatus;
		}
		
		Comment comment = new Comment();
		comment.setComment(strComment);
		comment.setComentedBy(user.getId());
		comment.setCommenterName(user.getFullName());
		
		CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(ticketId.longValue(), TargetName.TICKET);
		if(commentTarget != null) {
			comments = commentTarget.getComments();
		} else {
			commentTarget = new CommentTarget();
			commentTarget.setTargetId(ticketId.longValue());
			commentTarget.setTargetName(TargetName.TICKET);
			comments = new ArrayList<Comment>();
		}
	
		comment.setCommentTarget(commentTarget);
		comments.add(comment);
		commentTarget.setComments(comments);
		
			commentTarget = commentTargetService.saveCommentTarget(commentTarget);
			bStatus = true;
			
			if(!bUpdate) { // only add a new entry in tkm_comments table
				bStatus = tkmCommentsService.saveComments(ticketId, commentTarget);
			}
		
		return bStatus;
	}

	@Override
	@Transactional
	public boolean saveDocumentsInDB(Integer ticketId, Integer creatorId, List<String> docLinks, boolean bUpdate) throws TKMException {
		LOGGER.info("======================== in saveDocumentsInDB() =======================");
		boolean bStatus = false;
		if((docLinks.isEmpty())|| (ticketId <= 0) || (creatorId <= 0)) {
			LOGGER.error(ERR_MSG_PARAM_MISSING);
			return bStatus;
		}
		
		bStatus = tkmDocumentService.saveDocument(ticketId,creatorId, docLinks, bUpdate);
		return bStatus;
	}
	
	@Override
	@Transactional
	public String getTicketStatusFromDB(Integer ticketId) throws TKMException{
		String status = null;

		List<TkmTicketTasks> tasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if (tasks == null || tasks.isEmpty()) {
			LOGGER.error("No tasks found in ticket, status not implemented");
			return status;
		}

		//If task is cancelled set the status as cancelled
		if(tasks.get(0).getStatus().equalsIgnoreCase(TkmTicketTasks.task_status.Canceled.toString())) {
			status = TkmTicketTasks.task_status.Canceled.toString();
			return status;
		}
		
		// tasks available get status from Activiti framework
		String processInstanceId = tasks.get(0).getProcessInstanceId();
		status = tkmActivitiService.getTicketStatus(processInstanceId);

		return status;
	}
	
	@Override
	@Transactional
	public List<TkmTickets> searchTicketsByCriteria(TkmTicketsRequest ticketRequest) throws TKMException {
		List<TkmTickets> ticketList = null;
		if(ticketRequest == null) {
			return ticketList;
		}
		
		ticketList = ticketListService.searchTicketsByCriteria(ticketRequest);	
		return ticketList;
	}
	
	@Override
	@Transactional
	public void autoCancelProcess(String processInstanceId) throws TKMException {
		List <TkmTicketTasks> processInstanceTasks = tkmTicketTaskService.getProcessInstanceTaskList(processInstanceId);
		Integer adminUserId = userService.getAdminUsers().get(0);
		Integer ticketId = tkmTicketTaskService.getTaskTicketId(processInstanceTasks.get(0).getTaskId());
		changeTicketAndTaskStatus(processInstanceTasks, ticketId, adminUserId, null,false);
		tkmActivitiService.cancelProcess(processInstanceId);
		
	}
	
	private boolean  changeTicketAndTaskStatus(List<TkmTicketTasks> tasks,Integer ticketId,Integer userId, String comments, boolean reclassifyAction) throws TKMException{
		boolean bStatus = false;
		
		AccountUser assignee = new AccountUser();
		assignee.setId(userId);
		
		for (TkmTicketTasks task : tasks) {
			if(task.getStatus() != TkmTicketTasks.task_status.Closed.toString()) {
				task.setStatus(TkmTicketTasks.task_status.Canceled.toString());
				task.setAssignee(assignee);
			}
		}
		bStatus = tkmTicketTaskService.updateTasks(tasks);
		if(!bStatus) {
			LOGGER.error("Unable to cancel the ticket tasks with id: " + ticketId);
			return bStatus;
		}
		
		//Save the comments
		if(bStatus && (!StringUtils.isEmpty(comments))) { 
			saveCommentsInDB(ticketId, userId, comments, false);
		}
		
		TkmTickets ticket =reclassifyAction?ticketListService.findTkmTicketsById(ticketId):ticketListService.updateStatus(ticketId, TkmTickets.ticket_status.Canceled.toString(), userId, comments); 
		bStatus = (ticket != null) ? true : false;
		
		return bStatus;
	}
	
	private boolean  changeTaskQueueId(Integer ticketId,TkmQueues group) throws TKMException{
		boolean bStatus = false;
		
		List<TkmTicketTasks> tasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if(tasks.isEmpty()) {
			LOGGER.error("Unable to get the task for the ticket");
			return bStatus;
		}
		
		for (TkmTicketTasks task : tasks) {
				task.setQueue(group);
		}
		bStatus = tkmTicketTaskService.updateTasks(tasks);
		if(!bStatus) {
			LOGGER.error("Unable to cancel the ticket tasks with id: " + ticketId);
			return bStatus;
		}
		
	
		return bStatus;
	}
	
	
	/**
	 * Version 1.0
	 * 
	 * @author hardas_d
	 * @param status
	 * @param moduleId
	 * Version compatibility function for MS3 to do the BPM operations.
	 * 
	 */
	@Override
	public void statusChangeNotifyShop(String status, String moduleId) {
		LOGGER.info("Notify shop with " + status + " moduleId: " +moduleId);
		 
		//Ticket can be raised by any other entity like CSR or Agent for Employer
		if(StringUtils.isEmpty(moduleId)) {
			LOGGER.error("Module Id or Role ID missing for the ticket");
			return;
		}
		
		Integer roleId=roleService.findRoleByName(RoleService.EMPLOYER_ROLE).getId();
		statusChangeNotifyShop(status, moduleId, roleId.toString());
	}
	
	/**
	 * Version 2.0
	 * 
	 * @author hardas_d
	 * @param status
	 * @param moduleId
	 * This method will be called from the AppealRequest BPM XML to notify the SHOP module for the status change.
	 * The status values would be NEEDINFO, APPROVED, REJECTED
	 */
	@Override
	public void statusChangeNotifyShop(String status, String moduleId, String roleId) {
		LOGGER.info("Notify shop with " + status + " moduleId: " +moduleId);
		
		//Changed for HIX-23341 : ModuleId to be send instead of userID. 
		//Ticket can be raised by any other entity like CSR or Agent for Employer
		if(StringUtils.isEmpty(moduleId) || StringUtils.isEmpty(roleId)) {
			LOGGER.error("Module Id or Role ID missing for the ticket");
			return;
		}
		
		String enc = "UTF-8";
		StringBuilder sb = new StringBuilder();
		try {
			long noOfTickets = ticketListService.getCntByModuleAndRoleId(Integer.parseInt(moduleId), Integer.parseInt(roleId));
			
			sb.append("?");
			sb.append(URLEncoder.encode("moduleUserId", enc)).append('=').append(URLEncoder.encode(moduleId, enc));
			sb.append("&");
			sb.append(URLEncoder.encode("appealStatus", enc)).append('=').append(URLEncoder.encode(status.toUpperCase(), enc));
			sb.append("&");
			sb.append(URLEncoder.encode("noOfTickets", enc)).append('=').append(URLEncoder.encode(String.valueOf(noOfTickets), enc));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Enable to encode the url params", e);
			return;
		}
		
		LOGGER.info("Query string: "+sb.toString());
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		try {
			String get_resp = restTemplate.getForObject(ShopEndPoints.SEND_EMPLOYER_APPEAL_NOTIFICATION+sb.toString(), String.class);
			ShopResponse response = (ShopResponse) xstream.fromXML(get_resp);
			LOGGER.info("Shop status notification response: " + response.getStatus());
		} catch (Exception e) {
			LOGGER.error("Unable the receive the valid response", e);
		}
		
	}

	@Override
	public void verifyDocumentStatusChange(String status, List<String> documentIds,
			String processInstanceId, String activitiTaskId, String comments, String taskRejectionReason) {
			LOGGER.info("Notify document with :" + status + " DocId : " + documentIds.toString() + " activitiTask Id:" + activitiTaskId +
					" processId:" + processInstanceId);

		try {
			TkmTicketTasks task = tkmTicketTaskService.getTicketByTaskIdAndActivitiId(processInstanceId, activitiTaskId);
			updateStatusforDocumentVerification(task, documentIds, status, comments, taskRejectionReason);
		} catch (Exception e) {
			LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
		}
	}
	

	@Override
	public void verifyDocumentStatusChange(String status, String documentId,
			String processInstanceId, String activitiTaskId, String comments) {
			String taskRejectionReason = "";
			LOGGER.info("Notify document with :" + status + " DocId : " + documentId + " activitiTask Id:" + activitiTaskId +
					" processId:" + processInstanceId);

		try {
			List<String> documentIds = new ArrayList<String>();
			documentIds.add(documentId);
			TkmTicketTasks task = tkmTicketTaskService.getTicketByTaskIdAndActivitiId(processInstanceId, activitiTaskId);
			updateStatusforDocumentVerification(task, documentIds, status, comments, taskRejectionReason);
		} catch (Exception e) {
			LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
		}
	}
	
	/**
	 * This will be used to update the status of the document. 
	 * Also will be called from BPMN and Cancel ticket API
	 * 
	 * @param task
	 * @param docId
	 * @param status
	 * @param comments
	 * @throws NotificationTypeNotFound
	 */
	private void updateStatusforDocumentVerification(TkmTicketTasks task, List<String> documentIds, String status, String comments, String taskRejectionReason)
			throws NotificationTypeNotFound {
		TkmTickets ticket = task.getTicket();

		if (("Document Verification").equalsIgnoreCase(ticket.getTkmWorkflows().getCategory())) {
			
			//Get the doc id - when cancel ticket is done docId would not be available.
			if(documentIds == null || documentIds.isEmpty()) {
				List<TkmDocuments> tkmDocuments = ticketRepository.findDocumentsFromTicketID(ticket);
				List<String> consumerDocumentIds = new ArrayList<>();
				for(TkmDocuments document : tkmDocuments) {
					ConsumerDocument consumerDocument = ticketRepository.findConsumerDocumentsByECMID(document.getDocumentPath());
					consumerDocumentIds.add(String.valueOf(consumerDocument.getId()));
				}
				documentIds = consumerDocumentIds;
			}
			
			if (("Referral Identity Verification").equalsIgnoreCase(ticket.getTkmWorkflows().getType())) {
				for(String docId : documentIds) {
					updateCMRDocumentForReferralActivation(docId, ACCEPTED.equalsIgnoreCase(status) ? "Y" : "N", comments);
				}
			} else {
				sendVerifyDocumentEmail(task, ticket, status.toLowerCase(), comments, taskRejectionReason);
				for(String docId : documentIds) {
					updateCMRDocument(docId, ACCEPTED.equalsIgnoreCase(status) ? "Y" : "N", comments);
				}
			}
		}
	}

	private void updateCMRDocument(String cmrDocumentId, String accepted, String comments) {
		
		if(!StringUtils.isEmpty(cmrDocumentId)){
			try{
				Map<String,Object > requestPayLoad = new HashMap<>();
				requestPayLoad.put("cmrDocumentId", Long.parseLong(cmrDocumentId));
				requestPayLoad.put("comments", comments);
				requestPayLoad.put("accepted", accepted);
		        hIXHTTPClient.getPOSTData(GhixEndPoints.CONSUMER_SERVICE_URL+"consumerDocument/update", JacksonUtils.getJacksonObjectWriterForHashMap(Integer.class, Object.class).writeValueAsString(requestPayLoad), "application/json");
			}catch(Exception e){
				LOGGER.error("Unable to update RIDP Document verified status", e);
			}
		}
	}
	
	
	  private void updateCMRDocumentForReferralActivation(String cmrDocumentId, String accepted, String comments) {
          
          if(!StringUtils.isEmpty(cmrDocumentId)){
                 try{
                      
                       Map<String,Object > requestPayLoad = new HashMap<>();
                       requestPayLoad.put("cmrDocumentId", Long.parseLong(cmrDocumentId));
                       requestPayLoad.put("comments", comments);
                       requestPayLoad.put("accepted", accepted);
                  hIXHTTPClient.getPOSTData(GhixEndPoints.ELIGIBILITY_URL+"referral/activation/updateDocumentStatus", JacksonUtils.getJacksonObjectWriterForHashMap(Integer.class, Object.class).writeValueAsString(requestPayLoad), "application/json");
                 }catch(Exception e){
                       LOGGER.error("Unable to update RIDP Document verified status", e);
                 }
          }
   }
	
	/**
	 * 
	 * @author hardas_d
	 * @param ticketId
	 * @param userId
	 * @param taskVariables
	 * @return true if the actions are performed successfully.
	 * This method will auto claim and complete the active tasks for the ticket specified by using the userId and variables
	 * @throws Exception 
	 */
	@Override
	public boolean autoClaimCompleteTasks(Integer ticketId, Integer userId, String comments, Map<String, Object> taskVariables) throws TKMException{
		boolean bStatus = false;
		List<TkmTicketTasks> tasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if(tasks == null) {
			LOGGER.error("Ticket does not contains any tasks");
			return bStatus;
		}
				
		for (TkmTicketTasks task : tasks) {
			//Validate the task for the user
			if(valTaskForUser(task, userId)) {
				
				//Claim the task only if the status is 'Unclaimed'
				if(task.getStatus().equalsIgnoreCase(TkmTicketTasks.task_status.UnClaimed.toString())) {
					bStatus = claimTicketTask(task.getTaskId(), task.getActivitiTaskId(), userId);
					if(!bStatus) {
						return bStatus;
					}
				}
				
				//Complete the ticket tasks
				if(task.getStatus().equalsIgnoreCase(TkmTicketTasks.task_status.Claimed.toString())) {
					Map<String, Object> vars = (taskVariables != null) ? taskVariables : new HashMap<String, Object>(); 
					bStatus = completeTicketTask(task.getTaskId(), task.getActivitiTaskId(), userId, comments, vars, true, task.getProcessInstanceId());
				}
			}
		}
		
		String processInstanceId = tasks.get(0).getProcessInstanceId();
		if(tkmActivitiService.isProcessComplete(processInstanceId)) {
			ticketListService.updateStatus(ticketId, TkmTickets.ticket_status.Resolved.toString(), userId, null);
		}
		return bStatus;
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @return boolean
	 * @param task
	 * @param userId
	 * @return true if the user is valid to perform action on the task
	 */
	private boolean valTaskForUser(TkmTicketTasks task, Integer userId) {
		boolean bStat = false;
		
		AccountUser user = task.getAssignee();
		if(user == null) {
			TkmQueues queue = task.getQueue();			
			if(queue == null) {
				return bStat;
			}
			
			List<Integer> groupUsers = tkmQueueUsersService.getUserQueueId(userId);			
			bStat = ((groupUsers != null) && (!groupUsers.isEmpty()) && (groupUsers.contains(queue.getId())));			
		} else {
			bStat = (user.getId() == userId);
		}
	
		return bStat;
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param taskId
	 * @param activitiTaskId
	 * @param userId
	 * @return true if the task is claimed by the userId specified
	 * This method will claim the task in activiti framework and TkmTicketTask
	 * @throws Exception 
	 */
	@Override
	public boolean claimTicketTask(Integer taskId, String activitiTaskId, Integer userId) throws TKMException {
		boolean bStatus = false;
		
		bStatus = tkmActivitiService.claimTask(taskId, activitiTaskId, userId);
		bStatus = (bStatus) ? tkmTicketTaskService.claimTicket(taskId, userId) : bStatus;
		
		return bStatus;
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param taskId
	 * @param activitiTaskId
	 * @param comments
	 * @param taskVariables
	 * @param bAutoComplete If auto complete then the comments will not be added into the comments target 
	 * as it already done in updateTicket
	 * @return true if task is completed.
	 * This method will complete the task from activiti framework and TkmTicketTask
	 * @throws Exception 
	 */
	@Transactional
	@Override
	public boolean completeTicketTask(Integer taskId, String activitiTaskId, Integer userId,
			String comments, Map<String, Object> taskVariables, boolean bAutoComplete, String processInstanceId) throws TKMException{
		boolean bStatus = false;
		LOGGER.debug("CompleteTicketTask is called with taskId: "+taskId +" and activitiTaskId: "+activitiTaskId );
		bStatus = tkmActivitiService.completeTask(taskId, activitiTaskId, comments, taskVariables, processInstanceId);
		bStatus = (bStatus) ? tkmTicketTaskService.completeTask(taskId, comments) : bStatus;
		LOGGER.debug("Ticket and activiti task complete call ends");
		
		//Save the comments to comment target plugin
		if((!bAutoComplete) && bStatus && (!StringUtils.isEmpty(comments))) {
			TkmTicketTasks task = tkmTicketTaskService.findByTaskId(taskId);
			bStatus = saveCommentsInDB(task.getTicket().getId(), userId, comments, false);
		}
		
		return bStatus;
	}	
	@Override
	public boolean reassignTkkActiveTask(Integer taskId,Integer groupId,Integer userId,String ticketId) throws TKMException{
		TkmTicketTasks ticketTasks =tkmTicketTaskService.findByTaskId(taskId);
		String assignee=null;
		
		TkmQueues tkmQueues = null;
		if(null!=groupId){
			tkmQueues = tkmQueuesService.findById(groupId);
		}
		ticketTasks.setQueue(tkmQueues);
		
		AccountUser user = null;
		if(null!=userId){
			user = userService.findById(userId);
			ticketTasks.setAssignee(user);
			assignee=userId.toString();
		}
		
		TkmTickets tickets = ticketListService.findTkmTicketsById(Integer.parseInt(ticketId));
		
		if(null!=tkmQueues){
			tickets.setTkmQueues(tkmQueues);
		}
		if(null!=user){
			tickets.setUpdator(user);
		}
		TkmTickets savedTickets = ticketListService.saveEditedTicket(tickets);
		
		ticketTasks.setTicket(savedTickets);
		
		boolean bStat = false;
		TkmTicketTasks tkmTicketTasks = tkmTicketTaskService.addNewTicketTask(ticketTasks);
		if(tkmTicketTasks == null) {
			LOGGER.error("Unable to save the reassigned ticket task");
			return bStat;
		}
		try{
		bStat = tkmActivitiService.reassignActivitiTask(ticketTasks.getActivitiTaskId(), groupId.toString(), assignee);
		}catch(Exception e)
		{
			throw new TKMException(e);
		}
			if(!bStat) {
			LOGGER.error("Unable to ressign the ticket task");
		}
		
		return bStat;		
	}

	/**
	 * @author hardas_d
	 * @param queueId
	 * @param userList
	 * @return The status of the transaction on the DB to update the userlist for the specified queue/group id
	 * @throws Exception 
	 */
	@Override
	public boolean updateQueueUsers(Integer queueId, List<Integer> userList,boolean addFlag) throws TKMException {
		boolean bStatus = false;
		
		bStatus = tkmQueueUsersService.updateQueueUsers(queueId, userList,addFlag);
		return bStatus;
	}

	@Override
	public void sendTicketResolvedEmail(Integer ticketId, String approveStatus, String comment) throws NotificationTypeNotFound {
		String exchangeType = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		String stateCode = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		TkmTickets tickets = null;
		AccountUser user = null, requester = null;
		try {
			tickets = ticketListService.findTkmTicketsById(ticketId);
		} catch (Exception e) {
			LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
		}
		if (null != tickets) {
			user = userService.findById(tickets.getRole().getId());
			requester = userService.findById(tickets.getCreator().getId());
		}
		String subject = "";
		if ("STATE".equalsIgnoreCase(exchangeType) && "ID".equalsIgnoreCase(stateCode)) {
			String exchangeName = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			subject = "This is an AUTOMATED update from " + exchangeName + " Customer Support";
		} else {
			subject = TICKET_SUBJECT + tickets.getNumber() + " resolved ";
		}

		if (user != null) {
			if(approveStatus.equals("Rejected") || approveStatus.equals("Denied")) {
				tickets.getTkmtickettasks().get(0).setStatus("Rejected");
			}
			sendTicketStatusUpdateEmailToUser(tickets, user, subject, false);
		}

		if (requester != null && user.getId()!=requester.getId()) {
			if(approveStatus.equals("Rejected") || approveStatus.equals("Denied")) {
				tickets.getTkmtickettasks().get(0).setStatus("Rejected");
			}
			if(!StringUtils.isEmpty(comment)) {
				tickets.setClosingComment(comment);
			}
			sendTicketStatusUpdateEmailToUser(tickets, requester, subject, true);
		}

	}

	private void sendTicketStatusUpdateEmailToUser(TkmTickets tickets, AccountUser user, String subject, boolean isRequester)  throws NotificationTypeNotFound {
		Map<String, String> emailTokens = new HashMap<String, String>();
		emailTokens.put(SUBJECT, subject);
		emailTokens.put(RECIPIENT, user.getEmail());
		emailTokens.put(NAME, user.getFirstName() + " " + user.getLastName());
		emailTokens.put(TASKDETAIL, "");
		emailTokens.put(USER_ID, String.valueOf(user.getId()));
		emailTokens.put("isRequster", "false");
		if(tickets.getTkmtickettasks()!=null && tickets.getTkmtickettasks().size()>0 && 
				!StringUtils.isEmpty(tickets.getTkmtickettasks().get(0).getStatus()) 
				&& tickets.getTkmtickettasks().get(0).getStatus().equals("Rejected")) {
			emailTokens.put("status", "rejected");
		}
		if(isRequester) {
			emailTokens.put("isRequster", "true");
			emailTokens.put("TicketNumber", tickets.getNumber());
			emailTokens.put("TicketCategory", tickets.getTkmWorkflows().getCategory());
			emailTokens.put("subCategory", tickets.getTkmWorkflows().getType());
			emailTokens.put("CreationDate", tickets.getCreated().toString());
			emailTokens.put("createdFor", tickets.getRole().getFirstName()+" "+tickets.getRole().getLastName());
			if(TkmTickets.ticket_status.valueOf(tickets.getStatus())==TkmTickets.ticket_status.UnClaimed)
				emailTokens.put("status", "received");
			if(TkmTickets.ticket_status.valueOf(tickets.getStatus())==TkmTickets.ticket_status.Open)
				emailTokens.put("status", "open");
			if(TkmTickets.ticket_status.valueOf(tickets.getStatus())==TkmTickets.ticket_status.Resolved)
				emailTokens.put("status", "resolved");
			if(TkmTickets.ticket_status.valueOf(tickets.getStatus())==TkmTickets.ticket_status.Canceled)
				emailTokens.put("status", "cancelled");

			if(tickets.getTkmtickettasks()!=null && tickets.getTkmtickettasks().size()>0 && 
					!StringUtils.isEmpty(tickets.getTkmtickettasks().get(0).getStatus()) 
					&& tickets.getTkmtickettasks().get(0).getStatus().equals("Rejected")) {
				emailTokens.put("status", "rejected");
				if(!StringUtils.isEmpty(tickets.getClosingComment())) {
					emailTokens.put("rejectionReason", tickets.getClosingComment());
				}
			}
			if(!emailTokens.containsKey("rejectionReason") && !StringUtils.isEmpty(tickets.getClosingComment())) {
				emailTokens.put("comment", tickets.getClosingComment());
			}
			
			emailTokens.put("fName", user.getFirstName());
			emailTokens.put("lName", user.getLastName());
			if(tickets.getTkmWorkflows().getCategory().equals("Document Verification"))
			{
				emailTokens.put("ticket", "Document");
			} else {
				emailTokens.put("ticket", "Ticket");
			}
			
		}
		tkmNotificationService.sendEmailNotification(emailTokens, tickets);
	}
	
	@SuppressWarnings("all")
	private void sendVerifyDocumentEmail(TkmTicketTasks task, TkmTickets ticket, String documentStatus, String comments, String taskRejectionReason) throws NotificationTypeNotFound {
		Map<String,String> emailTokens = new HashMap<String, String>();
		  AccountUser user = userService.findById(ticket.getRole().getId());
		  String subject =null;
		  if(ticket.getTkmWorkflows().getCategory().equalsIgnoreCase("Document Verification")&&ticket.getTkmWorkflows().getType().equalsIgnoreCase("RIDP")){
			subject = documentStatus.equalsIgnoreCase(ACCEPTED) ? DOCUMENT_APPROVED_SUBJECT:DOCUMENT_REJECT_SUBJECT;	
			if(!documentStatus.equalsIgnoreCase(ACCEPTED)) {
				emailTokens.put("ticketDesc", comments);
			}
		  }
		  else{
			  subject = TICKET_SUBJECT+ ticket.getNumber()+" document(s) for "+ticket.getTkmWorkflows().getType()+" has been "+ documentStatus;
		  }
		  
		  emailTokens.put(RECIPIENT, user.getEmail());
		  emailTokens.put(SUBJECT, subject);
		  emailTokens.put(NAME, user.getFirstName()+" "+user.getLastName());
		  emailTokens.put(TASKDETAIL,"");
		  emailTokens.put(DOCUMENT_TYPE, "documentVerification");
		  emailTokens.put("documentStatus", documentStatus);
		  emailTokens.put("subType", ticket.getTkmWorkflows().getType());
		  emailTokens.put(USER_ID, String.valueOf(user.getId()));

		  if(StringUtils.isNotBlank(taskRejectionReason) && StringUtils.isNotBlank(documentStatus) && !documentStatus.equalsIgnoreCase(ACCEPTED)) {
			emailTokens.put("taskRejectionReason", taskRejectionReason);
		  }
		  
		  tkmNotificationService.sendEmailNotification(emailTokens,ticket);  
	}

	@Override
	public void sendTicketCancelledEmail(Integer ticketId)
			throws NotificationTypeNotFound {
		
		TkmTickets tickets = null;
		AccountUser user=null, requester = null;
		try {
			tickets = ticketListService.findTkmTicketsById(ticketId);
			user = userService.findById(tickets.getRole().getId());
			requester = userService.findById(tickets.getCreator().getId());
		} catch (Exception e) {
			LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
		}
		String subject = TICKET_SUBJECT + tickets.getNumber() + " cancelled ";
		if (null != tickets && (user !=null)) {			
			sendTicketStatusUpdateEmailToUser(tickets, user, subject, false);
		}
		if (null != tickets && (requester !=null) && requester.getId()!=user.getId()) {			
			sendTicketStatusUpdateEmailToUser(tickets, requester, subject, true);
		}
	}

		
	@Override
	 public void sendTicketCreateEmail(Integer ticketId) throws NotificationTypeNotFound {
		String exchangeType=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		String stateCode=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	  
		  TkmTickets tickets = null;
		  AccountUser user=null, requester=null;
		  try {
		   tickets = ticketListService.findTkmTicketsById(ticketId);
		  } catch (Exception e) {
		   LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
		  }
		  if(null!=tickets){
		   user =  userService.findById(tickets.getRole().getId());
		   requester =  userService.findById(tickets.getCreator().getId());
		  }
		 // String subject =TICKET_SUBJECT+ tickets.getNumber()+" created "; 		  
		  String subject = null;
		  if("STATE".equalsIgnoreCase(exchangeType) && "ID".equalsIgnoreCase(stateCode))
			{
				String exchangeName=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
				subject= "This is an AUTOMATED response from "+ exchangeName+ " Customer Support";
			}
		  else
		  {
			  subject =TICKET_SUBJECT+ tickets.getNumber()+" created ";  
		  }
		 
		  if(user != null) {
			  sendTicketStatusUpdateEmailToUser(tickets, user, subject, false);
		  }
		  if(requester != null && user.getId()!=requester.getId()) {
			  sendTicketStatusUpdateEmailToUser(tickets, requester, subject, true);
		  }
	 }
	
	/**
	 * @author hardas_d
	 * @throws NotificationTypeNotFound
	 * This function will initiate the notification email to be send when the ticket status is changed to 'Open' 
	 */
	@Override
	public boolean sendTicketOpenEmail(Integer ticketId) throws NotificationTypeNotFound {
		LOGGER.info("Start sending email for the ticket status changed to Open");
		boolean bStatus = false;
		
		if(ticketId == null) {
			LOGGER.error("Invalid params in sendTicketOpenEmail");
			return bStatus;
		}
		
		//Get the ticket data
		TkmTickets ticket = null;
		AccountUser user = null, requester = null;
		try {
			ticket = ticketListService.findTkmTicketsById(ticketId);
			user = userService.findById(ticket.getRole().getId());
			requester = userService.findById(ticket.getCreator().getId());
		} catch (Exception e) {
			LOGGER.error("Unable to get the ticket data. ", e);
			return bStatus;
		}
		
		//Create the content of an email
		
		//Initiate sending an email

		String subject =TICKET_SUBJECT+ ticket.getNumber()+" opened "; 
		if(user!=null) {
			sendTicketStatusUpdateEmailToUser(ticket, user, subject, false);
		}
		if(requester!=null && user.getId()!=requester.getId()) {
			sendTicketStatusUpdateEmailToUser(ticket, requester, subject, true);
		}
		LOGGER.info("Email Sent for the ticket status changed to Open");
		bStatus = true;
		
		return bStatus;
	}
	
	
	/*@Override
	public TkmTickets reClassifyTicketWorkFlow(Integer ticketId,String category,String type, Map<String, String> ticketFormPropertiesMap) throws TKMException {
		
		TkmWorkflows tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria(type, category);	
		
		
		TkmTickets addedTicket=ticketListService.findTkmTicketsById(ticketId);
		
		if(tkmWorkflows == null) {
			LOGGER.error(ERR_MSG_NO_TICKET_TYPES_FOUND);
		}
		TkmQueues group =tkmWorkflows.getTkmQueues();
		boolean bStatus = tkmActivitiService.initiateProcess(addedTicket, tkmWorkflows.getFilename(), group.getName(), ticketFormPropertiesMap);
		
		//Update the ticket object with new workflow properties
		if(bStatus){
			changeTaskQueueId(ticketId,group);
			addedTicket.setTkmWorkflows(tkmWorkflows);
			addedTicket.setTkmQueues(tkmWorkflows.getTkmQueues());
			Calendar c = TSCalendar.getInstance();
			c.setTime(new TSDate()); // Now use today date.
			c.add(Calendar.DATE,tkmWorkflows.getExpDate()); 
			Date dueDate =c.getTime();
			addedTicket.setDueDate(dueDate);

			return ticketListService.saveEditedTicket(addedTicket);
		}
			
		return null;
	}*/
	
	
	
	@Override
	public TkmTickets reClassifyTicketWorkFlow(Integer ticketId,String category,String type, 
			Map<String, String> ticketFormPropertiesMap, Integer userId) throws TKMException {
		
		TkmWorkflows tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria(type, category);	
		TkmTickets addedTicket=ticketListService.findTkmTicketsById(ticketId);
		TkmTickets updatedTicket = null;
		
		if(tkmWorkflows == null) {
			LOGGER.error(ERR_MSG_NO_TICKET_TYPES_FOUND);
			return updatedTicket;
		}
		
		TkmQueues group =tkmWorkflows.getTkmQueues();
		boolean bStatus = tkmActivitiService.initiateProcess(addedTicket, tkmWorkflows.getFilename(), group.getName(), ticketFormPropertiesMap);
		
		
		//Update the ticket object with new workflow properties
		if(bStatus){
			changeTaskQueueId(ticketId,group);
			addedTicket.setTkmWorkflows(tkmWorkflows);
			addedTicket.setTkmQueues(tkmWorkflows.getTkmQueues());
			Calendar c = TSCalendar.getInstance();
			c.setTime(new TSDate()); // Now use today date.
			c.add(Calendar.DATE,tkmWorkflows.getExpDate()); 
			Date dueDate =c.getTime();
			addedTicket.setDueDate(dueDate);

			updatedTicket = ticketListService.saveEditedTicket(addedTicket);
			
			// if ticket is OPEN, then claim first task and assign it logged-in user
			if(updatedTicket != null && (TkmTickets.ticket_status.Open.toString()).equalsIgnoreCase(updatedTicket.getStatus())) {
				bStatus = claimTasks(updatedTicket.getId(), userId);
				
				if(!bStatus) {
					LOGGER.error("Unable to claim the task after reclassify.");
				}
			}
		}
		
		return updatedTicket;
	}
	
	/**
	 * Claim the ticket silently. No email/notification will be triggered.
	 * This feature will be used for re-clasify or auto-claiming the ticket.
	 * 
	 * @param ticketId
	 * @param userId
	 * @return
	 * @throws TKMException
	 */
	private boolean claimTasks(Integer ticketId, Integer userId) throws TKMException{
		boolean bStatus = false;
		List<TkmTicketTasks> tasks = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
		if(tasks == null) {
			LOGGER.error("Ticket does not contains any tasks");
			return bStatus;
		}
				
		for (TkmTicketTasks task : tasks) {
			//Claim the task only if the status is 'Unclaimed'
			if(task.getStatus().equalsIgnoreCase(TkmTicketTasks.task_status.UnClaimed.toString())) {
				bStatus = claimTicketTask(task.getTaskId(), task.getActivitiTaskId(), userId);
				if(!bStatus) {
					return bStatus;
				}
			}
		}
		
		bStatus = true;
		return bStatus;
	}
	
	
	
	
	@Override
	public boolean sendEmailByTicketType(String ticketStatusValue,String emailConfigValue) {
		//Map<String, String> map = new HashMap<String, String>();
		Map<String, String> map = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		boolean emailConfigFlag = false;
	
		try{
		map = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, String.class).readValue(emailConfigValue);
		String emailFlagValue = map.get(ticketStatusValue);
		
		if(StringUtils.isBlank(emailFlagValue)) {
			emailFlagValue = map.get(ticketStatusValue.toLowerCase());
		}
		emailConfigFlag = Boolean.parseBoolean(emailFlagValue);
		}catch(Exception e)
		{
			LOGGER.error("exception occurred while sending mail", e);
			e.getMessage();
		}

		return emailConfigFlag;
	}

	/* Complaint workflow functions starts */
	/**
	 * @author hardas_d
	 * @since Apr 14, 2014
	 * @param ticketNum
	 * @param moduleId
	 * @return
	 * This method is a expression from Complaint workflow to reclassify the complaint ticket to Appeal ticket.
	 */
	@Override
	public void reClassifyToAppeal(String ticketNum) {	
		// Validate the params
		if(StringUtils.isEmpty(ticketNum) ) {
			LOGGER.error(ERR_MSG_PARAM_MISSING);
			return;
		}
		
		try {
			
			TkmQueues queues = tkmQueuesService.findByName("Appeal Queue");
			TkmTickets tickets = ticketListService.findByTicketNumber(ticketNum);
			tickets.setTkmQueues(queues);
			TkmWorkflows tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria("Individual Appeal", "Request");
			tickets.setTkmWorkflows(tkmWorkflows);
			tickets = ticketListService.saveEditedTicket(tickets);
			
		} catch (Exception e) {
			LOGGER.error("Error classifying complaint to appeal", e);
		}		
	}

	/** 
	 * @author hardas_d
	 * @since Apr 14, 2014
	 * @param ticketNum
	 * @param denounceUserId
	 * @return void
	 * This method sends the notification to the user against whom the complaint is lodged. 
	 */
	@Override
	public void sendMailToDenounceUser(String ticketNum, String denounceUserId) {
		try {
			TkmTickets ticket = ticketListService.findByTicketNumber(ticketNum);
			
			//Create the content of an email
			String subject =TICKET_SUBJECT+ ticket.getNumber()+" complaint has been lodged."; 
			AccountUser user = userService.findById(Integer.parseInt(denounceUserId));
			
			//Tokens for sending email
			Map<String,String> emailTokens = new HashMap<String, String>();
			emailTokens.put(RECIPIENT, user.getEmail());
			emailTokens.put(SUBJECT, subject);
			emailTokens.put(NAME, user.getFirstName()+" "+user.getLastName());
			emailTokens.put(TASKDETAIL,"");
			emailTokens.put(DESC, ticket.getDescription());
			emailTokens.put(DESC, ticket.getDescription());
			emailTokens.put(USER_ID, String.valueOf(user.getId()));
			
			//Initiate sending an email
			tkmNotificationService.sendEmailNotification(emailTokens,ticket);	
			LOGGER.info("Complaint notification sent to the denounce user.");
		} catch (Exception e) {
			LOGGER.error("Unable to notify to the user about the complaint lodged.", e);
		}		
	}

	/**
	 * @author hardas_d
	 * @since Apr 14, 2014
	 * @param ticketNum
	 * @return void
	 */
	@Override
	public void copyToOSI(String ticketNum) {
		LOGGER.info("Needs to be implemented");		
	}	
	/* Complaint workflow functions ENDs */

	/**
	 * @author hardas_d
	 * @param ticketNum
	 * @return void
	 */
	@Override
	public void sendFormToMedicaid(String ticketNum) {
		LOGGER.info("Needs to be implemented");		
	}

	/**
	 * @author hardas_d
	 * @param ticketNum
	 * @return void
	 */
	@Override
	public void storeQHPAppealForm(String ticketNum) {
		LOGGER.info("Needs to be implemented");	
	}	
	
	/**
	 * @author hardas_d
	 * @param appealReason
	 * @return void
	 */
	@Override
	public void sendAppChangeNoticeToCust(String appealReason) {
		LOGGER.info("Not yet finalized.");	
		
	}
	
	/**
	 * @author hardas_d
	 * @param appealReason
	 * @param appealStatus
	 * @return void
	 */
	@Override
	public void sendIndvAppealNoticeToCust(String appealReason,String ticketReason) {
		LOGGER.info("Not yet finalized.");
	}

	@Override
	public void sendIndvAppealNoticeToCust(String appealReason) {
		LOGGER.info("Not yet finalized.");
	}

	@Override
	public void sendAppChangeNoticeToCust(String appealReason,
			String ticketReason) {
		LOGGER.info("Not yet finalized.");
		
	}
	
	@Override
	public String getCreatedForIndv(String userText) {
		String result = ticketListService.getCreatedForIndv(userText);	
		return result;
	}
	
	@Override
	public String getCreatedForUser(String roleId, String userText) {
		String result = ticketListService.getCreatedForUser(roleId, userText);
		return result;
	}

	@Override
	public List<Household> getHouseholdByuserId(int houseHoldUserId) {
		List<Household> householdlist =ticketListService.getHouseholdByuserId(houseHoldUserId);
		return householdlist;
	}
	
	@Override
	public boolean archiveTicket(Integer ticketId, Integer archivedBy) throws TKMException {
		boolean bStatus = false;
		
		bStatus = ticketListService.archiveTicket(ticketId, archivedBy);
		return bStatus;
	}
	
	/**
	 * @author hardas_d
	 * Reopens the ticket. Initiates the default workflow for the ticket.
	 * @throws TKMException 
	 */
	@Override
	public void reopenTicket(Integer ticketId, Integer lastUpdatedBy) throws TKMException {
		LOGGER.info("Reopening the ticket");
		
		TkmTickets ticket = ticketListService.findTkmTicketsById(ticketId);
		
		if(tkmReqValidator.isReopenAllowed(ticket)) {
			ticket.setStatus(TkmTickets.ticket_status.UnClaimed.toString());
			AccountUser updator = new AccountUser();
			updator.setId(lastUpdatedBy);
			ticket.setUpdator(updator);
			
			TkmQueues workgroup = ticket.getTkmQueues();
			boolean bStatus = false;
			if (ticket.getTkmWorkflows().getFilename().equals("QleValidationProcess")) {
				TkmTicketsRequest ticketRequest = new TkmTicketsRequest();
				ticketRequest.setType(ticket.getTkmWorkflows().getType());
				Map<String, String> map = new HashMap<String, String>();
				List<TkmDocuments> documentList = iTkmDocumentsRepository.findByTicketId(ticket.getId());
				if (documentList != null && !documentList.isEmpty()) {
					map.put("docId", String.valueOf(documentList.get(0).getId()));
				} else {
					map.put("docId", "");
				}
				map.put("subModuleId", String.valueOf(ticket.getSubModuleId()));
				map.put("subModuleName", ticket.getSubModuleName());
				map.put("ticketId", String.valueOf(ticket.getId()));

				bStatus = tkmActivitiService.initiateProcess(ticket, ticket.getTkmWorkflows().getFilename(), workgroup.getName(), map);
			} else {
				bStatus = tkmActivitiService.initiateProcess(ticket, DEFAULT_WORKFLOW_NAME, workgroup.getName(), null);
			}
			
			if(!bStatus) {
				LOGGER.error("Unable to initiate the default workflow.");
			}
		}
		LOGGER.info("Reopening the ticket - DONE");
	}
	
	/**
	 * @author hardas_d
	 * Restarts the ticket.
	 * @throws TKMException 
	 */
	@Override
	public void restartTicket(Integer ticketId, Integer lastUpdatedBy) throws TKMException {
		LOGGER.info("Restarting the ticket");
		
		TkmTickets ticket = ticketListService.findTkmTicketsById(ticketId);
		
		if(tkmReqValidator.isReopenAllowed(ticket)) {
			ticket.setStatus(TkmTickets.ticket_status.Restarted.toString());
			AccountUser updator = new AccountUser();
			updator.setId(lastUpdatedBy);
			ticket.setUpdator(updator);
			
			TkmQueues workgroup = ticket.getTkmQueues();
			TkmWorkflows workflow = ticket.getTkmWorkflows();
			
			//get ticket form properties.
			Map<String, String> mapTaskFormProp = new HashMap<>();
			List<TaskFormProperties> propList = tkmActivitiService.getWorlFlowsMergedProperties(workflow.getFilename(), ticket.getId());
			for (TaskFormProperties taskFormProperties : propList) {
				String propertyId  = taskFormProperties.getId();
				String value = StringUtils.EMPTY;
				
				if(taskFormProperties.getValues() != null && !taskFormProperties.getValues().isEmpty()) {
					value = taskFormProperties.getValues().get(0);
				}
				
				mapTaskFormProp.put(propertyId, value);
			}
			boolean bStatus = tkmActivitiService.initiateProcess(ticket, workflow.getFilename(), workgroup.getName(), mapTaskFormProp);
			
			if(!bStatus) {
				LOGGER.error("Unable to re-initiate the workflow.");
			}
		}
		LOGGER.info("Restarting the ticket - DONE");
	}
	
	@Override
	public void addQueuesToUser(Integer userId, List<Integer> queueIdList) {
		List<Integer> queueList = queueIdList;
		if(queueList == null || queueList.size() == 0) {
			queueList = tkmQueuesService.getDefaultQueueIdlist();
		}
		
		tkmQueueUsersService.addQueuesToUser(userId, queueList);
	}
	
	/**
	 * @author Kunal Dav
	 * @param ticketId
	 * @param eventType
	 * @return
	 * @throws TKMException 
	 * @description Method to log ticket event
	 */
	@Override
	public void logTicketEvents(Integer ticketId, String eventType) throws TKMException  {
		TkmTickets tkmTickets = ticketListService.findTkmTicketsById(ticketId, true);
		logTicketEvents(tkmTickets, eventType);
	}
	
	/**
	 * @author Kunal Dav
	 * @param tkmTickets
	 * @param eventType
	 * @return
	 * @description Method to log ticket event
	 */
	@Override
	public void logTicketEvents(TkmTickets tkmTickets, String eventType)  {
		LOGGER.info("======================== in logTicketEvents() =======================");
		if(tkmTickets != null && 
				! (tkmTickets.getModuleName() != null && 
				(tkmTickets.getModuleName().equals("HOUSEHOLD") || tkmTickets.getModuleName().equals("SSAP_DOCUMENTS")))
				) {
			return;
		}
		LOGGER.info("======================== in logTicketEvents() passed if condition=======================");
		if(tkmTickets != null && tkmTickets.getTkmWorkflows() != null  && tkmTickets.getTkmQueues() != null){
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(TICKET_NUMBER, tkmTickets.getNumber());
			mapEventParam.put(TICKET_TYPE, tkmTickets.getTkmWorkflows().getCategory());
			mapEventParam.put(TICKET_SUBTYPE, tkmTickets.getTkmWorkflows().getType());
			mapEventParam.put(TICKET_WORKGROUP, tkmTickets.getTkmQueues().getName());
			
			//event type and category
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APPEVENT_TICKET, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();
			eventInfoDto.setEventLookupValue(lookupValue);
			eventInfoDto.setModuleId(tkmTickets.getModuleId());
			eventInfoDto.setModuleName("INDIVIDUAL");
			
			appEventService.record(eventInfoDto, mapEventParam);
		}else{
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("======================== in logTicketEvents() can't log event ======================="+eventType );	
			}
			
		}
	}
	
	@Override
	public void updateTicketWithEvent(String ticketId, String subModuleId, String subModuleName) {
		if (StringUtils.isNotEmpty(ticketId) && StringUtils.isNotEmpty(subModuleId)) {
				ticketListService.saveTicketEvent(Integer.valueOf(ticketId),Integer.valueOf(subModuleId),subModuleName);
		}
	}

	@Override
	public void processQLEVerification(String status, String docId, String processInstanceId, String activitiTaskId,
			String comments) {
		LOGGER.info("Notify document with :" + status + " DocId : " + docId + " activitiTask Id:" + activitiTaskId + 
				" processId:" + processInstanceId);

	try {
		TkmTicketTasks task = tkmTicketTaskService.getTicketByTaskIdAndActivitiId(processInstanceId, activitiTaskId);
		updateEventforQLEVerification(task, docId, status, comments);
	} catch (Exception e) {
		LOGGER.error(ERR_MSG_UNABLE_GET_TICKET, e);
	}
	}
	
	private void updateEventforQLEVerification(TkmTicketTasks task, String docId, String status, String comments) 
			throws NotificationTypeNotFound {
		LOGGER.info("Inside updateEventforQLEVerification: docId="+docId+" and Status="+status);

		TkmTickets ticket = task.getTicket();
		if(ticket != null){
			LOGGER.info("Inside updateEventforQLEVerification: eventId="+ticket.getSubModuleId()+" and Status="+status);
			try{
				AccountUser user = ticket.getUpdator();
				Map<String,Object > requestPayLoad = new HashMap<>();
				requestPayLoad.put("status", status.equalsIgnoreCase(ACCEPTED)? SsapApplicantEvent.ApplicantEventValidationStatus.VERIFIED : SsapApplicantEvent.ApplicantEventValidationStatus.REJECTED);
				requestPayLoad.put("last_updated_user_id", user.getId());
				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"ssap/applicantevent/"+ticket.getSubModuleId(), user.getUsername(), HttpMethod.PUT, MediaType.APPLICATION_JSON, String.class, requestPayLoad);
				
				LOGGER.info("Response from eligibility QLE: "+response.getBody());
			}catch(Exception e){
				LOGGER.error("UNABLE TO UPDATE EVENT AND QLE VERIFICATION STATUS OF APPLICANT", e);
			}	
		}else{
			LOGGER.error(ERR_MSG_UNABLE_GET_TICKET);
		}
		
	}

		
}

