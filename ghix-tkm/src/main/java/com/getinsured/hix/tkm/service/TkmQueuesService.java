package com.getinsured.hix.tkm.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.tkm.tkmException.TKMException;

public interface TkmQueuesService {

	List<TkmQueues> getQueuesList() throws TKMException;

	TkmQueues findById(Integer id);
	TkmQueues findByName(String name);

	List<Integer> getQueueIdlist();

	List<Integer> getDefaultQueueIdlist();
	void updateDefaultQueues(String queueIds);
	
	TkmQueues createQueue(TkmQueues tkmQueues);

	Map<Integer,String> getTicketWorkgroups();
	
}
