package com.getinsured.hix.tkm.activiti.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Task;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.tkm.tkmException.TKMException;

public interface TkmActivitiService {
	boolean initiateProcess(TkmTickets tickets, String processId, String groupName, Map<String, String> ticketFormPropertiesMap);
	
	boolean claimTask(Integer taskId, String activitiTaskId, Integer userId) throws TKMException;
	
	boolean completeTask(Integer taskId, String activitiTaskId, String comments, Map<String, Object> taskVariables, String processInstanceId) throws TKMException;

	boolean isProcessComplete(String processInstanceId);

	boolean cancelProcess(String processInstanceId);

	String getTicketStatus(String processInstanceId);
	
	List<Task> getTaskList(AccountUser user, String groupName, String processInstanceId);

	boolean reassignActivitiTask(String activitiTaskId, String groupId,
			String assignee) throws TKMException;

	List<TaskFormProperties> getWorlFlowsMergedProperties(String workFlowName,Integer ticketId)
			throws TKMException;

	Task getActivitiTaskById(String activitiTaskId) throws TKMException;

	String getDocIdFromVerifyDocumentBPMN(String workFlowName, TkmTicketTasks task); 
	
}
