package com.getinsured.hix.tkm.service.jpa;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.tkm.TkmPendingReportDto;
import com.getinsured.hix.tkm.service.TkmReportService;

@Service("TkmReportService")
public class TkmReportServiceImpl implements TkmReportService {

	private static final Logger LOGGER = Logger.getLogger(TkmReportServiceImpl.class);
	private static final String ANY = "Any";
	private static final String START_DUEDATE = "startDate";
	private static final String END_DUEDATE = "endDate";
	private static final String START_COUNT = "startCnt";
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	@Override
	public Map<String, Object> getPendingTicketDetails(Map<String, Object> searchCriteria) {
		Map<String, Object> mapTicketRecCount =  new HashMap<>();
		Map<String, Object> mapQueryParam = new HashMap<String, Object>();
		String openTicketCount = (String) searchCriteria.get("openCount");
		
		StringBuilder query = new StringBuilder("SELECT new com.getinsured.hix.dto.tkm.TkmPendingReportDto(COALESCE(tt.assignee.id,0) as id, " +
				"case  when tt.assignee.id > 0 then (user.firstName || ' ' || user.lastName) else ' ' end, " +
				"queueName.name, count(*), "); 
		query.append("sum((case when ((cast(now() AS date) - cast(t.created AS date)) BETWEEN 0 AND  7 ) then 1 else 0 end)) as BETWN_0_7, ");
		query.append("sum((case when ((cast(now() AS date) - cast(t.created AS date)) BETWEEN 8 AND  14 ) then 1 else 0 end)) as BETWN_8_14, ");
		query.append("sum((case when ((cast(now() AS date) - cast(t.created AS date)) BETWEEN 15 AND  30 ) then 1 else 0 end)) as BETWN_15_30, ");
		query.append("sum((case when ((cast(now() AS date) - cast(t.created AS date)) BETWEEN 31 AND  45 ) then 1 else 0 end)) as BETWN_31_45, ");
		query.append("sum((case when ((cast(now() AS date) - cast(t.created AS date)) > 45 ) then 1 else 0 end)) as MORE_THAN_45 ) ");
		
		String fromClause 	= "FROM  TkmTicketTasks tt LEFT OUTER JOIN tt.ticket as t LEFT OUTER JOIN t.tkmQueues as queueName LEFT OUTER JOIN tt.assignee as user ";
		String whereClause 	= "WHERE t.status IN ('UnClaimed','Open') AND tt.status IN ('UnClaimed','Claimed') ";
		String groupByClause = "GROUP BY tt.assignee.id, user.firstName, user.lastName, queueName.name ORDER BY tt.assignee.id";
		
		query.append(fromClause);
		query.append(whereClause);	
		applyFiltersForPendingReport(query, searchCriteria, mapQueryParam);
		query.append(groupByClause);
		
		
		
		LOGGER.info("Search TkmTicketTask Query: " + query.toString());
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			
			@SuppressWarnings("unchecked")
			List<TkmPendingReportDto> resultList = hqlQuery.getResultList();
			
//			// Merge the object to show the cumulative output 
			Map<Integer, TkmPendingReportDto> mapMergedDto = new LinkedHashMap<>();
			for (TkmPendingReportDto tkmPendingReportDto : resultList) {
				if(mapMergedDto.containsKey(tkmPendingReportDto.getAssigneeId())) {
					//System.out.println("BEFORE: " + tkmPendingReportDto.toString());
 					TkmPendingReportDto dto = mapMergedDto.get(tkmPendingReportDto.getAssigneeId());
					tkmPendingReportDto.merge(dto);
					
					//System.out.println("AFTER: " + tkmPendingReportDto.toString());
				}
				mapMergedDto.put(tkmPendingReportDto.getAssigneeId(), tkmPendingReportDto);				
			}

			List<TkmPendingReportDto> mergedDtoList = new LinkedList<>(mapMergedDto.values());	
			
			if(openTicketCount != null && (openTicketCount.contains("-") || openTicketCount.contains("+")) ) {
				 filterByCount(mergedDtoList,openTicketCount);			
			}	
			
			mapTicketRecCount.put("ticketDetailList", mergedDtoList);			
		} catch (Exception e) {
			LOGGER.error("Unable to search tickets", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return mapTicketRecCount;
	}
	
	private void applyFiltersForPendingReport(StringBuilder query, Map<String, Object> searchCriteria, Map<String, Object> mapQueryParam) {
		if(searchCriteria == null) {
			return;
		}
		
		String assigneeId = (String) searchCriteria.get("assigneeId");
		if(!StringUtils.isEmpty(assigneeId) && ((ANY).compareToIgnoreCase(assigneeId) !=0)) {
			query.append("AND tt.assignee.id=:").append("assigneeId").append(" ");
			mapQueryParam.put("assigneeId", Integer.parseInt(assigneeId));
		}
		
		//DUE IN
		filterByDueIn(query, searchCriteria, mapQueryParam); 
		
		// filter by workgroup
		String workgroup = (String) searchCriteria.get("queueName");
		if(!StringUtils.isEmpty(workgroup) && (ANY).compareToIgnoreCase(workgroup) !=0) {
			query.append(" AND queueName.name = :").append("queueName").append(" ");
			mapQueryParam.put("queueName", workgroup);
		}
		
	}
	
	private void filterByCount(List<TkmPendingReportDto> mergedTicketDtoList, String openTicketcount) {
		// filter by open ticket count
		if(mergedTicketDtoList != null && mergedTicketDtoList.size() > 0 ) {
				String[] split=null;
				String startCnt=null;
				String endCnt=null;
				
				if(openTicketcount.contains("-"))
				{
					split=openTicketcount.split("-");
					startCnt = split[0];
					endCnt = split[1];
					
					if(!StringUtils.isEmpty(startCnt) && !StringUtils.isEmpty(endCnt) && (ANY).compareToIgnoreCase(startCnt) !=0) {
						Long startVal =  Long.valueOf(startCnt);
						Long endVal =  Long.valueOf(endCnt);
						mergedTicketDtoList.removeIf(e -> (e.getOpenCount() < startVal || e.getOpenCount() > endVal ));
					}
				}
				else if(openTicketcount.contains("+"))
				{
					startCnt = "30";
					Long countVal = Long.valueOf(startCnt);
					mergedTicketDtoList.removeIf(e -> e.getOpenCount() < countVal );
					
				}
		}

	}
	
	private void filterByDueIn(StringBuilder query, Map<String, Object> searchCriteria, Map<String, Object> mapQueryParam) {
		
		String strDueIn =(String) searchCriteria.get("dueIn");
		if(StringUtils.isEmpty(strDueIn)) {
			return;
		}
		
		TkmPendingReportDto.DUEIN dueIn = TkmPendingReportDto.DUEIN.valueOf(strDueIn);
		Calendar cal = TSCalendar.getInstance();
		
		cal.setTimeInMillis(getSystemTimeStampFromDB().getTime());
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormat.format(cal.getTime());
		
		String startDate =date;
		String endDate = date;
		
		switch (dueIn) {
		case OVERDUE:	
			filterByDate(query, null, endDate, mapQueryParam);
			break;
			
		case TODAY:
			filterByDate(query, startDate, endDate, mapQueryParam);
			break;
			
		case TOMORROW:
			cal.add(Calendar.DATE, 1);
			startDate = dateFormat.format(cal.getTime());
			endDate = dateFormat.format(cal.getTime());
			filterByDate(query, startDate, endDate, mapQueryParam);
			break;

		case ZERO_TO_SEVEN_DAYS:
			startDate = date;
			cal.add(Calendar.DATE, 7);
			endDate = dateFormat.format(cal.getTime());
			filterByDate(query, startDate, endDate, mapQueryParam);
			break;
						
		case EIGHT_TO_FOURTEEN_DAYS:			
			cal.add(Calendar.DATE, 8);
			startDate = dateFormat.format(cal.getTime());
			cal.add(Calendar.DATE, 14);
			endDate = dateFormat.format(cal.getTime());
			filterByDate(query, startDate, endDate, mapQueryParam);
			break;

		default:
			break;
		}
	}
	
	private void filterByDate(StringBuilder query, String startDate, String endDate,	Map<String, Object> mapQueryParam) {
		if(StringUtils.isNotEmpty(endDate)) {
			query.append("AND t.dueDate <= :").append(END_DUEDATE).append(" ");
			Date submittedBeforeDate = prepareDateCriteria(endDate, true);
			submittedBeforeDate = (submittedBeforeDate != null) ? submittedBeforeDate : new TSDate();
			mapQueryParam.put(END_DUEDATE, submittedBeforeDate);
		}
		
		if(StringUtils.isNotEmpty(startDate)) {
			query.append("AND t.dueDate >=:").append(START_DUEDATE).append(" ");
			Date submittedAfterDate = prepareDateCriteria(startDate, false);
			submittedAfterDate = (submittedAfterDate != null) ? submittedAfterDate : new TSDate();
			mapQueryParam.put(START_DUEDATE, submittedAfterDate);
		}
	}

	private Date prepareDateCriteria(String submittedDateString, boolean maxtime) {
		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		if(StringUtils.isEmpty(submittedDateString)) {
			return effectiveEndDate;
		}
		
		try {
			effectiveEndDate = formatter.parse(submittedDateString);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			return effectiveEndDate;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		if(maxtime){
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));
		}else{
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMinimum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMinimum(Calendar.SECOND));
		}
		
		return calendarDate.getTime();
	}
	
	private Timestamp getSystemTimeStampFromDB() {
		return  (Timestamp)emf.createEntityManager().createNativeQuery("select now()").getSingleResult();
	}

	/**
	 * @param hqlQuery
	 * @param mapQueryParam
	 * This method will add the values in the queries as parameters and will avoid SQL Injection.
	 */
	private void setNamedQueryParams(Query hqlQuery, Map<String, Object> mapQueryParam) {
		for(String param : mapQueryParam.keySet()){
			hqlQuery.setParameter(param, mapQueryParam.get(param));
		}
	}

}
