package com.getinsured.hix.tkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmWorkgroupResponseDto;

@Repository
public interface ITkmQueuesRepository extends JpaRepository<TkmQueues, Integer>{
	
	TkmQueues findByNameIgnoreCase(String name);
	
	@Query("SELECT tq.id FROM TkmQueues tq")
	List<Integer> findTkmQueueIds();
	
	@Query("SELECT tq.id FROM TkmQueues tq WHERE tq.isDefault='Y'")
	List<Integer> findDefaultTkmQueueIds();

	@Query("SELECT tq.id, tq.name FROM TkmQueues tq ORDER BY tq.name ASC")
	List<Object[]> getTicketWorkgroups();
}
