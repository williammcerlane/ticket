package com.getinsured.hix.tkm.tkmException;

public class TKMException extends Exception {

	private static final long serialVersionUID = 1061262462011519257L;

	public TKMException() {
		super();
	}
	public TKMException(Throwable cause) {
		super(cause);
	}
	public TKMException(String message, Throwable cause) {
		super(message, cause);
	}
	public TKMException(String message) {
		super(message);
	}
}
