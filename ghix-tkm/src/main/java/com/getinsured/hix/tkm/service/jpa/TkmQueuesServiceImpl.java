package com.getinsured.hix.tkm.service.jpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.tkm.repository.ITkmQueuesRepository;
import com.getinsured.hix.tkm.service.TkmQueuesService;

@Service("tkmQueuesService")
@Repository
public class TkmQueuesServiceImpl implements TkmQueuesService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmQueuesServiceImpl.class);

	@Autowired private ITkmQueuesRepository iTkmQueuesRepository;
	
	@Override
	public List<TkmQueues> getQueuesList() {
		List<TkmQueues> queueList =iTkmQueuesRepository.findAll();
		
		return queueList;
	}

	@Override
	public TkmQueues findById(Integer id) {
		return iTkmQueuesRepository.findOne(id);
	}

	@Override
	public TkmQueues findByName(String name) {
		LOGGER.debug("checkig if workgroup exist with name "+name);
		return iTkmQueuesRepository.findByNameIgnoreCase(name);
	}
	
	@Override
	public List<Integer> getQueueIdlist() {
		List<Integer> queueIdList =iTkmQueuesRepository.findTkmQueueIds();
		
		return queueIdList;
	}
	
	@Override
	public List<Integer> getDefaultQueueIdlist() {
		List<Integer> queueIdList =iTkmQueuesRepository.findDefaultTkmQueueIds();
		return queueIdList;
	}
	
	@Override
	public void updateDefaultQueues(String defaultQueueIds) {
		String [] queueIdArray = defaultQueueIds.split("\\|");
		List<String> queueIdList = new ArrayList<String>(Arrays.asList(queueIdArray));
		List<TkmQueues> queuesList = getQueuesList();
		for(TkmQueues tkmQueues : queuesList) {
			if(queueIdList.contains(tkmQueues.getId().toString())) {
				tkmQueues.setIsDefault("Y");
			} else {
				tkmQueues.setIsDefault("N");
			}
			iTkmQueuesRepository.save(tkmQueues);
		}
	}

	@Override
	public TkmQueues createQueue(TkmQueues tkmQueues) {
		tkmQueues = fillQueueData(tkmQueues);
		return iTkmQueuesRepository.save(tkmQueues);
	}

	private TkmQueues fillQueueData(TkmQueues tkmQueuesDto) {
		TkmQueues tkmQueues = null;
		if(null != tkmQueuesDto && null != tkmQueuesDto.getId()) {
			tkmQueues = iTkmQueuesRepository.findOne(tkmQueuesDto.getId());
		}
		if(null == tkmQueues) {
			tkmQueues = new TkmQueues();
		}
		if(null != tkmQueuesDto.getName()) {
			tkmQueues.setName(tkmQueuesDto.getName());
		}
		if(!StringUtils.isEmpty(tkmQueuesDto.getIsDefault())) {
			tkmQueues.setIsDefault(tkmQueuesDto.getIsDefault());
		}
		if(null != tkmQueuesDto.getRoleId()) {
			tkmQueues.setRoleId(tkmQueuesDto.getRoleId());
		}
		if(null != tkmQueuesDto.getCreator()) {
			tkmQueues.setCreator(tkmQueuesDto.getCreator());
		}
		if(null != tkmQueuesDto.getUpdator()) {
			tkmQueues.setUpdator(tkmQueuesDto.getUpdator());
		}
		return tkmQueues;
	}

	@Override
	public Map<Integer,String> getTicketWorkgroups() {
		Map<Integer,String> results = new HashMap<Integer,String>();

		List<Object[]> list = iTkmQueuesRepository.getTicketWorkgroups();
		for (Object[] ob : list) {
			String value = (String) ob[1];
			Integer key = (Integer) (ob[0]);
			results.put(key, value);
		}
		return results;
		}
	
	
	
}
