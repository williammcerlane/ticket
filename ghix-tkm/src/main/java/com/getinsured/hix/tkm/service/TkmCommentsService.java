package com.getinsured.hix.tkm.service;

import com.getinsured.hix.model.CommentTarget;

public interface TkmCommentsService {

	boolean saveComments(Integer ticketId, CommentTarget comment);

	CommentTarget getCommentTargetByTicketId(Integer ticketId);
}
