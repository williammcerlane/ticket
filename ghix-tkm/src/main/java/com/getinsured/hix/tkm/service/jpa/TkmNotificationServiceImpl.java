package com.getinsured.hix.tkm.service.jpa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.tkm.notification.TkmTicketNotificationTemplate;
import com.getinsured.hix.tkm.notification.TkmTicketRequesterNotificationTemplate;
import com.getinsured.hix.tkm.service.TkmNotificationService;
import com.getinsured.hix.tkm.util.TkmUtil;


@Service("tkmNotificationService")
public class TkmNotificationServiceImpl implements TkmNotificationService {
	
	

	private static final Logger LOGGER = LoggerFactory.getLogger(TkmNotificationServiceImpl.class);

	@Autowired private UserService userService;
	
	@Autowired
	@Qualifier("tkmTicketNotificationTemplate")
	private TkmTicketNotificationTemplate tkmTicketNotificationTemplate; 
	
	private TkmTicketNotificationTemplate tkmNotificationTemplate; 
	
	@Autowired
	@Qualifier("tkmTicketRequesterNotificationTemplate")
	private TkmTicketRequesterNotificationTemplate requesterNotificationTemplate;
	
	@Autowired 
	private NoticeService noticeService;

	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	private static final String SUBJECT ="subject";
	private static final String TICKET_DESC = "ticketDesc";
	private static final String MODULE_ID = "ModuleID";
	private static final String FILE_NAME = "fileName";

	@Override
	public void sendEmailNotification(Map<String, String> tokens,TkmTickets ticket) throws NotificationTypeNotFound {	
		String exchangeType=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		String stateCode=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		tokens.put("currentYear", ""+LocalDate.now().getYear());
		tokens.put("exchangeFullName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("exchangeURL", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL) );
		String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
		if(contactInformation != null){
			tokens.put("contact_information",contactInformation);
		} else {
			tokens.put("contact_information","");
		}
		setTemplateTypeForRequester(tokens);
		
		Map<String, String> externalTokens = getDefaultTokens(tokens,ticket);
		if(!externalTokens.containsKey(TICKET_DESC)) {
			externalTokens.put(TICKET_DESC, ticket.getDescription());
		}
		
		boolean bSecure = TkmUtil.getBooleanValueFromEmailConfig(TkmTicketsRequest.Email_Config.SecureInbox.toString(), ticket.getTkmWorkflows().getEmailConfig());
		if(bSecure){ 
			
			Map<String, Object> emailTemplateTokens = getTemplateTokens(ticket, externalTokens);
			tkmNotificationTemplate.setTkmTickets(ticket);
			tkmNotificationTemplate.updateSingleData(externalTokens);
			
			externalTokens.put("ticketNumber", ticket.getNumber());
			if("STATE".equalsIgnoreCase(exchangeType) && "ID".equalsIgnoreCase(stateCode))
			{
			String exchangeName=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			String subject= "This is an AUTOMATED update from "+ exchangeName+ " Customer Support";
			externalTokens.put("ticketSubject", subject);
			}
			else
			{
				externalTokens.put("ticketSubject", ticket.getSubject());
			}
			
			
			externalTokens.put("ticketStatus",  getTiketStatus(ticket.getStatus()));
			sendSecureEmail(emailTemplateTokens,externalTokens);
		}
		else{
			sendPrivateEmailnotification(ticket,externalTokens);
		}
	}
	
	private StringBuilder generateCustomiseMsgText(Map<String, String> externalTokens,TkmTickets ticket){
		StringBuilder msg = new StringBuilder("");
		
		String exchangeType=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		String stateCode=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		
		if(externalTokens.containsKey("documentType")){
			msg.append("Your document to ")
					.append(ticket.getSubject())
					.append(" has been ")
					.append(externalTokens.get("documentStatus"))
					.append(". Check your Dashboard for any next steps or open items.");
		}else
		{
			if("STATE".equalsIgnoreCase(exchangeType) && "ID".equalsIgnoreCase(stateCode))
			{
				if(ticket.getStatus().equalsIgnoreCase(TkmTickets.ticket_status.UnClaimed.toString())) {
					
					if("Feedback".equalsIgnoreCase(ticket.getTkmWorkflows().getCategory()) && "Provide Feedback".equalsIgnoreCase(ticket.getTkmWorkflows().getType()))
					{
						msg.append("We have received your suggestion and will review it shortly. Thank you for your support and help in improving our services.<br><br>Your ticket number is:"+ticket.getNumber()+"<br><br>"+
								"<u>What happens next?</u><br><br>"+
								"The "+exchangeName+" team will review your suggestion and may follow up with you if we need additional information.<br>");
								
						
					}
					
					else
					{
					msg.append("We have received your request and will review it shortly.<br><br>Your ticket number is:"+ticket.getNumber()+"<br><br>"+
					"<u>What happens next?</u><br><br>"+
					"The "+exchangeName+" team will review your report and take appropriate action. We may follow up with you if we need additional information." +
					" Your patience is greatly appreciated.<br>");
					}
					
				} 
				
				//ticket.getTkmWorkflows().getCategory()
				else if(ticket.getStatus().equalsIgnoreCase(TkmTickets.ticket_status.Resolved.toString())) {
					msg.append("We have resolved your request and ticket number:"+ticket.getNumber()+" has been closed."+"<br><br>" +
							"If you have any questions about this ticket, you may email Connect@YourHealthIdaho.org. Please include this ticket number in your message.");
				}
				else
				{
				msg.append("<b>" +ticket.getNumber()+"</b>, "+ticket.getSubject()+" has been "+ticket.getStatus());
				msg.append("Description :");
				msg.append(ticket.getDescription());
				}
			}
			else
			{
			String status = ticket.getStatus();
			if(externalTokens.containsKey("status")) {
				status = externalTokens.get("status");
			}
			msg.append("<b>" +ticket.getNumber()+"</b>, "+ticket.getSubject()+" has been "+status);
			msg.append("<br/>Description :");
			msg.append(ticket.getDescription());
			}
			
		}
		msg.append("<br>");
		/*msg.append("Description :");
		msg.append(ticket.getDescription());	*/
		return msg;
	}
	
	
	
	//private Map<String, String> getDefaultTokens(Map<String, String> tokens,TkmTickets ticket){
	private String getTiketStatus(String status) {
		if("UnClaimed".equalsIgnoreCase(status)){
			return "created";
		}
		else if("Canceled".equalsIgnoreCase(status)){
			return "cancelled";
		}
		else if("Resolved".equalsIgnoreCase(status)){
			return "resolved";
		}
		else if("Open".equalsIgnoreCase(status)){
			return "opened";
		}
		
		return "";
		
	}
	
	private Map<String, String> getDefaultTokens(Map<String, String> tokens,TkmTickets ticket){
		Map<String, String> mapTokens = tokens;
		if(!mapTokens.containsKey("msgTo")) {
			mapTokens.put("msgTo", StringUtils.EMPTY);
		}
		
		if(!mapTokens.containsKey("msgText")) {
			
			mapTokens.put("msgText",generateCustomiseMsgText(tokens, ticket).toString());
			
		}		
		return mapTokens;
	}

	
		
	private void sendPrivateEmailnotification(TkmTickets ticket,Map<String, String> emailTokens) throws  NotificationTypeNotFound{
		LOGGER.info("appealRequestNotificationTemplate : START");
		tkmNotificationTemplate.setTkmTickets(ticket);
		tkmNotificationTemplate.updateSingleData(emailTokens);
		Notice noticeObj = tkmNotificationTemplate.generateEmail();
//		LOGGER.info(noticeObj.getEmailBody());
		tkmNotificationTemplate.sendEmail(noticeObj);
		LOGGER.info("appealRequestNotificationTemplate : END");
	}
	
	private Map<String, Object> getTemplateTokens(TkmTickets ticket, Map<String, String> externalTokens)  {
		LOGGER.info("Initiating template to send secure email");
		Map<String, Object> emailData = new HashMap<>();
		
		List<ModuleUser> moduleUserList = null;
		emailData.put("EmailTitle", externalTokens.get(SUBJECT)); 
		
		String recipent = (String) externalTokens.get("recipient");
		List<String> sendToEmailList = new ArrayList<String>(); 
		if(StringUtils.isEmpty(recipent)) {
			moduleUserList = userService.getUserModules(ticket.getRole().getId(), ticket.getUserRole().getName().toLowerCase());
			sendToEmailList.add(ticket.getRole().getEmail());
			emailData.put("accountUser",ticket.getRole());
			emailData.put("toFullName", ticket.getRole().getFullName());
		} else  {
			AccountUser user = null;
			String userId = (String) externalTokens.get("userId");
			if(!StringUtils.isEmpty(userId)){
				user = userService.findById(Integer.parseInt(userId));
			} else {
				user =  userService.findByEmail(recipent);
			}

			Role userRole = userService.getDefaultRole(user);
			moduleUserList = userService.getUserModules(user.getId(),userRole.getName().toLowerCase());
			sendToEmailList.add(recipent);
			emailData.put("accountUser", user);
			emailData.put("toFullName",externalTokens.get("name"));
		}
		emailData.put("sendToEmailList",sendToEmailList);
		emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		emailData.put("updateSubject", externalTokens.get(SUBJECT));
		
		
		if(null!=moduleUserList &&!moduleUserList.isEmpty()) {
			//tokens for secure inbox template
			emailData.put(MODULE_ID,moduleUserList.get(0).getModuleId());
			emailData.put("ModuleName", moduleUserList.get(0).getModuleName());
			emailData.put(FILE_NAME,"Notice-"+emailData.get(MODULE_ID)+"-"+new TSDate().getTime()+".pdf");
		} else {
			emailData.put(FILE_NAME,"Notice-"+ticket.getId()+"-"+new TSDate().getTime()+".pdf");
		}
		
		return emailData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void sendSecureEmail(Map<String,Object> emailData, Map<String,String> externalTokens) throws NotificationTypeNotFound {
		LOGGER.info("secureEmailNotification : START");
		setTemplateTypeForRequester(externalTokens);
		try {
					Map<String, Object> emailTokens = new HashMap<String, Object>();
					Map<String,String> templateTokens = new HashMap<String, String>();			
					templateTokens.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);
//					LOGGER.debug("Exchange full Name " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
					templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
					templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
					templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
					templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
					templateTokens.put(TemplateTokens.PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
					emailTokens.putAll(templateTokens);
					emailTokens.putAll(emailData);
					emailTokens.putAll(externalTokens);
					templateTokens.putAll(externalTokens);	
					tkmNotificationTemplate.setNotificationData(emailTokens);
					tkmNotificationTemplate.updateSingleData(templateTokens);
					AccountUser user= (AccountUser) tkmNotificationTemplate.notificationData.get("accountUser");
					String userName =user.getFirstName()+" "+user.getLastName();
					emailTokens.put("userName", userName);
					Notice notice = null;
					if(emailTokens.containsKey(MODULE_ID)){
						// send the notice using createModuleNotice
						
						notice = noticeService.createModuleNotice(tkmNotificationTemplate.getClass().getSimpleName(),
								GhixLanguage.US_EN,
								tkmNotificationTemplate.notificationData,
								"notification/"+tkmNotificationTemplate.notificationData.get("ModuleName").toString()+"/"+tkmNotificationTemplate.notificationData.get(MODULE_ID),
								tkmNotificationTemplate.notificationData.get(FILE_NAME).toString(), 
								tkmNotificationTemplate.notificationData.get("ModuleName").toString(),
								Long.parseLong(tkmNotificationTemplate.notificationData.get(MODULE_ID).toString()),
								(List<String>) tkmNotificationTemplate.notificationData.get("sendToEmailList"),
								tkmNotificationTemplate.notificationData.get("fromFullName").toString(),
								tkmNotificationTemplate.notificationData.get("toFullName").toString());
								
					} else {
						// the requester is not from module user table, send mail using createNotice 
						notice = noticeService.createNotice(
								tkmNotificationTemplate.getClass().getSimpleName(),
								GhixLanguage.US_EN,
								tkmNotificationTemplate.notificationData,
								"notification/"
										+ "admin"
										+ "/"
										+ user.getId(),
								tkmNotificationTemplate.notificationData
										.get(FILE_NAME).toString(), user);
					}  					
//					LOGGER.info((notice!=null) ? notice.getEmailBody() : "Notice not generated");
		} catch (Exception e) {
			LOGGER.error("Error in creating secure notice for TKM Module: ", e);
		}
		LOGGER.info("secureEmailNotification : END");
	}
	
	@Override
	public void sendSecureEmail(Map<String, String> emailTokens) throws NotificationTypeNotFound {
		LOGGER.info("secureEmailNotification : START");
		setTemplateTypeForRequester(emailTokens);
		tkmNotificationTemplate.updateSingleData(emailTokens);
		Notice noticeObj = tkmNotificationTemplate.generateEmail();
//		LOGGER.info(noticeObj.getEmailBody());
		tkmNotificationTemplate.sendEmail(noticeObj);
		LOGGER.info("secureEmailNotification : END");
	}

	private void setTemplateTypeForRequester(Map<String, String> emailTokens) {
		if(emailTokens.containsKey("isRequster") && emailTokens.get("isRequster").equals("true")) {
			this.tkmNotificationTemplate = this.requesterNotificationTemplate;
		} else {
			this.tkmNotificationTemplate = this.tkmTicketNotificationTemplate;
		}
	}

}
