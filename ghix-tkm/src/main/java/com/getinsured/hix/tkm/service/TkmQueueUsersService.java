/**
 * 
 */
package com.getinsured.hix.tkm.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.tkm.tkmException.TKMException;

/**
 * @author hardas_d
 *
 */
public interface TkmQueueUsersService {

	boolean updateQueueUsers(Integer queueId, List<Integer> userList,boolean addFlag) throws TKMException;
	
	List<AccountUser> getAccountUsersByQueueName(String queuename);
	
	List<Integer> getUserQueueId(int userId);

	List<TkmQueues> getQueueForUser(int userId);
	
    List<Object[]> getAccountUsersWithRoleByQueueName(String queuename);
    
    boolean addQueuesToUser(Integer userId,	List<Integer> queueIdList);

	List<Object[]> getAccountUsersForReassign(String queuename);
    Map<String,Integer> getworkgrpNameWithCount();
    List<Object[]> getAccountUsersNotInQueue(String queueName, String roleName, boolean isAddFlow);

}
