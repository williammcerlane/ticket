package com.getinsured.hix.tkm.dto;

import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.persistence.entity.GroupEntity;

import com.getinsured.hix.model.TkmQueues;

public class TkmIdentityGroupDto  implements GroupEntity {
	
	private String id;
	
	private String name;
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public TkmIdentityGroupDto(TkmQueues queues){
		this.id=String.valueOf(queues.getId());
		this.name=queues.getName();
	}
	
	@Override
	public void setId(String id) {
		this.id=id;
		
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name=name;
		
	}

	@Override
	public String getType() {
		return name;
	}

	@Override
	public void setType(String name) {
			this.name=name;
		
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public boolean isInserted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setInserted(boolean inserted) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isUpdated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setUpdated(boolean updated) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setDeleted(boolean deleted) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getPersistentState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRevision(int revision) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getRevision() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRevisionNext() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
