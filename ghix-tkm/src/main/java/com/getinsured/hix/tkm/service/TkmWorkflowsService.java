package com.getinsured.hix.tkm.service;

import java.util.List;

import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.TkmWorkflowsDto;
import com.getinsured.hix.tkm.tkmException.TKMException;

public interface TkmWorkflowsService {
	
	List<TkmWorkflows> getTicketTypeList() throws TKMException;
	
	List<String> getDistinctTicketType() throws TKMException;
	
	TkmWorkflows getTicketWorkflowsByCriteria (String type, String ticketCategory);
	
	TkmWorkflows getTicketWorkflowsByCriteria (String ticketType);
	
	List<TkmWorkflows> getActiveTicketTypeList() throws TKMException;
	
	TkmWorkflows createTicketType(TkmWorkflowsDto tkmWorkflows);

	TkmWorkflows getTicketWorkflow(Integer id) throws TKMException;

	List<String> getTicketCategories(List<String>filteredPermissions);

	List<TkmWorkflows> getWorkflowsByCategory(String category,List<String>filteredPermissions);

	
	List<String> getWorkflowTypes();
	
	List<String> getPermissionNames(List<String>filteredPermissions);
	
	List<String> getTicketTypes();
	
	void updateTkmCategoryName(String category,String old_category) throws TKMException;




}
