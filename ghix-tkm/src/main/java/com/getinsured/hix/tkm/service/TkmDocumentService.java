package com.getinsured.hix.tkm.service;

import java.util.List;

import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.tkm.tkmException.TKMException;


public interface TkmDocumentService {

	boolean saveDocument(Integer ticketId,  Integer creatorId, List<String> docList, boolean bUpdate) throws TKMException;

	List<TkmDocuments> getDocumentList(Integer ticketId) throws TKMException;

	boolean deleteDocument(String docId) throws TKMException;

	List<String> getDocumentPathList(Integer ticketId) throws TKMException;
}
