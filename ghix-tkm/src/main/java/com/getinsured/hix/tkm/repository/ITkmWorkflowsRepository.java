package com.getinsured.hix.tkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.TkmWorkflows;

@Repository
public interface ITkmWorkflowsRepository extends JpaRepository<TkmWorkflows, Integer>
{
	@Query("SELECT DISTINCT t.type FROM TkmWorkflows t")
	List<String> getDistinctTicketType();
	
	@Query("FROM TkmWorkflows tW WHERE tW.category = :category AND tW.type = :type AND tW.status = 'ACTIVE'")
	TkmWorkflows findByCriteria(@Param("type") String type, @Param("category") String category);
	
	@Query("FROM TkmWorkflows tW WHERE LOWER(tW.category) = :category AND LOWER(tW.type) = :type AND tW.filename = :workflowfilename")
	TkmWorkflows findByCriteria(@Param("type") String type, @Param("category") String category, @Param("workflowfilename") String workflowFilename);

	@Query("FROM TkmWorkflows tW WHERE tW.type = :type AND tW.status = 'ACTIVE'")
	TkmWorkflows findByCriteria(@Param("type") String ticketType);	

	List<TkmWorkflows> findByStatus(TkmWorkflows.TicketStatus status);	

	TkmWorkflows findById(Integer id);	

	@Query("SELECT DISTINCT tw.category FROM TkmWorkflows tw where tw.status IN ('ACTIVE' ,'INCOMPLETE') and tw.permission IN (:filteredPermissions) order by tw.category asc")
	List<String> getCategoriesByPermission(@Param("filteredPermissions")List< String> filteredPermissions);

	@Query("FROM TkmWorkflows tw where tw.category = :category AND tw.permission IN (:filteredPermissions) AND tw.id IS NOT NULL order by tw.status ASC, tw.type ASC")
	List<TkmWorkflows> getWorkflowsByCategory(@Param("category") String category,@Param("filteredPermissions")List< String> filteredPermissions);

	@Query("SELECT DISTINCT tw.filename FROM TkmWorkflows tw where tw.status = 'ACTIVE' ORDER BY tw.filename ASC")
	List<String> getWorkflowTypes();

	@Query("SELECT DISTINCT tw.permission FROM TkmWorkflows tw where tw.status = 'ACTIVE' and tw.permission IN (:filteredPermissions) ORDER BY tw.permission ASC")
	List<String> getPermissionNames(@Param("filteredPermissions")List< String> filteredPermissions);
	
	@Query("SELECT DISTINCT tw.type FROM TkmWorkflows tw where tw.status = 'ACTIVE' ORDER BY tw.type ASC")
	List<String> getTicketTypes();
	
	@Modifying
	@Transactional
	@Query("UPDATE TkmWorkflows tw SET tw.category=:category  WHERE tw.category=:old_category ")
	void updateTkmCategoryName(@Param("category") String category,@Param("old_category") String old_category);

}
