package com.getinsured.hix.tkm.activiti;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;
import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.tkm.dto.TkmIdentityUserDto;
import com.getinsured.hix.tkm.service.TkmQueueUsersService;

public class TkmUserManager extends AbstractManager implements UserEntityManager {

	
	private UserService userService;
	private TkmQueueUsersService tkmQueueUsersService;

	public TkmUserManager(ProcessEngineConfigurationImpl processEngineConfiguration, UserService userService,TkmQueueUsersService tkmQueueUsersService) {
		super(processEngineConfiguration);
		this.userService = userService;
		this.tkmQueueUsersService =tkmQueueUsersService;
	}

	@Override
	public User createNewUser(String userId) {

		throw new ActivitiException(
				"My user manager doesn't support creating a new user");

	}

	public UserEntity findById(String userLogin) {

		TkmIdentityUserDto queuesDTO = null;
		AccountUser users = userService.findById(Integer
				.parseInt(userLogin));
		if (checkNotNull(users)) {
			queuesDTO = new TkmIdentityUserDto(users);
		}
		return queuesDTO;

	}

	@Override
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {

		List<User> userList = new ArrayList<User>();
		UserQueryImpl userQuery = query;

		if (StringUtils.isNotEmpty(userQuery.getId())) {

			userList.add(findById(userQuery.getId()));

			return userList;

		} else if (StringUtils.isNotEmpty(userQuery.getLastName())) {

			userList.add(findById(userQuery.getLastName()));

			return userList;

		} else {
			if (StringUtils.isNotEmpty(userQuery.getGroupId())) {

				userList = findUsersByGroupName(userQuery.getGroupId());//using group name as id parameter

				return userList;

			} else {


				 return Collections.emptyList();

			} // TODO: you can add other search criteria that will allow
				// extended

		} // TODO: you can add other search criteria that will allow extended
			// support using the Activiti engine API

	}

	private List<User> findUsersByGroupName(String groupName) {
		
		List<User> identityUserDtos = new ArrayList<User>();
		List<AccountUser> accountUsers = tkmQueueUsersService.getAccountUsersByQueueName(groupName);
		for (AccountUser accountUser : accountUsers) {
			TkmIdentityUserDto tkmIdentityUserDto= new TkmIdentityUserDto(accountUser);
			identityUserDtos.add(tkmIdentityUserDto);
		}
		return  identityUserDtos;
	}

	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return findUserByQueryCriteria(query, null).size();

	}

	@Override
	public Boolean checkPassword(String userId, String password) {

		// TODO: check the password in your domain and return the appropriate
		// boolean

		return false;

	}

	
	private boolean checkNotNull(AccountUser users) {
		if (null != users) {
					return true;
		}
		return false;
	}

	@Override
	public UserEntity create() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(UserEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insert(UserEntity entity, boolean fireCreateEvent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UserEntity update(UserEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity update(UserEntity entity, boolean fireUpdateEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(UserEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(UserEntity entity, boolean fireDeleteEvent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateUser(User updatedUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Group> findGroupsByUser(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserQuery createNewUserQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isNewUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Picture getUserPicture(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserPicture(String userId, Picture picture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePicture(User user) {
		// TODO Auto-generated method stub
		
	}
}
