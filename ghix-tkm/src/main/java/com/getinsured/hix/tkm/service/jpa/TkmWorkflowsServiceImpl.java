package com.getinsured.hix.tkm.service.jpa;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.TkmWorkflowsDto;
import com.getinsured.hix.tkm.repository.ITkmWorkflowsRepository;
import com.getinsured.hix.tkm.service.TkmQueuesService;
import com.getinsured.hix.tkm.service.TkmWorkflowsService;
import com.getinsured.hix.tkm.tkmException.TKMException;

@Service("tkmWorkflowsService")
@Repository
public class TkmWorkflowsServiceImpl implements TkmWorkflowsService
{

	@Autowired private ITkmWorkflowsRepository iTkmWorkflowsRepository;
	@Autowired private TkmQueuesService queuesService;
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmWorkflowsServiceImpl.class);
	
	
	@Override
	public List<TkmWorkflows> getTicketTypeList() throws TKMException {
		return iTkmWorkflowsRepository.findAll();
	}

	@Override
	public List<String> getDistinctTicketType() throws TKMException
	{
		return iTkmWorkflowsRepository.getDistinctTicketType();
	}
	
	@Override
	public TkmWorkflows getTicketWorkflowsByCriteria (String type, String ticketCategory) {
		return iTkmWorkflowsRepository.findByCriteria(type, ticketCategory);
	}

	@Override
	public TkmWorkflows getTicketWorkflowsByCriteria (String ticketType) {
		return iTkmWorkflowsRepository.findByCriteria(ticketType);
	}

	@Override
	public List<TkmWorkflows> getActiveTicketTypeList() throws TKMException {
		return iTkmWorkflowsRepository.findByStatus(TkmWorkflows.TicketStatus.ACTIVE);
	}
	
	@Override
	public TkmWorkflows getTicketWorkflow(Integer id) throws TKMException {
		return iTkmWorkflowsRepository.findById(id);
	}

	@Override
	public TkmWorkflows createTicketType(TkmWorkflowsDto tkmWorkflowsDto) {
		TkmWorkflows tkmWorkflows = null;
		
		tkmWorkflows = fillWorkflowsData(tkmWorkflowsDto);
		
		return iTkmWorkflowsRepository.save(tkmWorkflows);
	}
	
	private TkmWorkflows fillWorkflowsData(TkmWorkflowsDto tkmWorkflowsDto) {
		TkmWorkflows tkmWorkflows = null;
		
		try {
			tkmWorkflows = getTicketWorkflow(tkmWorkflowsDto.getId());
			if(null == tkmWorkflows) {
				tkmWorkflows = new TkmWorkflows();
				tkmWorkflows.setCreator(tkmWorkflowsDto.getCreator());
			}
			if(null != tkmWorkflowsDto.getCategory()  && !StringUtils.isEmpty(tkmWorkflowsDto.getCategory())) {
				tkmWorkflows.setCategory(tkmWorkflowsDto.getCategory());				
			}
			if(null != tkmWorkflowsDto.getType() && !StringUtils.isEmpty(tkmWorkflowsDto.getType())) {
				tkmWorkflows.setType(tkmWorkflowsDto.getType());				
			}
			if(null != tkmWorkflowsDto.getFilename() && !StringUtils.isEmpty(tkmWorkflowsDto.getFilename())) {
				tkmWorkflows.setFilename(tkmWorkflowsDto.getFilename());				
			}
			if(null != tkmWorkflowsDto.getDescription() && !StringUtils.isEmpty(tkmWorkflowsDto.getDescription())) {
				tkmWorkflows.setDescription(tkmWorkflowsDto.getDescription());
			}
			if(null != tkmWorkflowsDto.getStatus() && !StringUtils.isEmpty(tkmWorkflowsDto.getStatus().toString())) {
				tkmWorkflows.setStatus(tkmWorkflowsDto.getStatus());
			}
			tkmWorkflows.setUpdator(tkmWorkflowsDto.getUpdator());
			if(null != tkmWorkflowsDto.getQueueId() && !StringUtils.isEmpty(tkmWorkflowsDto.getQueueId().toString())) {
				tkmWorkflows.setTkmQueues(queuesService.findById(tkmWorkflowsDto.getQueueId()));
			}
			if(null != tkmWorkflowsDto.getEmailConfig() && !StringUtils.isEmpty(tkmWorkflowsDto.getEmailConfig())) {
				tkmWorkflows.setEmailConfig(tkmWorkflowsDto.getEmailConfig());
			}
			if(null != tkmWorkflowsDto.getExpDate() && !StringUtils.isEmpty(tkmWorkflowsDto.getExpDate().toString())) {
				tkmWorkflows.setExpDate(tkmWorkflowsDto.getExpDate());
			}
			if(null != tkmWorkflowsDto.getPermission() && !StringUtils.isEmpty(tkmWorkflowsDto.getPermission())) {
				tkmWorkflows.setPermission(tkmWorkflowsDto.getPermission());
			}
			
			if(null != tkmWorkflowsDto.getDefault_Priority() && !StringUtils.isEmpty(tkmWorkflowsDto.getDefault_Priority())) {
				tkmWorkflows.setDefaultPriority(tkmWorkflowsDto.getDefault_Priority());
			}	else if(null != tkmWorkflowsDto.getDefaultPriority() && !StringUtils.isEmpty(tkmWorkflowsDto.getDefaultPriority().getTicketPriority())) {
				tkmWorkflows.setDefaultPriority(tkmWorkflowsDto.getDefaultPriority().getTicketPriority());
			}
			
		
		} catch (TKMException e) {
			e.printStackTrace();
		}
		return tkmWorkflows;
	}
	
	@Override
	public List<String> getTicketCategories(List<String>filteredPermissions) {
		List<String> categories = iTkmWorkflowsRepository.getCategoriesByPermission(filteredPermissions);
		if(null != categories && LOGGER.isDebugEnabled()) {
			LOGGER.debug("======================== returning from getTicketCategoriesByPermission() ======================="+categories.size());	
		}
		return categories;
	}

	@Override
	public List<TkmWorkflows> getWorkflowsByCategory(String category,List<String>filteredPermissions) {
		List<TkmWorkflows> workflows = iTkmWorkflowsRepository.getWorkflowsByCategory(category,filteredPermissions);
		
		if(null != workflows && LOGGER.isDebugEnabled()) {
			LOGGER.debug("======================== returning from getTicketCategoriesByPermission() ======================="+workflows.size());	
		}
		return workflows;
	}

	@Override
	public List<String> getWorkflowTypes() {
		List<String> workflowTypes = iTkmWorkflowsRepository.getWorkflowTypes();
		if(null != workflowTypes) {
			LOGGER.info("======================== returning from getWorkflowTypes() ======================="+workflowTypes.size());	
		}
		return workflowTypes;	
	}
	

	@Override
	public List<String> getTicketTypes() {
		List<String> ticketTypes = iTkmWorkflowsRepository.getTicketTypes();
		if(null != ticketTypes) {
			LOGGER.info("======================== returning from getTicketTypes () ======================="+ticketTypes.size());	
		}
		return ticketTypes;	
	}

	@Override
	public void updateTkmCategoryName(String category,String old_category) throws TKMException {
		iTkmWorkflowsRepository.updateTkmCategoryName(category, old_category);		
	}

	@Override
	public List<String> getPermissionNames(List<String> filteredPermissions) {

		List<String> permissionNames = iTkmWorkflowsRepository.getPermissionNames(filteredPermissions);
		if(null != permissionNames) {
			LOGGER.info("======================== returning from getPermissionNames () ======================="+permissionNames.size());	
		}
		return permissionNames;	
	
		
	}

}
