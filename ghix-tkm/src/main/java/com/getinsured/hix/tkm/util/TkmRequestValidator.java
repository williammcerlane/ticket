package com.getinsured.hix.tkm.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.TkmQueueUsersRequest;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * 
 * @author hardas_d
 * This class will validate the request for each of the business scenarios. 
 */
@Component
public class TkmRequestValidator {
	
	@Autowired private UserService userService;
	@Autowired private UserRoleService userRoleService;

	/**
	 * This method will validate the request for reopen.
	 * Should contain ticket id and the initiator of the request
	 * 
	 * @author hardas_d
	 * @param tkmReq
	 * @return
	 */
	public boolean validReopenRequest(TkmTicketsRequest tkmReq) {
		boolean bValid = true;
		
		Integer ticketId = tkmReq.getTicketId();
		Integer updatedBy = tkmReq.getLastUpdatedBy();
		
		if(ticketId == null || ticketId <= 0) {
			bValid = false;
		}
		
		if(updatedBy == null || updatedBy <=0) {
			bValid = false;
		}
		
		return bValid;
	}

	/**
	 * This method will validate the request for restart.
	 * Should contain ticket id and the initiator of the request
	 * 
	 * @author hardas_d
	 * @param tkmReq
	 * @return
	 */
	public boolean validRestartRequest(TkmTicketsRequest tkmReq) {
		boolean bValid = true;
		
		Integer ticketId = tkmReq.getTicketId();
		Integer updatedBy = tkmReq.getLastUpdatedBy();
		
		if(ticketId == null || ticketId <= 0) {
			bValid = false;
		}
		
		if(updatedBy == null || updatedBy <=0) {
			bValid = false;
		}
		
		return bValid;
	}

	public boolean isReopenAllowed(TkmTickets ticket) {
		boolean bAllowed=false;
		
		String status = ticket.getStatus();
		if(TkmTickets.ticket_status.Canceled.toString().equalsIgnoreCase(status) ||
			TkmTickets.ticket_status.Resolved.toString().equalsIgnoreCase(status)) {
			bAllowed = true;
		}
		
		return bAllowed;
	}
	
	/**
	 * This method will validate the request for Adding CSR's to Workgroups.
	 * Should contain user id
	 * 
	 * @author Kunal Dav
	 * @param queueUserRequest
	 * @return
	 */
	public boolean validAddQueuesToCsrUser(TkmQueueUsersRequest  queueUserRequest) {
		boolean bAllowed=false;
		
		Integer userId = queueUserRequest.getUserId();
		
		// Check user id is not null
		if(userId != null) { 
			bAllowed= true; 
		}		
		
		return bAllowed;
	}
}
