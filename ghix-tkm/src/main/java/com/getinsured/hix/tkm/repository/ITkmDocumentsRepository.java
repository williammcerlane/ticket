package com.getinsured.hix.tkm.repository;

import java.util.List;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.getinsured.hix.model.TkmDocuments;

public interface ITkmDocumentsRepository extends JpaRepository<TkmDocuments, Integer> {
	
	@Query("SELECT t FROM TkmDocuments t WHERE t.ticket.id= :id")
	List<TkmDocuments> findByTicketId(@Param("id") int id);
	
	@Query("SELECT t.documentPath FROM TkmDocuments t WHERE t.ticket.id= :id")
	List<String> findDocPathByTicketId(@Param("id") int id);
}
