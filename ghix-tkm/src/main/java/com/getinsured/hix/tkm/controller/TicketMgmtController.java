package com.getinsured.hix.tkm.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.ws.http.HTTPException;

import org.activiti.engine.FormService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.exception.RestfulException;
import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.dto.tkm.TicketTaskDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CancelTicketRequest;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmQueueUsersRequest;
import com.getinsured.hix.model.TkmQueueUsersResponse;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmQueuesResponse;
import com.getinsured.hix.model.TkmQuickActionDto;
import com.getinsured.hix.model.TkmTicketTaskResponse;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.TkmTicketsTaskRequest;
import com.getinsured.hix.model.TkmUserDTO;
import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.TkmWorkflowsDto;
import com.getinsured.hix.model.TkmWorkflowsResponse;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.tkm.activiti.service.TkmActivitiService;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmWorkflowsRepository;
import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmDocumentService;
import com.getinsured.hix.tkm.service.TkmMgmtService;
import com.getinsured.hix.tkm.service.TkmQueueUsersService;
import com.getinsured.hix.tkm.service.TkmQueuesService;
import com.getinsured.hix.tkm.service.TkmReportService;
import com.getinsured.hix.tkm.service.TkmTicketTaskService;
import com.getinsured.hix.tkm.service.TkmWorkflowsService;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.getinsured.hix.tkm.util.TkmRequestValidator;
import com.getinsured.hix.tkm.util.TkmUtil;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.wordnik.swagger.annotations.ApiOperation;
/**
 * 
 * @author Sharma_k
 * Ticket Management controller
 */

@Controller
@RequestMapping("/ticketmgmt")
public class TicketMgmtController 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(TicketMgmtController.class);
	@Autowired private TicketListService ticketListService;
	@Autowired private TkmWorkflowsService tkmWorkflowsService; 
	@Autowired private TkmQueuesService tkmQueuesService;
	@Autowired private TkmTicketTaskService tkmTicketTaskService;
	@Autowired private TkmActivitiService tkmActivitiService;
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private TkmDocumentService tkmDocumentService;
	@Autowired private FormService formService;
	@Autowired private TkmMgmtService tkmMgmtService;
	@Autowired private TkmQueueUsersService queueUsersService;
	@Autowired private TkmReportService tkmReportService;
	@Autowired private TkmRequestValidator tkmReqValidator;
	@Autowired private ITicketRepository ticketRepository;
	@Autowired private Gson platformGson;
	@Autowired private ITkmWorkflowsRepository iTkmWorkflowsRepository;
	
	private static final String APPEAL_REQUEST_TICKET = "Appeal Request";
	private static final String ERR_MSG_PARAM_MISSING = "Error: Required parameters are missing.";
	private static final String ERR_MSG_NO_HISTORY_FOUND = "No History Records found based on the given ticket id";
	
	private static final String TICKET_CREATED = "TICKET_CREATED";
	private static final String TICKET_CLAIMED = "TICKET_CLAIMED";
	private static final String TICKET_RESOLVED = "TICKET_RESOLVED";
	private static final String TICKET_CANCELLED = "TICKET_CANCELLED";
	private static final String ERROR_IN_TICKET_DESCRIPTION = "Error: Ticket Description/comment length should be between 1 to 2000.";
	
	// Enum to set the error code for Exception handling
	public enum error_code {
		CREATE(10000), HISTORY(10001), TASK(10002), CLAIM(10003), COMPLETE(
				10004), UPDATE(10005), SEARCH(10006), DOCUMENT(10007), EDIT(
				10008), DETAIL(10009), REASSIGN(10011), CANCEL(10012), DELETE(
				10013), SAVECOMMENT(10014), STATUS(10015), SEND_EMAIL(10016), SUBJECT(
				10017), TKT_NUM(10018), TKT_REQUESTER(10019), TKT_TYPE(10020), TKT_TYPELIST(
				10021), TKT_QUEUELIST(10022), USER_TKTLIST(10023), ASSIGNEE(
				10024), EMAIL_RESOLVE(10025), CANCEL_EMAIL(10026), TICKET_LIST(
				10027), SAVEDOC(10028), AUTOCOMPLETE(10029), UPDATE_USER(10030), TSK_PROP(
				10031), LIST(10032), SENDSECUREMSG(10033), REOPEN(10034), RESTART(10035), SAVE_QUEUES_TO_USER(10036);

		private final int code;

		private error_code(int cd) {
			code = cd;
		}

		public int getCode() {
			return code;
		}
	}
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome()
	{
		LOGGER.info("Welcome to GHIX-TKM module");

		return "Welcome to GHIX-TKM module: ";
	}
	
	@RequestMapping(value = "/searchtickets", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> searchTickets(@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("======================== in searchTickets() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		try {
			Map<String, Object> tkmTicketsAndRecordCount = ticketListService.searchTicketsWithNativeQuery(searchCriteria);
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTicketsMap(tkmTicketsAndRecordCount);
		} catch (Exception e) {
			throw new RestfulException("Unable to search tickets.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ageing/report", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> ageingReport(@RequestBody Map<String, Object> reportCriteria) {
		LOGGER.info("======================== in ageingReport() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		try {
			List<ReportDto> ticketReportDtoList = ticketListService.getAgeingReport(reportCriteria);
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTicketReportDtoList(ticketReportDtoList);
		} catch (Exception e) {
			LOGGER.error("Error while searching ticket", e);
			throw new RestfulException("Unable to get aging report.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/gettickettype", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getDistinctTicketType()
	{
		LOGGER.info("======================== in getDistinctTicketType() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();
		
		try {
			List<String> ticketList = tkmWorkflowsService.getDistinctTicketType();
			
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setTicketType(ticketList);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket type", e);
			throw new RestfulException("Unable to get distinct ticket type.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
	@ResponseBody public ResponseEntity<String> findTkmTicketsById(@PathVariable String id) {
		LOGGER.info("======================== in getTkmTicketsDetail() =======================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		Integer ticketId = Integer.valueOf(id);
		try {
			// status value to be changed for more appropriate spellings to be displayed in UI.
			boolean statusDisplayFlag = true;
			TkmTickets tkmTickets = ticketListService.findTkmTicketsById(ticketId,statusDisplayFlag);
				Optional<UserRole> userRole = tkmTickets.getCreator().getUserRole().stream().filter(userrole -> userrole.isDefaultRole()).findFirst();
				if(userRole.isPresent())
				{
					tkmTicketsResponse.setRequestdByrole(userRole.get().getRole());
				}
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTickets(tkmTickets);
			
			if("BROKER".equalsIgnoreCase(tkmTickets.getUserRole().getDescription())) {
			    
				tkmTicketsResponse.setBrokerUserId(String.valueOf(tkmTickets.getRole().getId()));
			}

			
			if(null !=tkmTickets.getModuleId())
			{
				
			Household household =ticketListService.getNameCreatedFor(tkmTickets.getModuleId());
			if(null !=household) {
			tkmTicketsResponse.setCreatedForName(household.getFirstName()+" "+household.getLastName());
				}else {
					if(LOGGER.isDebugEnabled())
					LOGGER.debug("Unable to get household against module id:"+tkmTickets.getModuleId());

				}
			}
			
			
		} catch (Exception e) {
			LOGGER.error("Unable to get the ticket by id", e);
			throw new RestfulException("Unable to get distinct ticket type.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getticketsubject", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getDistinctSubject(@RequestBody TkmTicketsRequest tkmTicketsRequest)
	{
		LOGGER.info("======================== in getDistinctSubject() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			List<String> ticketSubjectList = ticketListService.getDistinctSubject(tkmTicketsRequest.getId());
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTicketsSubjectList(ticketSubjectList);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket subject list", e);
			throw new RestfulException("Unable to get ticket subject.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/getworkflowlist", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> createTicket() {
		LOGGER.info("======================== in getworkflowlist() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();

		try {
			List<TkmWorkflows> ticketTypeList = tkmWorkflowsService.getActiveTicketTypeList();
			
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setTkmWorkflowsList(ticketTypeList);
		} catch (Exception e) {
			LOGGER.error("Unable to get workflow list", e);
			throw new RestfulException("Unable to get workflow list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/queueslist", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> queuesList(@RequestBody TkmTicketsRequest tkmRequest) {
		LOGGER.info("======================== in queuesList() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmQueuesResponse tkmQueuesResponse = new TkmQueuesResponse();

		try {
			Integer userId = tkmRequest.getRequester();
			List<TkmQueues> queuesList = (userId != null) ? queueUsersService.getQueueForUser(userId) : tkmQueuesService.getQueuesList();

			tkmQueuesResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmQueuesResponse.setTkmQueuesList(queuesList);
		} catch (Exception e) {
			LOGGER.error("Unable to get queue list", e);
			throw new RestfulException("Unable to get queue list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmQueuesResponse),HttpStatus.OK);
	}

	// added for HIX-122693 
	@RequestMapping(value = "/defaultManageGrpCount", method = RequestMethod.GET)
	@ResponseBody public ResponseEntity<Map<String,Integer>> getQueueNameWithCount() {
		LOGGER.info("======================== in getDefaultManageGrpCount() =======================");
		Map<String, Integer> queueUserMap=null;
		try {
			 queueUserMap =  queueUsersService.getworkgrpNameWithCount();
		} catch (Exception e) {
			LOGGER.error("Unable to get queue user count map", e);
			throw new RestfulException("Unable to get queue list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		return new ResponseEntity<Map<String,Integer>>(queueUserMap,HttpStatus.OK);
	}
	@RequestMapping(value = "/addnewtickets", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> addNewTicket(@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in addnewticket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tickResponse = new TkmTicketsResponse();
		TkmTickets ticket = null; 
		boolean emailStat = false;
		
		if(!validateTicketDescription(tkmTicketsRequest)){
			tickResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			tickResponse.setErrMsg(ERROR_IN_TICKET_DESCRIPTION);
			return new ResponseEntity<String>(xstream.toXML(tickResponse),HttpStatus.OK);
		}
		
		try{
			ticket = tkmMgmtService.createTicketInDB(tkmTicketsRequest);	
			tkmMgmtService.logTicketEvents(ticket, TICKET_CREATED);
			String emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
			emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Create.toString(),emailConfigValue);
		} catch(Exception e) {
			LOGGER.error("Unable to create a new ticket in DB", e);
			throw new RestfulException("Unable to create a new ticket in DB.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		 		
		if ((ticket != null) && emailStat) {
			try {
				tkmMgmtService.sendTicketCreateEmail(ticket.getId());
				
			} catch (Exception e) {
				LOGGER.info("Unable to send email for the ticket creation: ",  e);
				throw new RestfulException("Unable to send email for the ticket creation.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
		}

		tickResponse.setTkmTickets(ticket);
		tickResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		tickResponse.setErrMsg("");
		return new ResponseEntity<String>(xstream.toXML(tickResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author Sharma_k
	 * @since 02nd May 2013
	 * @param ticketId
	 * @return
	 * @description controller method is written to fetch TkmTicketTasks data for rest calls.
	 */
	@RequestMapping(value = "/gettickettasklist", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketTaskList(@RequestBody String ticketId) {
		LOGGER.info("======================== in getDistinctSubject() =======================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketTaskResponse tkmTicketTaskResponse = new TkmTicketTaskResponse();

		try {
			List<TkmTicketTasks> tkmTicketTasks = tkmTicketTaskService.getTkmTicketTasksList(Integer.parseInt(ticketId));
			tkmTicketTaskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketTaskResponse.setTkmTicketTaskList(tkmTicketTasks);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket task list", e);
			throw new RestfulException("Unable to get ticket task list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketTaskResponse),HttpStatus.OK);
	}

	/**
	 * @author Sharma_k
	 * @param tkmTicketsRequest
	 * @return
	 * This method is written to save ticket closing comments.
	 */
	@RequestMapping(value = "/savecomment", method= RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveTicketComments(
			@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in savecomment() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		String description = tkmTicketsRequest.getComment();
		Integer userId = tkmTicketsRequest.getLastUpdatedBy();

		tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);

		// save comments
		try {
				tkmMgmtService.saveCommentsInDB(tkmTicketsRequest.getTicketId(),
						userId, description, false);
				tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				tkmTicketsResponse.setErrMsg("");
		} catch (TKMException e) {
			LOGGER.error("Unable to save comments.", e);
			throw new RestfulException("Unable to save comments.",HttpStatus.PRECONDITION_FAILED , e);
		}

		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);

	}
	
	@RequestMapping(value = "/editticket", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> editTicket(@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in editticket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		TkmTickets tkmTickets = null;
		try {
			tkmTickets = ticketListService.findTkmTicketsById(tkmTicketsRequest.getId());
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket details.", e);
			throw new RestfulException("Unable to get ticket details.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		tkmTickets.setSubject(tkmTicketsRequest.getSubject());
		tkmTickets.setDescription(tkmTicketsRequest.getDescription());
		tkmTickets.setPriority(tkmTicketsRequest.getPriority());

		AccountUser user = new AccountUser();
		user.setId(tkmTicketsRequest.getLastUpdatedBy());
		tkmTickets.setUpdator(user);

		TkmTickets objtkmTickets = null;
		try {
			objtkmTickets = ticketListService.saveEditedTicket(tkmTickets);
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTickets(objtkmTickets);
		} catch (Exception e) {
			LOGGER.error("Unable to edit the ticket", e);
			throw new RestfulException("Unable to edit the ticket",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getUserTicketList", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getUserTicketList(@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in getUserTicketList() =======================");
		Integer userId = null;
		List<Integer> queueId = null;
		if (tkmTicketsRequest.getAssignee() != null) {
			userId = tkmTicketsRequest.getAssignee();
		}
		if (tkmTicketsRequest.getQueue() != null) {
			queueId = tkmTicketsRequest.getQueue();
		}
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		try {
			List<TkmTickets> tkmTicketsAndRecordCount = ticketListService.getTicketByUser(queueId, userId);
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTicketsList(tkmTicketsAndRecordCount);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket list", e);
			throw new RestfulException("Unable to get ticket list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}

	/**
	 * 
	 * @author hardas_d
	 * @param taskRequest
	 * @return
	 * This method will execute the activiti claim(..) implementation to assign it to group or user
	 */
	@RequestMapping(value = "/claimTicketTask", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> claimTicketTask(@RequestBody TkmTicketsTaskRequest taskRequest) {
		LOGGER.info("======================== in claimTicketTask() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();

		boolean bStat = false;

		if (taskRequest != null) {
			Integer taskId = taskRequest.getTaskId();
			String activitiTaskId = taskRequest.getActivitiTaskId();
			Integer userId = taskRequest.getUserId();

			try {
				TkmTicketTasks ticketTasks = tkmTicketTaskService.findByTaskId(taskId);

				TkmTickets ticket = ticketListService.findTkmTicketsById(ticketTasks.getTicket().getId());
				if (ticket == null) {
					bStat = false;
					LOGGER.error("Unable to get the ticket data");
				} else {
					// Get the ticket status, required to send mail if status 'Unclaimed'
					String oldStatus = ticket.getStatus().toString();
					
					// Claim the ticket
					bStat = tkmMgmtService.claimTicketTask(taskId, activitiTaskId, userId);

					if(bStat && // Ticket claimed
						((TkmTickets.ticket_status.UnClaimed.toString().compareToIgnoreCase(oldStatus) == 0) ||
						 (TkmTickets.ticket_status.Restarted.toString().compareToIgnoreCase(oldStatus) == 0) ||
						 (TkmTickets.ticket_status.Reopened.toString().compareToIgnoreCase(oldStatus) == 0)
						)) {
						// update the ticket status
						ticket = ticketListService.updateStatus(ticketTasks.getTicket().getId(),
								TkmTickets.ticket_status.Open.toString(), userId, null);

						bStat = (ticket != null);
						String emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
						boolean emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Open.toString(),emailConfigValue);
						// Send an email for the status changed to 'Open' if it is not an Appeal Request
						if (bStat && emailStat) {
							tkmMgmtService.sendTicketOpenEmail(ticket.getId());
						}
					}
				}
				
				tkmMgmtService.logTicketEvents(ticket.getId(), TICKET_CLAIMED);
				
				taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} catch (Exception e) {
				LOGGER.error("Unable to claim the ticket", e);
				throw new RestfulException("Unable to claim the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
		}
		
		
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param taskRequest
	 * @return
	 * This method will complete the task and will insert the next task if available.
	 * 
	 * If no other task is available and the activiti process is fully executed,
	 * the status of the corresponding ticket will be changed to "Completed"
	 */
	@RequestMapping(value = "/completeTicketTask", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> completeTicketTask(
			@RequestBody TkmTicketsTaskRequest taskRequest) {
		LOGGER.info("======================== in completeTicketTask() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();

		boolean bStat = false;

		if (taskRequest != null) {
			Integer taskId = taskRequest.getTaskId();
			String activitiTaskId = taskRequest.getActivitiTaskId();
			String comments = taskRequest.getComments();
			String processInstanceId = taskRequest.getProcessInstanceId();
			String action = taskRequest.getAction();
			Map<String, Object> taskVariables = taskRequest.getTaskVariables();
			Integer userId = taskRequest.getUserId();
			boolean bComplete = false;
			TkmTicketTasks ticketTasks=null;
		 if(!action.equalsIgnoreCase("Awaiting Internal") && !action.equalsIgnoreCase("Awaiting Requestor")) {
			try {
				 ticketTasks = tkmTicketTaskService.findByTaskId(taskId);	
				TkmTickets tickets = ticketListService.findByTicketNumber(ticketTasks.getTicket().getNumber());
				List<TaskFormProperties> formProperties =tkmActivitiService.getWorlFlowsMergedProperties(tickets.getTkmWorkflows().getFilename(),tickets.getId());
				taskResponse.setTkmTaskProperties(formProperties);
				
				bStat = tkmMgmtService.completeTicketTask(taskId,activitiTaskId, userId, comments, taskVariables, false, processInstanceId);
				
				// Check if the task BPM file is fully executed
				bComplete = tkmActivitiService.isProcessComplete(processInstanceId);
				
				
				
				taskResponse.setActivitiProcessCompleted(bComplete);
				
				tkmMgmtService.logTicketEvents(tickets.getId(), TICKET_RESOLVED);
				
			} catch (Exception e) {
				LOGGER.error("Unable to complete the ticket", e);
				throw new RestfulException("Unable to complete the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
		  }
		  else if(action.equalsIgnoreCase("Awaiting Internal") || action.equalsIgnoreCase("Awaiting Requestor")){
			   String updateAction = action.equalsIgnoreCase("Awaiting Internal")? TkmTickets.ticket_status.Awaiting_Internal.toString() : TkmTickets.ticket_status.Awaiting_Requestor.toString();
				try {
					 ticketTasks = tkmTicketTaskService.findByTaskId(taskId);	
					 TkmTickets tickets = ticketListService.updateStatus(ticketTasks.getTicket().getId(), 
								updateAction, userId, null);
					 
					 if(tickets != null && !StringUtils.isEmpty(comments)) {
								TkmTicketTasks task = tkmTicketTaskService.findByTaskId(taskId);
								boolean bStatus = tkmMgmtService.saveCommentsInDB(task.getTicket().getId(), userId, comments, false);

					 }
					 
				}
				catch (Exception e) {
					LOGGER.error("Unable to update the ticket", e);
					throw new RestfulException("Unable to update the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
				}
			}
			

			// Change the status of the ticket when activity process is completed
			if (bStat && bComplete) {
				try {
					
					TkmTickets tickets = ticketListService.updateStatus(ticketTasks.getTicket().getId(), 
							TkmTickets.ticket_status.Resolved.toString(), userId, null);
					 String emailConfigValue = tickets.getTkmWorkflows().getEmailConfig();
					 boolean emailAllowed =tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Complete.toString(),emailConfigValue);
					if ((tickets != null) && emailAllowed) {
						String ticketStatus = taskVariables.containsKey("ticketStatus") ? taskVariables.get("ticketStatus").toString() : "";
						tkmMgmtService.sendTicketResolvedEmail(tickets.getId(), ticketStatus, comments);
					}
						
				} catch (Exception e) {
					LOGGER.info("Unable to send email for the resolved ticket : ", e);
					throw new RestfulException("Unable to send email for the resolved ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
				}
			}	
		}
				
		if(StringUtils.isEmpty(taskResponse.getErrMsg())) {
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			taskResponse.setErrCode(0);
		}
		
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	/*
	 * HIX-122689
	 */
	@RequestMapping(value = "/getNewTicketTaskToClaim", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> getNewTicketTaskToclaim(
			@RequestBody TkmTicketsTaskRequest taskRequest) {
		LOGGER.info("======================== in completeTicketTask() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();
		TicketTaskDto ticketTaskDTO=null;
		boolean bStat = false;

		if (taskRequest != null) {
			Integer userId = taskRequest.getUserId();
			String workGroup = taskRequest.getWorkGroup();
			try {
				
					 ticketTaskDTO = tkmTicketTaskService.getNewTicketTaskToClaim(userId,workGroup);
					
				
			} catch (Exception e) {
				LOGGER.error("Unable to complete the ticket", e);
				throw new RestfulException("Unable to complete the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}

		}
				
			if(StringUtils.isEmpty(taskResponse.getErrMsg())) {
				taskResponse.setTicketTaskDto(ticketTaskDTO);
				taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				taskResponse.setErrCode(0);
			}
			
			return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * REST call to create the new ticket
	 */
	@RequestMapping(value = "/tickets/ticket/create", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> createNewTicket(@RequestBody TkmTicketsRequest ticketRequest) {
		LOGGER.info("======================== in createNewTicket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();

		if(!valCreateTicketReq(ticketRequest)) {
			throw new RestfulException("Missing parameters.",HttpStatus.PRECONDITION_FAILED);
		}
		
		if(!validateTicketDescription(ticketRequest)){
			throw new RestfulException("Invalid description.",HttpStatus.PRECONDITION_FAILED);
		}

		// valid data - create a ticket
		TkmTickets ticket = null;
		boolean emailStat = false;
		try {
			ticket = tkmMgmtService.createTicketInDB(ticketRequest);
			tkmMgmtService.logTicketEvents(ticket, TICKET_CREATED);
			saveComments(ticketRequest, ticket, false);
			saveDocuments(ticketRequest, ticket, false);
			
			String emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
			emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Create.toString(), emailConfigValue);		
		} catch (Exception e) {
			LOGGER.error("Error occured while creating a ticket", e);
			throw new RestfulException("Error occured while creating a ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		try {
			
			if (StringUtils.isEmpty(ticketResponse.getErrMsg())	&& emailStat) {
				tkmMgmtService.sendTicketCreateEmail(ticket.getId());
			}
		} catch (Exception e) {
			LOGGER.error("Unable to send email for the ticket creation: ", e);
			throw new RestfulException("Unable to send email for the ticket creation.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if(StringUtils.isEmpty(ticketResponse.getErrMsg())) {
			ticketResponse.setTkmTickets(ticket);
			ticketResponse.setErrMsg("");
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}

	/**
	 *
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * REST call to create the new ticket
	 */
	@RequestMapping(value = "/tickets/ticket/createOrUpdate", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> createOrUpdateTicket(@RequestBody TkmTicketsRequest ticketRequest) {
		LOGGER.info("======================== in createOrUpdateTicket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();

		if(!valCreateTicketReq(ticketRequest)) {
			throw new RestfulException("Missing parameters.",HttpStatus.PRECONDITION_FAILED);
		}

		if(!validateTicketDescription(ticketRequest)){
			throw new RestfulException("Invalid description.",HttpStatus.PRECONDITION_FAILED);
		}

		// Valid data - create or update ticket
		TkmTickets ticket = null;
		boolean createTicket = true;
		boolean emailStat = false;
		try {
			List<String> validTicketStatuses = Arrays.asList("UnClaimed", "Open", "Restarted", "Reopened", TkmTickets.ticket_status.Awaiting_Internal.name(), TkmTickets.ticket_status.Awaiting_Requestor.name());
			Map<String, String> formProperties = ticketRequest.getTicketFormPropertiesMap();

			List<TkmTickets> existingTicket = null;
			try {
				existingTicket = ticketRepository.findExistingTicket(Long.valueOf(formProperties.get("applicantID")), ticketRequest.getType(), validTicketStatuses);
			} catch (Exception ex) {
				LOGGER.error("createOrUpdateTicket::error getting existing ticket", ex);
			}

			if(existingTicket != null && existingTicket.size() > 0) {
				createTicket = false;
				ticket = existingTicket.get(0);
				AccountUser updatedBy = new AccountUser();
				Integer updatorId = ticketRequest.getLastUpdatedBy();
				updatedBy.setId((updatorId != null) ? updatorId :  ticketRequest.getCreatedBy());
				ticket.setUpdator(updatedBy); // Updator
				if(ticket.getStatus().equals(TkmTickets.ticket_status.Awaiting_Internal.name()) || ticket.getStatus().equals(TkmTickets.ticket_status.Awaiting_Requestor.name())) {
					ticket.setStatus(TkmTickets.ticket_status.Open.name());
				}
			} else {
				ticket = tkmMgmtService.createTicketInDB(ticketRequest);
			}

			if(createTicket) {
				tkmMgmtService.logTicketEvents(ticket, TICKET_CREATED);
				String emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
				emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Create.toString(), emailConfigValue);
			}

			saveComments(ticketRequest, ticket, false);
			saveDocuments(ticketRequest, ticket, false);
		} catch (Exception e) {
			LOGGER.error("createOrUpdateTicket::Error occured while creating a ticket", e);
			throw new RestfulException("Error occured while creating a ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		try {
			if (StringUtils.isEmpty(ticketResponse.getErrMsg()) && emailStat && createTicket) {
				tkmMgmtService.sendTicketCreateEmail(ticket.getId());
			}
		} catch (Exception e) {
			LOGGER.error("createOrUpdateTicket::Unable to send email for the ticket creation: ", e);
			throw new RestfulException("Unable to send email for the ticket creation.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		if(StringUtils.isEmpty(ticketResponse.getErrMsg())) {
			//ticketResponse.setTkmTickets(ticket);
			ticketResponse.setErrMsg("");
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}

		return new ResponseEntity<String>(platformGson.toJson(ticketResponse),HttpStatus.OK);
	}

	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * This REST method will update the ticket details.
	 */
	@RequestMapping(value = "/updateticket", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> updateTicket(@RequestBody TkmTicketsRequest ticketRequest)
	{
		LOGGER.info("======================== in updateTicket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
	
		ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		
		if(valUpdateTicketReq(ticketRequest)) {
			ticketResponse.setErrMsg("");
			
			Integer ticketId = ticketRequest.getId();
			String desc = ticketRequest.getComment();
			Integer updatorId = ticketRequest.getLastUpdatedBy();

			//save comments				
			try {
				tkmMgmtService.saveCommentsInDB(ticketId, updatorId, desc, true);
			} catch (TKMException tke) {
				LOGGER.error("Unable to update ticket comments", tke);
				throw new RestfulException("Unable to update ticket comments.",HttpStatus.INTERNAL_SERVER_ERROR , tke);
			}
							
			//save documents
			List<String> docLinks = ticketRequest.getDocumentLinks();
			if((docLinks != null) && (!docLinks.isEmpty())) {					
				try {
				 tkmMgmtService.saveDocumentsInDB(ticketId, updatorId, docLinks, true);
				} catch (Exception e) {
					LOGGER.error("Unable to update document list of the ticket", e);
					throw new RestfulException("Unable to update document list of the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
				}
			}		
			
			//set the updator parameter
			 try {
				ticketListService.setUpdator(ticketId, updatorId);
			} catch (Exception e) {
				LOGGER.error("Unable to set the updator", e);
				throw new RestfulException("Unable to set the updator.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
			
			//get active task list
			try {
				 tkmMgmtService.autoClaimCompleteTasks(ticketId, updatorId, desc, null);
			} catch (Exception e) {
				LOGGER.error("Unable to auto-complete the ticket", e);
				throw new RestfulException("Unable to auto-complete the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);			}
		}  
		
		if(StringUtils.isEmpty(ticketResponse.getErrMsg())) {
			LOGGER.info("The ticket has been updated successfully.");
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketResponse.setErrMsg("");
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * This REST method will get the status of the ticket in the TkmTicketResponse.
	 */
	@RequestMapping(value = "/getticketstatus", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketStatus(@RequestBody TkmTicketsRequest ticketRequest) {
		LOGGER.info("======================== in getticketstatus() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
		
		ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		if ((ticketRequest != null) && (ticketRequest.getId() != null)) {
			// valid data - get a status a ticket
			try {
				String status = tkmMgmtService.getTicketStatusFromDB(ticketRequest.getId());
				
				ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				ticketResponse.setErrMsg("");
				ticketResponse.setTicketStatus(status.toUpperCase());
			} catch (Exception e) {
				LOGGER.error("Unable to get ticket status", e);
				throw new RestfulException("Unable to get ticket status.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
		}

		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}

	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * This REST method will cancel the ticket.
	 */
	@RequestMapping(value = "/cancelticket", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> cancelTicket(@RequestBody TkmTicketsRequest ticketRequest)
	{
		LOGGER.info("======================== in cancelticket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
	 	ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		boolean emailStat=false;
		boolean bStatus = false;
		String emailConfigValue = null;
		
		if(null != ticketRequest.getId()) {
			try {
				TkmTickets ticket = ticketListService.findTkmTicketsById(ticketRequest.getId());
				if (ticket != null) {
					emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
				}
				
	             bStatus = tkmMgmtService.cancelTicketAndTasks(ticketRequest.getId(), ticketRequest.getLastUpdatedBy(),ticketRequest.getComment(), false);
	             tkmMgmtService.logTicketEvents(ticketRequest.getId(), TICKET_CANCELLED);
			} catch (Exception e) {
				LOGGER.error("Unable to cancel the ticket", e);
				throw new RestfulException("Unable to cancel the ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);
			}
		}

		if(bStatus) {
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketResponse.setErrMsg("");
			try {
				emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Cancel.toString(),emailConfigValue);
				if(bStatus && emailStat){
				tkmMgmtService.sendTicketCancelledEmail(ticketRequest.getId());
				}
			} catch (Exception e) {
				LOGGER.info("Unable to send email for the cancelled ticket: ", e);
				throw new RestfulException("Unable to send email for the cancelled ticket.",HttpStatus.INTERNAL_SERVER_ERROR , e);			}
		} 
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return This REST method will set the ticket details in the
	 *         TkmTicketResponse.
	 */
	@RequestMapping(value = "/getticketdetails", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketDetails(@RequestBody TkmTicketsRequest ticketRequest) {
		LOGGER.info("======================== in getticketdetails() =======================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();

		ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		if ((ticketRequest != null) && (ticketRequest.getId() != null)) {

			Integer ticketId = ticketRequest.getId();
			// valid data - get the details of the ticket			
			try {
				// status value to be changed for more appropriate spellings to be displayed in UI.
				TkmTickets ticket = ticketListService.findTkmTicketsById(ticketId,false);
				ticketResponse.setTkmTickets(ticket);
				
				// get ticket status
				String ticketStatus = tkmMgmtService.getTicketStatusFromDB(ticketId);
				ticketStatus = (ticketStatus == null) ? ticketStatus : ticketStatus.toUpperCase();
				ticketResponse.setTicketStatus(ticketStatus);
				
				// task list for ticket
				List<TkmTicketTasks> tasks  = tkmTicketTaskService.getTkmTicketTasksList(ticketId);
				ticketResponse.setTaskList(tasks);
				
				// comment list for the ticket
				CommentTarget commentTarget = 
						commentTargetService.findByTargetIdAndTargetType(ticketId.longValue(), TargetName.TICKET);
				if (commentTarget != null) {
					ticketResponse.setCommentList(commentTarget.getComments());
				}
				
				// document list for ticket
				List<String> docList = tkmDocumentService.getDocumentPathList(ticketId);
				if (docList != null) {
					ticketResponse.setDocumentPathList(docList);
				}
				
				ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				ticketResponse.setErrMsg("");			
			} catch (Exception e) {
				LOGGER.error("Unable to get ticket details", e);
				throw new RestfulException("Unable to get ticket details.",HttpStatus.INTERNAL_SERVER_ERROR , e);			}
		}

		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws HTTPException
	 * fetches ticketHistory details.
	 */
	@RequestMapping(value = "/findhistorybyid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> findTkmTicketsHistoryById(@PathVariable String id) {
		LOGGER.info("======================== in findTkmTicketsHistoryById() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		if (id == null || id.equalsIgnoreCase("0")) {
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			tkmTicketsResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		} else {
			Integer ticketId = Integer.valueOf(id);
			List<HistoryDto> ticketHistoryList = null;
			
			try {
				//ticketHistoryList = tkmTicketTaskService.loadTicketStatusHistory(ticketId,isDetailedView);
                ticketHistoryList = tkmTicketTaskService.loadTicketAndTaskHistory(ticketId, true);
			} catch (Exception e) {
				LOGGER.error("Unable to get ticket history", e);
				throw new RestfulException("Unable to get ticket details.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.HISTORY.code), e);			
			}
			
			if (ticketHistoryList != null && !ticketHistoryList.isEmpty()) {
				tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				tkmTicketsResponse.setTkmTicketHistory(ticketHistoryList);
			}
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param taskId
	 * @return
	 * This method will return the form properties of the given task
	 */
	@RequestMapping(value = "/getTaskFormProperties", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTaskFormProperties(@RequestBody String taskId)
	{
		LOGGER.info("======================== in getTaskFormProperties() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();
		List<FormProperty> formProperties = null;
	
		try {
			formProperties = formService.getTaskFormData(taskId).getFormProperties();
		} catch (Exception e) {
			LOGGER.error("Unable to task data from activiti", e);
			throw new RestfulException("Unable to task data from activiti.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.TSK_PROP.code), e);
		}
		
		List<TaskFormProperties> taskFormPropertiesList = new ArrayList<TaskFormProperties>();
		
		if(formProperties != null) {
			String propertyName;
			String propertyType;
			String propertyId;
			List<String> propertyValues=new ArrayList<String>();

			for (FormProperty formProperty : formProperties) {
				propertyName = formProperty.getName();
				propertyId = formProperty.getId();
				
				if (formProperty.getType() instanceof EnumFormType) {
					propertyType = "enum";
					@SuppressWarnings(value = "unchecked")
					Map<String, String> propertiesMap = (Map<String, String>) formProperty.getType().getInformation("values");
					if(propertiesMap != null) {
						for (Entry<String, String> propertiesValue : propertiesMap.entrySet()) {
							propertyValues.add(propertiesValue.getValue());
						}
					}
				} else {
					propertyType = "string";
					propertyValues = new ArrayList<String>();
				}
	
				TaskFormProperties taskFormProperties = new TaskFormProperties();
				taskFormProperties.setId(propertyId);
				taskFormProperties.setName(propertyName);
				taskFormProperties.setType(propertyType);
				taskFormProperties.setValues(propertyValues);
	
				taskFormPropertiesList.add(taskFormProperties);
			}
		}	
		
		taskResponse.setTkmTaskProperties(taskFormPropertiesList);
		taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	private boolean saveDocuments(TkmTicketsRequest ticketRequest, TkmTickets ticket, boolean bUpdate) throws TKMException {
		boolean bStatus = true;
		List<String> docLinks = ticketRequest.getDocumentLinks();
		
		if ((docLinks != null) && (docLinks.size() > 0)) {
			bStatus = tkmMgmtService.saveDocumentsInDB(	ticket.getId(), ticketRequest.getCreatedBy(), docLinks, bUpdate);
		}
		
		return bStatus;
	}

	private boolean saveComments(TkmTicketsRequest ticketRequest, TkmTickets ticket, boolean bUpdate) throws TKMException {
		boolean bStatus = true;
		if (!StringUtils.isEmpty(ticketRequest.getComment())) {
			bStatus = tkmMgmtService.saveCommentsInDB(ticket.getId(), 
									ticketRequest.getCreatedBy(),
									ticket.getDescription(), bUpdate);
		}
		
		return bStatus;
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @param ticketRequest
	 * @return
	 * This REST method will return the ticket list based on the search criteria.
	 */
	@RequestMapping(value = "/getticketlist", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketList(@RequestBody TkmTicketsRequest ticketRequest)
	{
		LOGGER.info("======================== in getticketlist() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
		ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		//Check the mandatory variables
		if(ticketRequest == null) {
			throw new RestfulException("Received empty request.",HttpStatus.PRECONDITION_FAILED);		
		}

		try {
			List<TkmTickets> ticketList = tkmMgmtService.searchTicketsByCriteria(ticketRequest);
			ticketResponse.setTkmTicketsList(ticketList);
			
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketResponse.setErrMsg("");
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket list as per search criteria", e);
			throw new RestfulException("Unable to get ticket list as per search criteria.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}
	
	private boolean valCreateTicketReq(TkmTicketsRequest request) {
		boolean bStatus = false;

		if(request == null) {
			return bStatus;
		}
	
		//check tkmworkflow data
		if(StringUtils.isEmpty(request.getCategory()) 
				|| StringUtils.isEmpty(request.getType())) {
			return bStatus;
		}

		//check creator id, requester id and user role id
		if((request.getCreatedBy() == null) 
				|| (request.getRequester() == null) 
				|| (request.getUserRoleId() == null)) {
			return bStatus;
		}
		
		//check comments
		if(StringUtils.isEmpty(request.getComment())) {
			return bStatus;
		}
		
		bStatus = true;
		return bStatus;
	}
	
	private boolean valUpdateTicketReq(TkmTicketsRequest request) {
		boolean bStatus = false;
		
		if(request == null) {
			return bStatus;
		}
		
		if((request.getId() == null) 
				|| (request.getLastUpdatedBy() == null) 
				|| StringUtils.isEmpty(request.getComment())) {
			return bStatus;
		}
			
		bStatus = true;
		return bStatus;
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param ticketRequest
	 * @return
	 * This REST method will return the ticket list based on the search criteria.
	 */
	@RequestMapping(value = "/getticketdocuments", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketDocuments(@RequestBody String ticketId) {
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse ticketsResponse = new TkmTicketsResponse();
		
		try {
			List<TkmDocuments> documentList = tkmDocumentService.getDocumentList(Integer.parseInt(ticketId));
			
			ticketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketsResponse.setDocumentList(documentList);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket documents", e);
			throw new RestfulException("Unable to get ticket documents.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.DOCUMENT.code), e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketsResponse),HttpStatus.OK);
	}
	

	@RequestMapping(value = "/getTicketDocumentsAndFormParams", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketDocumentsAndFormParams(@RequestBody String ticketId) {
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse ticketsResponse = new TkmTicketsResponse();
		
		try {
			int ticket = Integer.parseInt(ticketId.toString());
			List<TkmDocuments> documentList = tkmDocumentService.getDocumentList(ticket);
			// status value to be changed for more appropriate spellings to be displayed in UI.
			boolean statusDisplayFlag = true;
			TkmTickets tkmTicket =    ticketListService.findTkmTicketsById(ticket,statusDisplayFlag);
			List<TaskFormProperties> formProperties =tkmActivitiService.getWorlFlowsMergedProperties(tkmTicket.getTkmWorkflows().getFilename(),ticket);
			
			ticketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketsResponse.setDocumentList(documentList);
			ticketsResponse.setTkmTicketFomProperties(formProperties);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket documents", e);
			throw new RestfulException("Unable to get ticket documents.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.DOCUMENT.code), e);		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketsResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param ticket id
	 * @return
	 * This REST method will return the active task list based on the ticket id.
	 */
	@RequestMapping(value = "/getTicketActiveTask", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketActiveTask(@RequestBody String ticketId)
	{
		XStream xstream = GhixUtils.xStreamHibernateJSONMashaling();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();
		List<TkmTicketTasks> ticketTasks =  null;
		List<Task> activeTasks = null ;
		List<TkmTicketTasks> tkmActiveTasks = null;
		
		taskResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		taskResponse.setErrCode(error_code.LIST.code);
		
		try {
			ticketTasks = tkmTicketTaskService.getTkmTicketTasksList(Integer.parseInt(ticketId));
			if(null!=ticketTasks && !ticketTasks.isEmpty()){
				int index = ticketTasks.size() -1;
				activeTasks = tkmActivitiService.getTaskList(null,null, ticketTasks.get(index).getProcessInstanceId());
				//activeTasks	= tkmActivitiService.getTaskList(null,null, ticketTasks.get(0).getProcessInstanceId());
			}
			
			tkmActiveTasks = new ArrayList<TkmTicketTasks>();
			if(null!=activeTasks) {
				for (Task task : activeTasks) {
					tkmActiveTasks.add(tkmTicketTaskService.findTkmTasksByActivitiTaskId(task.getId()));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket active tasks", e);
			throw new RestfulException("Unable to get ticket active tasks.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.DOCUMENT.code), e);		
		}
		
		if(null!=tkmActiveTasks&&!tkmActiveTasks.isEmpty()){
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			taskResponse.setTkmTicketTaskList(tkmActiveTasks);
		}
		
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param ticket id
	 * @return
	 * This REST method will reassign the active task.
	 */
	@RequestMapping(value = "/reassignTicketActiveTask", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> reassignTicketActiveTask(@RequestBody TkmTicketsTaskRequest  tkmTicketsTaskRequest)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();
		boolean reassignSuccessful = false;
		
		try {
			reassignSuccessful = tkmMgmtService.reassignTkkActiveTask(tkmTicketsTaskRequest.getTaskId(), tkmTicketsTaskRequest.getGroupId(), tkmTicketsTaskRequest.getUserId(),tkmTicketsTaskRequest.getTaskTicketId());
		} catch (Exception e) {
			LOGGER.error("Unable to reassign the ticket", e);
			throw new RestfulException("Unable to reassign the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.REASSIGN.code), e);		
		}
		
		if(reassignSuccessful){
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			taskResponse.setActivitiProcessCompleted(true);
		}
	
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/updatequeueusers", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> updateQueueUsers(@RequestBody TkmQueueUsersRequest  queueUserRequest,@RequestParam(value = "addFlag", required = false) boolean addFlag)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmQueueUsersResponse queueUserResp = new TkmQueueUsersResponse();
		
		if(!valUpdateQueue(queueUserRequest)) {
			queueUserResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			queueUserResp.setErrMsg(ERR_MSG_PARAM_MISSING);
			return new ResponseEntity<String>(xstream.toXML(queueUserResp),HttpStatus.OK);
		}
	
		try {
			tkmMgmtService.updateQueueUsers(queueUserRequest.getQueueId(), queueUserRequest.getUserList(),addFlag);
			queueUserResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
			queueUserResp.setErrMsg("");
		} catch (Exception e) {
			LOGGER.error("Unable to the update the users of the queue", e);
			throw new RestfulException("Unable to the update the users of the queue.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.UPDATE.code), e);		
		}
		
		return new ResponseEntity<String>(xstream.toXML(queueUserResp),HttpStatus.OK);
	}

	/**
	 * @author hardas_d
	 * @return boolean
	 * @param queueUserRequest
	 * @return
	 */
	private boolean valUpdateQueue(TkmQueueUsersRequest queueUserRequest) {
		boolean bStatus = false;
		
		bStatus = (queueUserRequest.getQueueId() == null) ? bStatus : true;
		return bStatus;
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param TkmTicketsTaskRequest id
	 * @return
	 * This REST method will reassign the active task.
	 */
	@RequestMapping(value = "/saveTicketAttachments", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> saveTicketAttachments(@RequestBody TkmTicketsRequest  tkmTicketsRequest)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse taskResponse = new TkmTicketsResponse();
		
		try {
			tkmMgmtService.saveDocumentsInDB(tkmTicketsRequest.getTicketId(), tkmTicketsRequest.getCreatedBy(), tkmTicketsRequest.getDocumentLinks(), false);
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			LOGGER.error("Unable to save the new document for the ticket", e);
			throw new RestfulException("Unable to save the new document for the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SAVEDOC.code), e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	/**
	 * @author Sharma_k
	 * @since 08th August 2013
	 * @param tkmTicketsTaskRequest
	 * @return
	 * Jira Id: HIX-14168 Manage ticket
	 */
	@RequestMapping(value="/gettickettaskbyassignee", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketTaskByAssignee(@RequestBody TkmTicketsTaskRequest tkmTicketsTaskRequest)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse tkmTicketTaskResponse = new TkmTicketTaskResponse();
		
		tkmTicketTaskResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmTicketTaskResponse.setErrCode(error_code.ASSIGNEE.code);
	
		List<TkmTicketTasks> tkmTicketTaskList = null;
		try {
			tkmTicketTaskList = tkmTicketTaskService.getTicketTaskByAssignee(tkmTicketsTaskRequest.getAssigneeName());
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket task", e);
			throw new RestfulException("Unable to get ticket task.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if (tkmTicketTaskList != null) {
			tkmTicketTaskResponse.setTkmTicketTaskList(tkmTicketTaskList);
			tkmTicketTaskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketTaskResponse.setErrCode(0);
		}
		  
		return new ResponseEntity<String>(xstream.toXML(tkmTicketTaskResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value="/geTaskUserInfobyAssignee", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> geTaskUserInfobyAssignee(@RequestBody TkmTicketsTaskRequest tkmTicketsTaskRequest)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse tkmTicketTaskResponse = new TkmTicketTaskResponse();
		
		tkmTicketTaskResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmTicketTaskResponse.setErrCode(error_code.ASSIGNEE.code);
		List<TkmUserDTO> tkmUserDTOs = new ArrayList<>();
	
		try {
			tkmUserDTOs = tkmTicketTaskService.geTaskUserInfoByAssignee(tkmTicketsTaskRequest.getAssigneeName());
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket task", e);
			throw new RestfulException("Unable to get ticket task.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if (tkmUserDTOs != null && tkmUserDTOs.size()>0) {
			tkmTicketTaskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketTaskResponse.setErrCode(0);
			tkmTicketTaskResponse.setTkmUserDTO(tkmUserDTOs);
		}
		  
		return new ResponseEntity<String>(xstream.toXML(tkmTicketTaskResponse),HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * @author kaul_s
	 * @param taskId
	 * @return
	 * This method will return the form properties of the given task
	 */
	@RequestMapping(value = "/deleteTicketAttachments", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> deleteTicketAttachments(@RequestBody String docId)
	{
		LOGGER.info("======================== in getTaskFormProperties() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
		boolean delete = false;
		try {
			delete = tkmDocumentService.deleteDocument(docId);
		} catch (Exception e) {
			LOGGER.error("Unable to delete the ticket document", e);
			throw new RestfulException("Unable to delete the ticket document.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.DELETE.code), e);
		}
		
		if(delete) {			
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @return String
	 * @return List of ticket numbers
	 */
	@RequestMapping(value = "/getticketnumbers", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketNumbers()
	{
		LOGGER.info("======================== in getTicketNumbers() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		List<String> ticketNumbers = null;
		
		tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmTicketsResponse.setErrCode(error_code.TKT_NUM.code);
		
		try {
			ticketNumbers = ticketListService.getDistinctNumbers();
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket number list", e);
			throw new RestfulException("Unable to get ticket number list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if (ticketNumbers != null) {
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTicketNumbers(ticketNumbers);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author hardas_d
	 * @return String
	 * @return List of ticket requesters
	 */
	@RequestMapping(value = "/getrequesters", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getRequesters(@RequestBody String userText)
	{
		LOGGER.info("======================== in getrequesters() =======================");
//		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmTicketsResponse.setErrCode(error_code.TKT_REQUESTER.code);
		
		List<AccountUser> requesters = null;
		try {
			requesters = ticketListService.getRequesters(userText);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket requester list", e);
			throw new RestfulException("Unable to get ticket requester list.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if(requesters != null){
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setRequesters(requesters);
		}
		return new ResponseEntity<String>(TkmUtil.getJsonForTkmTicketsResponse(tkmTicketsResponse,"getrequesters"),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSimpleOrDetailedHistory", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getSimpleOrDetailedHistory(@RequestBody TkmTicketsRequest tkmRequest)
	{
		LOGGER.info("======================== in getSimpleOrDetailedHistory() =======================");
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
				
		if(!valHistoryReq(tkmRequest)) {
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			tkmTicketsResponse.setErrMsg(ERR_MSG_NO_HISTORY_FOUND);
			return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
		}
		
		Integer ticketId = tkmRequest.getTicketId();
		List<HistoryDto> ticketHistoryList=null;
		boolean isDetailedView = tkmRequest.isDetailedHistoryView();
		try {
			//ticketHistoryList = tkmTicketTaskService.loadTicketStatusHistory(ticketId, isDetailedView);
			ticketHistoryList = tkmTicketTaskService.loadTicketAndTaskHistory(ticketId, isDetailedView);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket history", e);
			throw new RestfulException("Unable to get ticket history.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.HISTORY.code),  e);
		}
		if (ticketHistoryList != null && !ticketHistoryList.isEmpty()) {
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setErrMsg("");
			tkmTicketsResponse.setTkmTicketHistory(ticketHistoryList);
		} 
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);	
	}
	
	
	/**
	 * 
	 * @author yadav_sa	
	 * @return String
	 * @return Ticket Counts By Priority
	 */
	
	@RequestMapping(value = "/getTicketCountForPieReport", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketCountForPieReport(@RequestBody String criteria)
	{
		LOGGER.info("======================== in getTicketCount For PieReport =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmTicketsResponse.setErrCode(error_code.TKT_REQUESTER.code);
		
		Map<String,Integer> ticketCountMap = null;
		List<Object[]> listOfticketAndCount = null;
		
		try {
			listOfticketAndCount = ticketListService.getTkmTicketsForPieReport(criteria);
			
			if(listOfticketAndCount != null && !listOfticketAndCount.isEmpty()) {
				
				ticketCountMap = new HashMap<String, Integer>();
				
				for(Object[] ticketAndCount : listOfticketAndCount) {
					
					String priority = (String)ticketAndCount[0];
					Integer count = Integer.valueOf(ticketAndCount[1].toString());
					
					if(null != priority && !priority.isEmpty()) {
						
						ticketCountMap.put(priority, count);
					}
				} 
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket Priority and Count", e);
			throw new RestfulException("Unable to get ticket Priority and Count.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		if(ticketCountMap != null){
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTicketNumbersByPriorityOrType(ticketCountMap);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	
	
	private boolean valHistoryReq(TkmTicketsRequest tkmRequest) {
		boolean bStatus = false;
		
		if(tkmRequest != null && tkmRequest.getTicketId() !=null) {
			bStatus = true;
		}
		
		return bStatus;
	}
	
	
	/**
	 * 
	 * @author kaul_s
	 * @param TkmTicketsRequest
	 * @return
	 * This REST method will return all the merged form properties between the existing and new work flow for reclassify 
	 */
	
	@RequestMapping(value = "/fetchTargetTicketFormProerties", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTargetTicketFormProerties(@RequestBody TkmTicketsRequest tkmTicketsRequest)
	{
		LOGGER.info("======================== in getTaskFormProperties() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicektsResponse = new TkmTicketsResponse();
		try {
			
			TkmWorkflows  tkmWorkflows = tkmWorkflowsService.getTicketWorkflowsByCriteria(tkmTicketsRequest.getType(), tkmTicketsRequest.getCategory());
			
			List<TaskFormProperties> newFormPropList = tkmActivitiService.getWorlFlowsMergedProperties(tkmWorkflows.getFilename(),tkmTicketsRequest.getTicketId());
			
			tkmTicektsResponse.setTkmTicketFomProperties(newFormPropList);
			tkmTicektsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			
			
		} catch (Exception e) {
			LOGGER.error("Unable to task data from activiti", e);
			throw new RestfulException("Unable to task data from activiti.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.TSK_PROP.code),  e);
		}
		
				
		return new ResponseEntity<String>(xstream.toXML(tkmTicektsResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param TkmTicketsRequest
	 * @return
	 * This REST method will reclassify the ticket to a new work flow
	 */
	@RequestMapping(value = "/performTicketWorkflowReclassify", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> performTicketWorkflowReclassify(@RequestBody TkmTicketsRequest  tkmTicketsRequest)
	{
		LOGGER.info("======================== in performTicketWorkflowReclassify() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
		
		TkmTickets ticket = null;
		try {
			// cancel activity and
			// ticketasks for
			// the reclassified
			// ticket without canceling ticket
			
			tkmMgmtService.cancelTicketAndTasks(
					tkmTicketsRequest.getId(),
					tkmTicketsRequest.getLastUpdatedBy(), 
					tkmTicketsRequest.getComment(), true);
			
			//initialize new workflow for the selected category and type.
			ticket = tkmMgmtService.reClassifyTicketWorkFlow(
					tkmTicketsRequest.getId(), tkmTicketsRequest.getCategory(),
					tkmTicketsRequest.getType(),
					tkmTicketsRequest.getTicketFormPropertiesMap(),
					tkmTicketsRequest.getLastUpdatedBy());
		} catch (Exception e) {
			LOGGER.error("Error occured while creating a ticket", e);
			throw new RestfulException("Error occured while creating a ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.CREATE.code),  e);
		}
		
		if(ticket!=null){
			/*
			 * Ticket object is not being used further anywhere. But it creates issues in json parsing beucase of lazy loading of AccountUser object 
			 * hence commented the setter object in response.
			 * */
			//ticketResponse.setTkmTickets(ticket);
			ticketResponse.setErrMsg("");
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		
		if(StringUtils.isEmpty(ticketResponse.getErrMsg()) &&
			!ticket.getTkmWorkflows().getType().equalsIgnoreCase(APPEAL_REQUEST_TICKET)) {
			try {
				tkmMgmtService.sendTicketCreateEmail(tkmTicketsRequest.getId());
			} catch (Exception e) {
				LOGGER.info("Unable to send email for the ticket creation: " , e);
				throw new RestfulException("Unable to send email for the ticket creation.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SEND_EMAIL.code),  e);
			}
		}
		
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}	
	
	
	@RequestMapping(value = "/getAllUsersForQueue", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getAllUsersForQueue(@RequestBody String queueName)
	{
		LOGGER.info("======================== in getAllUsersForQueue() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			List<Object[]> userList = queueUsersService.getAccountUsersWithRoleByQueueName(queueName);
			
		    tkmTicketsResponse.setTkmQueueUserDtoList(userList);
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			LOGGER.error("Unable to get Users for the Queue ", e);
			throw new RestfulException("Unable to get Users for the Queue.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SUBJECT.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllUsersForQueueForReassign", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getAllUsersForQueueForReassign(@RequestBody String queueName)
	{
		LOGGER.info("======================== in getAllUsersForQueue() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			List<Object[]> userList = queueUsersService.getAccountUsersForReassign(queueName);
			
		    tkmTicketsResponse.setTkmQueueUserDtoList(userList);
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			LOGGER.error("Unable to get Users for the Queue ", e);
			throw new RestfulException("Unable to get Users for the Queue.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SUBJECT.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getQueuesForUser", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getQueuesForUser(@RequestBody String userId)
	{
		LOGGER.info("======================== in getAllUsersForQueue() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			List<Integer> queuesIdList = queueUsersService.getUserQueueId(Integer.valueOf(userId));
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setUserQueues(queuesIdList);
		} catch (Exception e) {
			LOGGER.error("Unable to get Users for the Queue ", e);
			throw new RestfulException("Unable to get Users for the Queue.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SUBJECT.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getDetailsForQuickActionBar", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getDetailsForQuickActionBar(@RequestBody Integer ticketId)
	{
		LOGGER.info("======================== in getDetailsForQuickActionBar() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			TkmQuickActionDto actionBarDetails = tkmTicketTaskService.getDetailsForQuickActionBar(ticketId);
	
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmQuickActionDto(actionBarDetails);
		} catch (Exception e) {
			LOGGER.error("Unable to get Users for the Queue ", e);
			throw new RestfulException("Unable to get Users for the Queue.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.USER_TKTLIST.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	/**
	 * This method will return the pending tickets count to be displayed in the report
	 * filtered by the search criteria 
	 * 
	 * @param searchCriteria
	 * @return
	 */
	@RequestMapping(value = "/getPendingTicketDetails", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getPendingTicketDetails(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("======================== in getPendingTicketDetails() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			Map<String, Object> pendingTicketsData = tkmReportService.getPendingTicketDetails(searchCriteria);
	
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTicketsMap(pendingTicketsData);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket details for pending report ", e);
			throw new RestfulException("Unable to get ticket details for pending report.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SEARCH.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getHouseholdNamesForCreatedFor", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getHouseholdNamesForCreatedFor(@RequestBody Map<String, String> searchCriteria)
	{
		LOGGER.info("======================== in getHouseholdNamesForCreatedFor() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			String roleId = searchCriteria.get("roleId");
			String moduleName = searchCriteria.get("moduleName");
			String userText = searchCriteria.get("userText");

			String createdFor = StringUtils.EMPTY;
			if("INDIVIDUAL".equalsIgnoreCase(moduleName)) {
				createdFor = tkmMgmtService.getCreatedForIndv(userText);
			} else {
				createdFor = tkmMgmtService.getCreatedForUser(roleId, userText);
			}
	
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setCreatedForName(createdFor);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket details for pending report ", e);
			throw new RestfulException("Unable to get ticket details for pending report.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SEARCH.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getHouseholdByuserId", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getHouseholdByuserId(@RequestBody int houseHoldUserId)
	{
		LOGGER.info("======================== in getHouseholdByuserId() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			
			
			List<Household> householdList = tkmMgmtService.getHouseholdByuserId(houseHoldUserId);
	
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setHouseholdList(householdList);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket details for pending report ", e);
			throw new RestfulException("Unable to get household by user id.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SEARCH.code),  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/archive", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> archiveTicket(@RequestBody TkmTicketsRequest ticketRequest)
	{
		LOGGER.info("======================== in cancelticket() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
	 	ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		try {
            tkmMgmtService.archiveTicket(ticketRequest.getId(), null);
             
		} catch (Exception e) {
			LOGGER.error("Unable to cancel the ticket", e);
			throw new RestfulException("Unable to archive the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.CANCEL.code),  e);
		}
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}
	
	/**
	 * @author hardas_d
	 * REST call to reopen the ticket.
	 * The ticket status will be changed to Reopened, Assignee will be removed and default workflow will be initialized.
	 * @param tkmReq
	 * @return
	 */
	@RequestMapping(value = "/reopen", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> reopenTicket(@RequestBody TkmTicketsRequest tkmReq)
	{
		LOGGER.info("======================== in reopen() =======================");		
		TkmTicketsResponse tkmResp = new TkmTicketsResponse();
	 	tkmResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmResp.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		
		if(tkmReqValidator.validReopenRequest(tkmReq)) {
			try {
	            tkmMgmtService.reopenTicket(tkmReq.getTicketId(), tkmReq.getLastUpdatedBy());
	            tkmResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
	    		tkmResp.setErrMsg(StringUtils.EMPTY);  
	    		
			} 
			catch (Exception e) {
				LOGGER.error("Unable to reopen the ticket", e);
				throw new RestfulException("Unable to reopen the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.REOPEN.code),  e);
			}
		}
		
		return new ResponseEntity<String>(TkmUtil.getJsonForTkmTicketsResponse(tkmResp,"reopenTicket"),HttpStatus.OK);
	}
	
	/**
	 * @author hardas_d
	 * REST call to restart the ticket
	 * The ticket status will be changed to Restarted, Assignee will be removed and workflow will be re-initialized.
	 * @param tkmReq
	 * @return
	 */
	@RequestMapping(value = "/restart", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> restartTicket(@RequestBody TkmTicketsRequest tkmReq)
	{
		LOGGER.info("======================== in restart() =======================");
		TkmTicketsResponse tkmResp = new TkmTicketsResponse();
	 	tkmResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		tkmResp.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		if(tkmReqValidator.validRestartRequest(tkmReq)) {
			try {
	            tkmMgmtService.restartTicket(tkmReq.getTicketId(), tkmReq.getLastUpdatedBy());
	            tkmResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
	    		tkmResp.setErrMsg(StringUtils.EMPTY);
	    		
			} catch (Exception e) {
				LOGGER.error("Unable to restart the ticket", e);
				throw new RestfulException("Unable to restart the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.RESTART.code),  e);
			}
		}
		return new ResponseEntity<String>(TkmUtil.getJsonForTkmTicketsResponse(tkmResp,"restart"),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> listTickets(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("======================== in searchTickets() =======================");
		LOGGER.info("Received Serach Criteria: " + searchCriteria);
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		try {
			Map<String, Object> tkmTicketsAndRecordCount = ticketListService.searchTicketsWithNativeQuery(searchCriteria);
			
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmTicketsResponse.setTkmTicketsMap(tkmTicketsAndRecordCount);
		
		} catch (Exception e) {
			LOGGER.error("Error while searching ticket", e);
			throw new RestfulException("Unable to archive the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SEARCH.code),  e);
		}
		return new ResponseEntity<String>(TkmUtil.getJsonForTkmTicketsResponse(tkmTicketsResponse,"list"),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param ticket id
	 * @return
	 * This REST method will reassign the active task.
	 */
	@RequestMapping(value = "/assign", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> assignTicket(@RequestBody TkmTicketsTaskRequest  tkmTicketsTaskRequest)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse taskResponse = new TkmTicketTaskResponse();
		boolean reassignSuccessful = false;
		
		try {
			reassignSuccessful = tkmMgmtService.reassignTkkActiveTask(tkmTicketsTaskRequest.getTaskId(), tkmTicketsTaskRequest.getGroupId(), tkmTicketsTaskRequest.getUserId(),tkmTicketsTaskRequest.getTaskTicketId());
			
			boolean bStat = false;
			TkmTicketTasks ticketTasks = tkmTicketTaskService.findByTaskId(tkmTicketsTaskRequest.getTaskId());

			TkmTickets ticket = ticketListService.findTkmTicketsById(Integer.parseInt(tkmTicketsTaskRequest.getTaskTicketId()));
			if (ticket == null) {
				bStat = false;
				LOGGER.error("Unable to get the ticket data");
				throw new RestfulException("Unable to get the ticket data.",HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				// Get the ticket status, required to send mail if status 'Unclaimed'
				String oldStatus = ticket.getStatus().toString();
				
				// Claim the ticket
				bStat = tkmMgmtService.claimTicketTask(ticketTasks.getTaskId(), ticketTasks.getActivitiTaskId(), tkmTicketsTaskRequest.getUserId());

				if (bStat && // Ticket claimed
					((TkmTickets.ticket_status.UnClaimed.toString().compareToIgnoreCase(oldStatus) == 0) ||
					 (TkmTickets.ticket_status.Restarted.toString().compareToIgnoreCase(oldStatus) == 0) ||
					 (TkmTickets.ticket_status.Reopened.toString().compareToIgnoreCase(oldStatus) == 0)
					)) {

					// update the ticket status
					ticket = ticketListService.updateStatus(ticketTasks.getTicket().getId(),
							TkmTickets.ticket_status.Open.toString(), tkmTicketsTaskRequest.getUserId(), null);

					bStat = (ticket != null);
					String emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
					boolean emailStat = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Open.toString(),emailConfigValue);
					// Send an email for the status changed to 'Open' if it is not an Appeal Request
					if (bStat && emailStat) {
						tkmMgmtService.sendTicketOpenEmail(ticket.getId());
					}
				}
			}
			
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		
			
			
		} catch (Exception e) {
			LOGGER.error("Unable to reassign the ticket", e);
			throw new RestfulException("Unable to reassign the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.REASSIGN.code),  e);
		}
		
		if(reassignSuccessful){
			taskResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			taskResponse.setActivitiProcessCompleted(true);
		}
	
		return new ResponseEntity<String>(xstream.toXML(taskResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addqueuestocsruser", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> addQueuesToCsrUser(@RequestBody TkmQueueUsersRequest  queueUserRequest) {
		
		TkmQueueUsersResponse queueUserResp = new TkmQueueUsersResponse();
		queueUserResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		queueUserResp.setErrMsg(ERR_MSG_PARAM_MISSING);
				
		try {
			if(tkmReqValidator.validAddQueuesToCsrUser(queueUserRequest)) {
				tkmMgmtService.addQueuesToUser(queueUserRequest.getUserId(), queueUserRequest.getQueueIdList());
				queueUserResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
				queueUserResp.setErrMsg("");
			}
		} catch (Exception e) {
			LOGGER.error("Unable to the add queues to the user", e);
			throw new RestfulException("Unable to the add queues to the user.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SAVE_QUEUES_TO_USER.code),  e);
		}
		return new ResponseEntity<String>(TkmUtil.getJsonForTkmQueueUsersResponse(queueUserResp,"addqueuestocsruser"),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updatedefaultqueues", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> updateDefaultQueues(@RequestBody String defaultQueueIds)
	{
		LOGGER.info("Updating Default Ticket Workgroups");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		
		try {
			tkmQueuesService.updateDefaultQueues(defaultQueueIds);
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			LOGGER.error("Unable to update default ticket workgroups : ", e);
			throw new RestfulException("Unable to update default ticket workgroups.",HttpStatus.INTERNAL_SERVER_ERROR,  e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	
	/**
	 * 
	 * @author kaul_s
	 * @param cancelticketRequest
	 * @return
	 * This REST method will cancel the ticket.
	 */
	@RequestMapping(value = "/tickets/ticket/cancel", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> cancelTickets(@RequestBody CancelTicketRequest ticketRequest)
	{
		LOGGER.info("======================== in cancelTickets() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
	 	ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		boolean bStatus = false;
		String emailConfigValue = null;
		String subModuleName = "";
		if(null !=ticketRequest.getSubModuleName()){
			subModuleName = 	ticketRequest.getSubModuleName().toString();
		}
		try {
			if (null != ticketRequest && ticketRequest.getTicketId() > 0) {

				cancelTicketAndTasks(ticketRequest, emailConfigValue);
			}
			else if(null != ticketRequest && null!=ticketRequest.getSubModuleIds() && ticketRequest.getSubModuleIds().size()>0){
				for (Integer subModuleId : ticketRequest.getSubModuleIds()) {
					TkmTickets ticket = null; 
							if(subModuleName.length() > 0){
								ticket = ticketListService.findTicketWithEventIdAndName(subModuleId,subModuleName);	
							}
							else{
								ticket = ticketListService.findTicketWithEventId(subModuleId);	
							}
							
					if (ticket != null) {
						emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();

						bStatus = tkmMgmtService.cancelTicketAndTasks(ticket.getId(),
								ticketRequest.getLastUpdatedBy(), ticketRequest.getComment(), false);
						tkmMgmtService.logTicketEvents(ticket.getId(), TICKET_CANCELLED);
						if (bStatus) {
							boolean emailAllowed = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Cancel.toString(),
									emailConfigValue);
							if(emailAllowed){
								tkmMgmtService.sendTicketCancelledEmail(ticket.getId());
							}
						}
					}
					
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to cancel the ticket", e);
			throw new RestfulException("Unable to cancel the ticket.",HttpStatus.INTERNAL_SERVER_ERROR ,String.valueOf(error_code.SAVE_QUEUES_TO_USER.code), e);
		}

		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
	}

	private void cancelTicketAndTasks(CancelTicketRequest ticketRequest, String emailConfigValue) throws TKMException {
		boolean bStatus;
		TkmTickets ticket = ticketListService.findTkmTicketsById(ticketRequest.getTicketId());
		if (ticket != null) {
			emailConfigValue = ticket.getTkmWorkflows().getEmailConfig();
		}

		bStatus = tkmMgmtService.cancelTicketAndTasks(ticketRequest.getTicketId(),
				ticketRequest.getLastUpdatedBy(), ticketRequest.getComment(), false);
		tkmMgmtService.logTicketEvents(ticketRequest.getTicketId(), TICKET_CANCELLED);
		if (bStatus) {
			boolean emailAllowed = tkmMgmtService.sendEmailByTicketType(TkmTicketsRequest.Email_Config.Cancel.toString(),
					emailConfigValue);
			if(emailAllowed){
				try {
					tkmMgmtService.sendTicketCancelledEmail(ticket.getId());
				} catch (NotificationTypeNotFound e) {
					LOGGER.error("Unable to send ticket cancel email", e);
				}
			}
		}
	}
	
	/**
	 * 
	 * @author satyaka_k
	 * @param ticketRequest
	 * @return
	 * This REST method will return the ticket list based on the search criteria.
	 */
	@RequestMapping(value = "/tickets/consumer", method = RequestMethod.POST)
	@ApiOperation(value="Search Tickets", notes="It takes Map of different criteria as input and returns Map with Ticket List and Record Count")
	@ResponseBody public ResponseEntity<String> getConsumerTicketList(@RequestBody TkmTicketsRequest ticketRequest)
	{
		LOGGER.info("======================== in getticketlist() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse ticketResponse = new TkmTicketsResponse();
		ticketResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		//Check the mandatory variables
		if(ticketRequest == null) {
			ticketResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
			return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);
		}
		try {
			List<TkmTickets> ticketList = tkmMgmtService.searchTicketsByCriteria(ticketRequest);
			ticketResponse.setTkmTicketsList(mapConsumerTicketList(ticketList));
			
			ticketResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			ticketResponse.setErrMsg("");
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket list as per search criteria", e);
			ticketResponse.setErrMsg(e.getStackTrace().toString());
			throw new RestfulException("Unable to get ticket list as per search criteria.",HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return new ResponseEntity<String>(xstream.toXML(ticketResponse),HttpStatus.OK);	
		
	}

	private List<TkmTickets> mapConsumerTicketList(List<TkmTickets> ticketList) {

		List<TkmTickets> consumerTicketList = new ArrayList<TkmTickets>();
		if (null != ticketList && ticketList.size() > 0) {
			for (TkmTickets tkmTickets : ticketList) {
				TkmTickets tkmTicket = new TkmTickets();
				tkmTicket.setId(tkmTickets.getId());
				tkmTicket.setSubject(tkmTickets.getSubject());
				tkmTicket.setStatus(tkmTickets.getStatus());
				tkmTicket.setCreated(tkmTickets.getCreated());
				tkmTicket.setNumber(tkmTickets.getNumber());
				tkmTicket.setUpdated(tkmTickets.getUpdated());
				tkmTicket.setTkmWorkflows(tkmTickets.getTkmWorkflows());
				consumerTicketList.add(tkmTicket);
			}
		}
		return consumerTicketList;
	}
	
	private boolean validateTicketDescription(TkmTicketsRequest ticketRequest) {
		String desc = StringUtils.defaultString(StringUtils.isEmpty(ticketRequest.getDescription()) ? ticketRequest.getComment() : ticketRequest.getDescription());
		if(desc.length() == 0 || desc.length() > 2000){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	@RequestMapping(value = "/saveworkflow", method= RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveWorkflow(
			@RequestBody TkmWorkflowsDto tkmWorkflowsRequest) {
		LOGGER.info("======================== in saveworkflows() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflows tkmWorkflowsResponse = new TkmWorkflows();
		try {
				String errors = validateTkmWorkflowsDto(tkmWorkflowsRequest);
				if(!errors.isEmpty()) {
					return new ResponseEntity<String>("Following errors occured while saving record:"+errors ,HttpStatus.OK);
				}
				tkmWorkflowsResponse = tkmWorkflowsService.createTicketType(tkmWorkflowsRequest);
		} catch (Exception e) {
			LOGGER.error("Unable to save workflows.", e);
			throw new RestfulException("Unable to save workflows.",HttpStatus.PRECONDITION_FAILED , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savequeue", method= RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveQueue(
			@RequestBody TkmQueues tkmQueuesRequest) {
		LOGGER.info("======================== in savequeue() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmQueues tkmQueuesResponse = new TkmQueues();
		try {
				String errors = validateTkmQueues(tkmQueuesRequest);
				if(!errors.isEmpty()) {
					return new ResponseEntity<String>("Following errors occured while saving record:"+errors ,HttpStatus.OK);
				}
				tkmQueuesResponse = tkmQueuesService.createQueue(tkmQueuesRequest);
		} catch (Exception e) {
			LOGGER.error("Unable to save workflows.", e);
			throw new RestfulException("Unable to save workflows.",HttpStatus.PRECONDITION_FAILED , e);
		}
		return new ResponseEntity<String>(xstream.toXML(tkmQueuesResponse),HttpStatus.OK);
	}
	
	private String validateTkmWorkflowsDto(TkmWorkflowsDto dto) {
		List<String> errResp = new ArrayList<String>();
		if(null == dto.getId()) {
		/*	if(StringUtils.isEmpty(dto.getCategory())) {
				errResp.add("Value for Category is empty");
			}
			if(StringUtils.isEmpty(dto.getPermission())) {
				errResp.add("Value for Permission is empty");
			}
			if(null == dto.getExpDate()) {
				errResp.add("Value for Expiry Days is empty");
			}
			if(null == dto.getDefaultPriority()) {
				errResp.add("Value for Default Priority is empty");
			}*/
			if(!StringUtils.isEmpty(dto.getCategory()) && !StringUtils.isEmpty(dto.getType()) && !StringUtils.isEmpty(dto.getFilename()) && 
					iTkmWorkflowsRepository.findByCriteria(dto.getType().toLowerCase(), dto.getCategory().toLowerCase(), dto.getFilename())!=null) {
				errResp.add("Duplicate record already exists for values(Category, Type, Workflow File Name)");
			}
			return errResp.stream().collect(Collectors.joining(","));
		}
		return "";
	}
	
	private String validateTkmQueues(TkmQueues tkmQueuesRequest) {
		List<String> errResp = new ArrayList<String>();
		if(null == tkmQueuesRequest.getId()) {
			if(StringUtils.isEmpty(tkmQueuesRequest.getName())) {
				errResp.add("Value for Name is empty");
			}
			if(StringUtils.isEmpty(tkmQueuesRequest.getIsDefault())) {
				errResp.add("Value for isDefault is empty");
			}
			return errResp.stream().collect(Collectors.joining(","));
		}
		return "";
	}
	

	@RequestMapping(value = "/getAllUsersNotInQueue", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getAllUsersNotInQueue(@RequestBody Map<String, String> paramMap)
	{
			LOGGER.debug("======================== in getAllUsersNotInQueue() =======================");

		String queueName = paramMap.get("queueName");
		String roleName = null;
		if(paramMap.containsKey("roleName")) {
			roleName = paramMap.get("roleName");
		}
		String addFlowFlag = paramMap.get("isAddFlow");

		boolean isAddFlow = false;
		if(StringUtils.isNotBlank(addFlowFlag)) {
			isAddFlow = BooleanUtils.toBoolean(addFlowFlag);
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("======================== parameters " + queueName + ", " + roleName+", "+isAddFlow);
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();

		try {
			List<Object[]> userList = queueUsersService.getAccountUsersNotInQueue(queueName, roleName, isAddFlow);

			tkmTicketsResponse.setTkmQueueUserDtoList(userList);
			tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			LOGGER.error("Unable to get Users not in the Queue ", e);
			throw new RestfulException("Unable to get Users not in the Queue.", HttpStatus.INTERNAL_SERVER_ERROR,
					String.valueOf(error_code.SUBJECT.code), e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);
	}
	

	@RequestMapping(value = "/getcategories", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getDistinctCategories( @RequestBody List<String>filteredPermissions)
	{
		LOGGER.info("======================== in getDistinctCategories() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();
		try {
			List<String> ticketCategories = tkmWorkflowsService.getTicketCategories(filteredPermissions);

			if(null != ticketCategories) {
				LOGGER.info("======================== in getDistinctCategories() ======================="+ticketCategories.size());
			}
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setTicketCategories(ticketCategories);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket categories", e);
			throw new RestfulException("Unable to get distinct ticket categories.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/getworkflowbycategory", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<TkmWorkflows>> getWorkflowsByCategory(@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in getWorkflowsByCategory() =======================");
		List<TkmWorkflows> ticketTypeList =null;
		try {
			if(null!=tkmTicketsRequest) {
			ticketTypeList = tkmWorkflowsService.getWorkflowsByCategory(tkmTicketsRequest.getCategory(),tkmTicketsRequest.getFilteredPermissions());
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get workflow list", e);
			throw new RestfulException("Unable to get workflow list for category.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		return new ResponseEntity<>( ticketTypeList,HttpStatus.OK);
	}

	@RequestMapping(value = "/getworkgroups", method = RequestMethod.GET)
	@ResponseBody 
	public Map<Integer,String> getTicketWorkgroups()
	{
		LOGGER.info("======================== in getTicketWorkgroups() =======================");
		Map<Integer,String> ticketWorkgroups = null;
		try {
			ticketWorkgroups = tkmQueuesService.getTicketWorkgroups();

			if(null != ticketWorkgroups) {
				LOGGER.info("======================== in getTicketWorkgroups() ======================="+ticketWorkgroups.size());
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket categories", e);
			throw new RestfulException("Unable to get distinct ticket workgroups.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return ticketWorkgroups;
	}
	
	
	@RequestMapping(value = "/getWorkflowTypes", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getWorkflowTypes()
	{
		LOGGER.info("======================== in getWorkflowTypes() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();
		try {
			List<String> workflowTypes = tkmWorkflowsService.getWorkflowTypes();

			if(null != workflowTypes) {
				LOGGER.info("======================== in getWorkflowTypes() ======================="+workflowTypes.size());
			}
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setWorkflowTypes(workflowTypes);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket WorkflowTypes", e);
			throw new RestfulException("Unable to get distinct WorkflowTypes.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getPermissionNames", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getPermissionNames(@RequestBody List<String>filteredPermissions)
	{
		LOGGER.info("======================== in getPermissionNames() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();
		try {
			List<String> permissionNames = tkmWorkflowsService.getPermissionNames(filteredPermissions);

			if(null != permissionNames) {
				LOGGER.info("======================== in getPermissionNames() ======================="+permissionNames.size());
			}
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setWorkflowTypes(permissionNames);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket PermissionNames", e);
			throw new RestfulException("Unable to get distinct PermissionNames.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}

	@RequestMapping(value = "/getTicketTypes", method = RequestMethod.POST)
	@ResponseBody public ResponseEntity<String> getTicketTypes()
	{
		LOGGER.info("======================== in getTicketTypes() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse tkmWorkflowsResponse = new TkmWorkflowsResponse();
		try {
			List<String> ticketTypes = tkmWorkflowsService.getTicketTypes();

			if(null != ticketTypes) {
				LOGGER.info("======================== in getTicketTypes() ======================="+ticketTypes.size());
			}
			tkmWorkflowsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			tkmWorkflowsResponse.setTicketType(ticketTypes);
		} catch (Exception e) {
			LOGGER.error("Unable to get ticket ticketTypes", e);
			throw new RestfulException("Unable to get distinct ticketTypes.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}
		
		return new ResponseEntity<String>(xstream.toXML(tkmWorkflowsResponse),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateTkmCategory", method= RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateTkmCategoryName(
			@RequestBody TkmTicketsRequest tkmTicketsRequest) {
		LOGGER.info("======================== in updateTkmCategoryName() =======================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = new TkmTicketsResponse();
		String category = tkmTicketsRequest.getCategory();
		String old_category = tkmTicketsRequest.getOld_category();

		// save comments
		try {
			tkmWorkflowsService.updateTkmCategoryName(category,old_category);
				tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				tkmTicketsResponse.setErrMsg("");
		} catch (TKMException e) {
			LOGGER.error("Unable to save category.", e);
			throw new RestfulException("Unable to save category.",HttpStatus.PRECONDITION_FAILED , e);
		}

		return new ResponseEntity<String>(xstream.toXML(tkmTicketsResponse),HttpStatus.OK);

	}
	
	@RequestMapping(value = "/getWorkflowByName", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> isQueueExists(@RequestBody String name) {
		LOGGER.debug("======================== in isQueueExists() =======================");
		TkmQueues queue = null;
		try {
			queue = this.tkmQueuesService.findByName(name);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("in isQueueExists() queue is "+queue);
			}

		} catch (Exception e) {
			LOGGER.error("Unable to get workflow list", e);
			throw new RestfulException("Unable to get workflow for name.",HttpStatus.INTERNAL_SERVER_ERROR , e);
		}

		if(null != queue) {
			return new ResponseEntity<String>(platformGson.toJson(queue),HttpStatus.OK);			
		} else {
			return null;
		}
	}


}

