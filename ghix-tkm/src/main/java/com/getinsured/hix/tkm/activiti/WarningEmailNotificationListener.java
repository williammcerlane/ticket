package com.getinsured.hix.tkm.activiti;

import java.util.logging.Logger;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmNotificationService;
 
 
public class WarningEmailNotificationListener implements JavaDelegate {
 
	/**
	 * 
	 */
	private static Logger log = Logger.getLogger(WarningEmailNotificationListener.class.getName());
 
	@SuppressWarnings("unused")
	private TkmNotificationService tkmNotificationService;
	
	@SuppressWarnings("unused")
	private TicketListService ticketListService;
	
	public void setTicketListService(TicketListService ticketListService) {
		this.ticketListService = ticketListService;
	}
	
	public void setTkmNotificationService(TkmNotificationService tkmNotificationService) {
		this.tkmNotificationService = tkmNotificationService;
	}
	
	@Override
	public void execute(DelegateExecution execution) {
		/*TaskService taskService;*/
		/*try{*/
		log.info("Inside WarningEmailNotificationListener code commented as per HIX-63524");
	/*	taskService=execution.getEngineServices().getTaskService();
		List<Task> activeTaskList  = taskService.createTaskQuery().processInstanceId(execution.getProcessInstanceId())
				.list();
		TkmTickets tkmTickets = ticketListService.getTkmTicketsByActivitiTaskId(activeTaskList.get(0).getId()).get(0);
		String inputDate;
		String subject = "";
		
		if(tkmTickets.getTkmWorkflows().getType().equals("Expedited Appeals"))
		{
			inputDate = "1";
		}else if(tkmTickets.getTkmWorkflows().getType().equals("Individual Appeal"))
		{
		    inputDate = "7"	;
		}
		else if(tkmTickets.getTkmWorkflows().getCategory().equals("Document Verification"))
		{
		    inputDate = "3"	;
		}
		else
		{
			inputDate = "5";
		}
		//both user name and group name are null
		for (Task activeTasks : activeTaskList) {
			StringBuilder message = new StringBuilder("");
				if (tkmTickets.getStatus().equals("Open")) {
					subject = "You have a "+ tkmTickets.getTkmWorkflows().getType()+" ticket due in "+inputDate+" days";
					message.append("<br> You have a ticket that is due in "+inputDate+" days. Please make sure to complete work on the ticket by "+tkmTickets.getDueDate()+"<br/><br/>");
				} else if (tkmTickets.getStatus().equals("UnClaimed")) {
					subject = "Unclaimed "
							+ tkmTickets.getTkmWorkflows().getType() +" ticket is due in "+inputDate+" days" ;
					message.append("<br> An unclaimed ticket assigned to your work group is due in "+inputDate+" days. Please claim the ticket and complete work by "+tkmTickets.getDueDate()+".<br/><br/>");
				}
			message.append("Ticket Type: "+tkmTickets.getTkmWorkflows().getType()+"<br>");
			message.append("Due Date: "+tkmTickets.getDueDate()+"<br>");
			message.append("Priority: "+activeTasks.getPriority()+"<br>");
			message.append("</a>");
			String assignee = activeTasks.getAssignee();
			String owner = null;
			if(assignee == null) {
				//ASSUMPTION: Owner should always be a group (only one) in BPM file 
				List<IdentityLink> links = taskService.getIdentityLinksForTask(activeTasks.getId());
				for (IdentityLink identityLink : links) {
					List<Group> group = Context.getProcessEngineConfiguration().getIdentityService().createGroupQuery().groupName(identityLink.getGroupId()).list();
					owner = group.get(0).getName();
					
					if(owner != null) {
						break;
					}
				}
				if(owner!=null){
					List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().memberOfGroup(owner).list();
					for (User user : users) {
						Map<String,String> emailTokens = new HashMap<String, String>();
						emailTokens.put("recipient", user.getEmail());
						emailTokens.put("subject", subject);
						emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
						emailTokens.put("msgText",message.toString() );
						
						tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
					}
					
					break;
				}
			}	
			else{
				List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().userId(assignee).list();
				User user =users.get(0);
				Map<String,String> emailTokens = new HashMap<String, String>();
				emailTokens.put("recipient", user.getEmail());
				emailTokens.put("subject", subject);
				emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
				emailTokens.put("msgText",message.toString() );
				
				tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
			}
		}
		}
		catch(Exception e){
			log.info("Exception in notification listener: "+ e);
		}*/
 
	}
}
