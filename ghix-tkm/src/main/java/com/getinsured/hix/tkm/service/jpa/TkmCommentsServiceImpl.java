/**
 * 
 */
package com.getinsured.hix.tkm.service.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.TkmComments;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmCommentsRepository;
import com.getinsured.hix.tkm.service.TkmCommentsService;

/**
 * @author hardas_d
 *
 */
@Service("TkmCommentsService")
public class TkmCommentsServiceImpl implements TkmCommentsService {

	@Autowired private ITkmCommentsRepository iTkmCommentsRepository;
	@Autowired private ITicketRepository iTicketRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmCommentsServiceImpl.class);
	
	@Override
	@Transactional
	public boolean saveComments(Integer ticketId, CommentTarget commentTarget) {
		boolean bStatus = false;
		if(!iTicketRepository.exists(ticketId)) {
			return bStatus;
		}	
		
		TkmTickets tickets = new TkmTickets();
		tickets.setId(ticketId);
		
		TkmComments tkmComments = new TkmComments();
		tkmComments.setTicket(tickets);
		tkmComments.setCommentTarget(commentTarget);
		
		try {
			TkmComments savedComment = iTkmCommentsRepository.save(tkmComments);
			bStatus = (savedComment != null) ? true : bStatus;
		} catch (Exception e) {
			LOGGER.error("Unable to save the comments",e);
		}
		
		return bStatus;
	}


	@Override
	@Transactional(readOnly=true)
	public CommentTarget getCommentTargetByTicketId(Integer ticketId) {
		CommentTarget commentTarget = null;
		TkmComments tkmComments = iTkmCommentsRepository.findByTicketId(ticketId);		
		
		if(tkmComments != null) {
			commentTarget = tkmComments.getCommentTarget();
		}			
		return commentTarget;
	}

}
