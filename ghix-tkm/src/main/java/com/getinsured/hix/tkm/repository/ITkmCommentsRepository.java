package com.getinsured.hix.tkm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.TkmComments;

public interface ITkmCommentsRepository extends JpaRepository<TkmComments, Integer>{

	@Query("SELECT t FROM TkmComments t WHERE t.ticket.id= :ticketId")
	TkmComments findByTicketId(@Param("ticketId") int ticketId);

}
