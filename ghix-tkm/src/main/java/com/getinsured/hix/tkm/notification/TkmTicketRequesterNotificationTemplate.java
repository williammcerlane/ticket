package com.getinsured.hix.tkm.notification;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Helper class to send Notification email in ticket Management flow to ticket requester.
 *
 * @author Dheeraj Maralkar
 *
 */
@Component
@Scope("prototype")
public class TkmTicketRequesterNotificationTemplate extends TkmTicketNotificationTemplate { 
}