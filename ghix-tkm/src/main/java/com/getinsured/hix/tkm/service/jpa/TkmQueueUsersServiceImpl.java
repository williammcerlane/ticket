/**
 * 
 */
package com.getinsured.hix.tkm.service.jpa;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmQueueUser;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.tkm.repository.ITkmQueueUsersRepository;
import com.getinsured.hix.tkm.repository.ITkmQueuesRepository;
import com.getinsured.hix.tkm.service.TkmQueueUsersService;

/**
 * @author hardas_d
 *
 */
@Service("tkmQueueUsersService")
public class TkmQueueUsersServiceImpl implements TkmQueueUsersService {

	@Autowired private ITkmQueueUsersRepository iTkmQueueUsersRepository;
	@Autowired private ITkmQueuesRepository iTkmQueuesRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmQueueUsersServiceImpl.class);
	
	/**
	 * @author hardas_d
	 * @param queueId
	 * @param userList
	 * @return Status of the transaction done on the data
	 * @throws Exception 
	 */
	@Override
	public boolean updateQueueUsers(Integer queueId, List<Integer> userList, boolean addFlag) {
		boolean bStatus = false;
		List<Integer> newUserList = userList;

		if (!iTkmQueuesRepository.exists(queueId)) {
			LOGGER.error("Invalid tkm queue id, not available in the DB: " + queueId);
			return bStatus;
		}


		if (!addFlag) {
			bStatus = deleteUsersFromQueue(queueId, userList);
		} else if (addFlag) {
			bStatus = addUsersToQueue(queueId, bStatus, userList);
		}


      return bStatus;
	}

	private boolean addUsersToQueue(Integer queueId, boolean bStatus, List<Integer> newUserList) {
		// new users to be added
		if (!newUserList.isEmpty()) {
			bStatus = addNewUsersToQueue(queueId, newUserList);
		}
		return bStatus;
	}

	/**
	 * @author hardas_d
	 * @return boolean
	 * @param queueId
	 * @param userList
	 * @return
	 */
	@Transactional
	private boolean deleteUsersFromQueue(Integer queueId,	List<Integer> userList) {
		boolean bStatus = false;
		
		try {
			iTkmQueueUsersRepository.deleteByGroupAndUsers(queueId, userList);
			bStatus = true;
			LOGGER.info("Users has been removed. count: " + userList.size());
		} catch (Exception e) {
			LOGGER.error("Unable to delete the users ", e);
		}
		
		return bStatus;
	}

	/**
	 * @author hardas_d
	 * @return boolean
	 * @param queueId
	 * @param userList
	 * @return Adds new users to the queue
	 */
	private boolean addNewUsersToQueue(Integer queueId,	List<Integer> userList) {
		List<TkmQueueUser> addUsers = new ArrayList<TkmQueueUser>();
		for (Integer userId : userList) {
			TkmQueueUser queueUser = new TkmQueueUser();
			queueUser.setGroupId(queueId);
			queueUser.setUserId(userId);
			
			addUsers.add(queueUser);
		}
		
		try {
			List<TkmQueueUser> addedUsers = iTkmQueueUsersRepository.save(addUsers);
			return (addedUsers.size() == addUsers.size());
		} catch (Exception e) {
			LOGGER.error("Unable to add the users ", e);
		}
		
		return false;
	}
	
	@Override
	public List<AccountUser> getAccountUsersByQueueName(String queuename) {
		return iTkmQueueUsersRepository.getAccountUsersByQueueName(queuename);
	}

	@Override
	public List<Integer> getUserQueueId(int userId) {
		return iTkmQueueUsersRepository.getQueueByUserId(userId);
	}

	@Override
	public List<TkmQueues> getQueueForUser(int userId) {
		return iTkmQueueUsersRepository.getQueueNameByUserId(userId);
	}

	@Override
	public List<Object[]> getAccountUsersWithRoleByQueueName(String queuename) {
		return  iTkmQueueUsersRepository.getAccountUsersWithRoleByQueueName(queuename);
	}
	
	@Override
	public List<Object[]> getAccountUsersForReassign(String queuename) {
		return  iTkmQueueUsersRepository.getAccountUsersByQueueNameForReassign(queuename);
	}
	
	/**
	 * @author Kunal Dav
	 * @return boolean
	 * @param userId
	 * @param List	queueIdList
	 * @return Adds new users to the queue
	 */
	public boolean addQueuesToUser(Integer userId,	List<Integer> queueIdList) {
		boolean bStatus= false;
		
		List<TkmQueueUser> addQueues = new ArrayList<TkmQueueUser>();
		for (Integer queueId : queueIdList) {
			TkmQueueUser queueUser = new TkmQueueUser();
			queueUser.setGroupId(queueId);
			queueUser.setUserId(userId);
			
			addQueues.add(queueUser);
		}
		
		List<TkmQueueUser> addedQueues = iTkmQueueUsersRepository.save(addQueues);
		bStatus = (addedQueues.size() == addQueues.size());
		
		return bStatus;
	}
	
	@Override
	public Map<String, Integer> getworkgrpNameWithCount()

	{
		Map<String, Integer> results = new HashMap<String, Integer>();
		List<Object[]> list = iTkmQueueUsersRepository.getworkgrpNameWithCount();
		for (Object[] ob : list) {
			String key = (String) ob[0];
			Integer value = ((BigInteger) ob[1]).intValue();
			results.put(key, value);
		}
		return results;
	}

	@Override
	public List<Object[]> getAccountUsersNotInQueue(String queuename, String roleName, boolean isAddFlow) {
		List<Object[]> users = null;
        List<String> roleNameList = null;

		if(BooleanUtils.isTrue(isAddFlow)) {
			if(StringUtils.isNotBlank(roleName)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Fetching users in add users flow with role filter "+roleName);
				}
				users = iTkmQueueUsersRepository.getAccountUsersNotInQueue(queuename, roleName);
			} else {
				LOGGER.info("Fetching all users in add users flow");
				  String roleListProp = DynamicPropertiesUtil
			                .getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.INCLUDED_ROLES);
			        if (roleListProp != null) {
			        	  roleNameList = Arrays.asList(roleListProp.split("\\s*,\\s*"));
			              if (roleNameList != null && roleNameList.size() > 0) {

				users = iTkmQueueUsersRepository.getAllAccountUsersNotInQueue(queuename,roleNameList);
			              }
			        }
			}
		} else {
			if(StringUtils.isNotBlank(roleName)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Fetching users in remove users flow with role filter "+roleName);
				}
				users = iTkmQueueUsersRepository.getAccountUsersWithRoleByQueueName(queuename, roleName);
			} else {
				LOGGER.info("Fetching all users in current users flow");
				users = iTkmQueueUsersRepository.getAccountUsersWithRoleByQueueName(queuename);
			}
		}
		LOGGER.info("returning users now");
		return  users;
	}

}
