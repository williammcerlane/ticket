package com.getinsured.hix.tkm.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.tkm.tkmException.TKMException;

public interface TkmMgmtService {

	boolean cancelTicketAndTasks(Integer ticketId, Integer userId, String comments, boolean reclassifyAction) throws TKMException;

	TkmTickets createTicketInDB(TkmTicketsRequest tickRequest) throws TKMException;

	boolean saveCommentsInDB(Integer ticketId, Integer creatorId, String strComment, boolean bUpdate) throws TKMException;

	boolean saveDocumentsInDB(Integer ticketId, Integer creatorId, List<String> docLinks, boolean bUpdate) throws TKMException;

	String getTicketStatusFromDB(Integer ticketId) throws TKMException;

	List<TkmTickets> searchTicketsByCriteria(TkmTicketsRequest ticketRequest) throws TKMException;

	void autoCancelProcess(String processInstanceId) throws TKMException;
	
	void statusChangeNotifyShop(String status, String userName, String roleId);
	
	void verifyDocumentStatusChange(String status, List<String> documentIds, String processInstanceId, String activitiTaskId, String comments, String taskRejectionReason);

	void verifyDocumentStatusChange(String status, String documentId, String processInstanceId, String activitiTaskId, String comments);

	boolean autoClaimCompleteTasks(Integer ticketId, Integer userId, String comments, Map<String, Object> taskVariables) throws TKMException;

	boolean claimTicketTask(Integer taskId, String activitiTaskId, Integer userId) throws TKMException;

	boolean completeTicketTask(Integer taskId, String activitiId, Integer userId, String comments, Map<String, Object> variables, boolean bAutoComplete, String processInstanceId) throws TKMException;
	
	boolean reassignTkkActiveTask(Integer taskId, Integer groupId,
			Integer userId, String string) throws TKMException;

	boolean updateQueueUsers(Integer queueId, List<Integer> userList,boolean addFlag) throws TKMException;
	
	void sendTicketCreateEmail(Integer ticketId) throws NotificationTypeNotFound;
	
	void sendTicketResolvedEmail(Integer ticketId, String approveStatus, String comment) throws NotificationTypeNotFound;
	
	void sendTicketCancelledEmail(Integer ticketId) throws NotificationTypeNotFound;

	boolean sendTicketOpenEmail(Integer ticketId) throws NotificationTypeNotFound;

	//TkmTickets reClassifyTicketWorkFlow(Integer ticketId, String ticketCategory,String type, Map<String, String> ticketFormPropertiesMap) throws TKMException;
	TkmTickets reClassifyTicketWorkFlow(Integer ticketId, String ticketCategory,String type, 
			Map<String, String> ticketFormPropertiesMap, Integer userId) throws TKMException;
	
	boolean sendEmailByTicketType(String ticketStatusValue,String emailConfigValue);
	
	void reClassifyToAppeal(String ticketStatus);
	void sendMailToDenounceUser(String ticketNum, String denounceUserId);
	void copyToOSI(String ticketNum);
	
	//Individual consumer activiti interfaces
	void sendFormToMedicaid(String ticketNum);
	void storeQHPAppealForm(String ticketNum);
	
	// Individual Appeal workflow interfaces from BPM file.
	void sendIndvAppealNoticeToCust(String appealReason);
	void sendAppChangeNoticeToCust(String appealReason);
	void statusChangeNotifyShop(String status, String moduleId);
	void sendIndvAppealNoticeToCust(String appealReason,String ticketReason);
	void sendAppChangeNoticeToCust(String appealReason,String ticketReason);

	String getCreatedForIndv(String userText);

	String getCreatedForUser(String roleId, String userText);

	List<Household> getHouseholdByuserId(int houseHoldUserId);
	
	boolean archiveTicket(Integer ticketId,Integer archivedBy) throws TKMException;

	void reopenTicket(Integer ticketId, Integer lastUpdatedBy) throws TKMException;

	void restartTicket(Integer ticketId, Integer lastUpdatedBy) throws TKMException;

	void addQueuesToUser(Integer userId, List<Integer> queueIdList);	
	
	void logTicketEvents(Integer ticketId, String eventType) throws TKMException;
	void logTicketEvents(TkmTickets tkmTickets, String eventType);
	void updateTicketWithEvent(String ticketId,String subModuleId,String subModuleName);
	void processQLEVerification(String status, String documentId, String processInstanceId, String activitiTaskId, String comments);

}
