package com.getinsured.hix.ticket.controller;

import com.getinsured.hix.indportal.dto.ticket.UserTicket;
import com.getinsured.hix.indportal.dto.ticket.UserComment;

import com.getinsured.hix.model.*;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.ticket.exception.*;

import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmQueueUsersRepository;
import com.getinsured.hix.tkm.service.TkmMgmtService;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.google.common.html.HtmlEscapers;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/tickets")
public class TicketController {

  @Autowired
  CommentTargetService commentTargetService;
  @Autowired
  UserService userService;
  @Autowired
  TkmMgmtService tkmMgmtService;
  @Autowired
  RoleService roleService;
  @Autowired
  private ITicketRepository iTicketRepository;
  @Autowired
  private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

  @Autowired
  private ITkmQueueUsersRepository iTkmQueueUsersRepository;

  private static final Logger log = LoggerFactory.getLogger(TicketController.class);

  private static final int MAX_COMMENT_LENGTH = 4000;

  /**
   * Added by William McErlane
   *
   *
   * @param id - ticketId which maps to target_id
   * @param type - TICKET, TICKET_PUBLIC (but can pass anything)
   * @return String - JSON payload of comments
   */
/*  @RequestMapping(value = "/{ticketId}/comments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Comment> getTicketComments(@PathVariable(value="ticketId") String id,
                                         @RequestParam(defaultValue = "TICKET_PUBLIC") String type ){

    try{
      log.info("TargetName.valueOf(targetName) : " +TargetName.valueOf(type));
      TargetName.valueOf(type);
    }catch (Exception e) {
      throw new InvalidTypeException(type);
    }

    CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(id), TargetName.valueOf(type));
    if(commentTarget==null) {
      throw new CommentNotFoundException(Long.valueOf(id));
    }
    List<Comment> commentsList = commentTarget.getComments();

    return commentsList;
  }*/


  /**
   * Added by William McErlane
   *
   * @param UserComment -incoming payload
   * @return Comment - created comment
   */

  @RequestMapping(value="/{ticketId}/comments",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  public Comment saveComment(@RequestBody UserComment newComment){

    String targetId = newComment.getTargetId();
    String targetName = newComment.getType();
    String commentText = newComment.getCommentText();

    log.info("Retrieving logged in user's name");

    AccountUser accountUser = null;
      accountUser = userService.findById(Integer.parseInt(newComment.getUserId()));
      if(accountUser==null){
        throw new UserNotFoundException(Long.valueOf(newComment.getUserId()));
      }

    if(commentText != null) {
      commentText = StringEscapeUtils.unescapeHtml(commentText.trim());
    }

    try{
      log.info("TargetName.valueOf(targetName) : " +TargetName.valueOf(targetName));
      TargetName.valueOf(targetName);
    }catch (Exception e) {
      throw new InvalidTypeException(targetName);
    }


    CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(targetId), TargetName.valueOf(targetName));
    if(commentTarget == null){
      commentTarget =  new CommentTarget();
      commentTarget.setTargetId(new Long(targetId));
      commentTarget.setTargetName(TargetName.valueOf(targetName));
    }

    List<Comment> comments = null;

    if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
      comments = new ArrayList<Comment>();
    }
    else{
      comments = commentTarget.getComments();
    }

    String commenterName = "";

    if(accountUser != null){
      commenterName += (accountUser.getFirstName() == null || accountUser.getFirstName().length() == 0 ) ? "" :  accountUser.getFirstName()+" ";
      commenterName += (accountUser.getLastName() == null || accountUser.getLastName().length() == 0 ) ? "" : accountUser.getLastName();
    }

    //Checking if user's name is blank
    if(commenterName == null || commenterName.trim().length() == 0){
      commenterName = "Anonymous User";
    }

    log.info("Commented by : " + commenterName);

    Comment comment = new Comment();

    //Setting logged in user's id
    log.info("Commenter's user Id : " + accountUser.getId());
    comment.setComentedBy(accountUser.getId());

    comment.setCommenterName(commenterName);

    //Fetching first 4000 char for comment text;
    int beginIndex = 0;
    int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
    commentText = commentText.substring(beginIndex, endIndex);
    comment.setComment(commentText);

    comment.setCommentTarget(commentTarget);

    comments.add(comment);
    commentTarget.setComments(comments);
    commentTargetService.saveCommentTarget(commentTarget);

    return commentTarget.getComments().get(commentTarget.getComments().size()-1);
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public UserTicket createTicket(@RequestBody UserTicket ticket, @RequestParam(defaultValue = "true") String sendEmail ){

    Role role = roleService.findRoleByName(ticket.getRole());

    if(role==null){
      throw new InvalidRoleException(ticket.getRole());
    }

    TkmTicketsRequest ticketRequest = new TkmTicketsRequest();
    ticketRequest.setDescription(HtmlEscapers.htmlEscaper().escape(ticket.getDetails()));
    ticketRequest.setSubject(HtmlEscapers.htmlEscaper().escape(ticket.getSubject()));
    ticketRequest.setRequester(Integer.parseInt(ticket.getCreatedFor()));
    ticketRequest.setCreatedBy(Integer.parseInt(ticket.getCreatedBy()));
    ticketRequest.setCategory(ticket.getCategory());
    ticketRequest.setType(ticket.getType());
    ticketRequest.setUserRoleId(role.getId());
    TkmTickets tkmTicket = null;
    try {
        tkmTicket = tkmMgmtService.createTicketInDB(ticketRequest);
    } catch(Exception ex){
      log.info( "TKMException " +ex.getMessage());
    }
    if(tkmTicket != null){
      ticket.setId(tkmTicket.getId());
      ticket.setNumber(tkmTicket.getNumber());
      ticket.setCreated(tkmTicket.getCreated());
      ticket.setStatus(tkmTicket.getStatus());
      ticket.setEncryptedId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(tkmTicket.getId())));
    }else{
      throw new TicketCreationException();
    }

    if(Boolean.parseBoolean(sendEmail)) {
      try {
        tkmMgmtService.sendTicketCreateEmail(ticket.getId());
      }catch(NotificationTypeNotFound e){
        log.info( "NotificationTypeNotFound " +e.getMessage());
      }
    }

    return ticket;
  }


  @RequestMapping(value = "/status", method = RequestMethod.POST)
  public TkmTickets updateStatus(@RequestBody UserTicket ticket){

    TkmTickets savedTicket = updateStatus(ticket.getId(), ticket.getStatus(), Integer.parseInt(ticket.getCreatedBy()));

    return savedTicket;
  }

  public TkmTickets updateStatus(Integer ticketId, String status, Integer userId) {
    TkmTickets savedTicket = null;

    TkmTickets tkmTickets = iTicketRepository.findOne(ticketId);
    if(tkmTickets != null) {
      tkmTickets.setStatus(TkmTickets.ticket_status.valueOf(status).toString());


      AccountUser user = new AccountUser();
      user.setId(userId);
      tkmTickets.setUpdator(user);

      savedTicket = iTicketRepository.save(tkmTickets);
    }

    return savedTicket;
  }

}
