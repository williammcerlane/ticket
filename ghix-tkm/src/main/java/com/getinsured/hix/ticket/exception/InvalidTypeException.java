package com.getinsured.hix.ticket.exception;

public class InvalidTypeException extends RuntimeException {

    public InvalidTypeException(String type) {
      super("Invalid type " + type);
    }
  }
