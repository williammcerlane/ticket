package com.getinsured.hix.ticket.exception;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(Long id) {
    super("Could not find User for id " + id);
  }
}
