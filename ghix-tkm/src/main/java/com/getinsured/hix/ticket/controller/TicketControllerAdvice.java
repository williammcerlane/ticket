package com.getinsured.hix.ticket.controller;

import com.getinsured.hix.ticket.exception.CommentNotFoundException;
import com.getinsured.hix.ticket.exception.InvalidTypeException;
import com.getinsured.hix.ticket.exception.UserNotFoundException;
import com.google.gson.Gson;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice(assignableTypes = TicketController.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
class TicketControllerAdvice {

  @ResponseBody
  @ExceptionHandler(CommentNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String commentNotFoundHandler(CommentNotFoundException ex) {
    return exceptionToJSON(ex);
  }

  @ResponseBody
  @ExceptionHandler(UserNotFoundException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  String userNotFoundHandler(UserNotFoundException ex) {
    return exceptionToJSON(ex);
  }

  @ResponseBody
  @ExceptionHandler(InvalidTypeException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  String invalidTypeFoundHandler(InvalidTypeException ex) {
      return exceptionToJSON(ex);
  }

  String exceptionToJSON(Exception ex){
    Gson gson = new Gson();
    Map<String, String> exc_map = new HashMap<String, String>();
    exc_map.put("message", ex.getMessage());
    return gson.toJson(exc_map);
  }


}
