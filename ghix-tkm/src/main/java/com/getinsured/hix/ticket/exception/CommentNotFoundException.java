package com.getinsured.hix.ticket.exception;


public class CommentNotFoundException extends RuntimeException {

 public CommentNotFoundException(Long id) {
    super("Could not find Comments for ticket id " + id);
  }
}
