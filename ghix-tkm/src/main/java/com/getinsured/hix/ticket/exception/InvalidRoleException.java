package com.getinsured.hix.ticket.exception;

public class InvalidRoleException extends RuntimeException {

        public InvalidRoleException(String role) {
            super("Invalid Role " + role);
        }
    }
