package com.getinsured.hix.ticket.exception;

public class TicketCreationException extends RuntimeException {

    public TicketCreationException() {
        super("Ticket could not be created at this time check ghix-tkm logs");
    }
}
